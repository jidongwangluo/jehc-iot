//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device/iot-device-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateIotDeviceConfig(){
	submitBForm('defaultForm',monitorModules+'/iotDeviceConfig/update',base_html_redirect+'/monitor/iot-device/iot-device-list.html',null,"PUT");
}

$(document).ready(function(){
	var device_id = GetQueryString("device_id");
    $('#device_id').val(device_id);
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotDeviceConfig/get/'+device_id,{},function(result){
		if(null != result.data && undefined != result.data){
            $('#device_config_id').val(result.data.device_config_id);
            $('#ip').val(result.data.ip);
            $('#port').val(result.data.port);
            $('#user_name').val(result.data.user_name);
            $('#password').val(result.data.password);
            $('#channel_no').val(result.data.channel_no);
            $('#rtsp_port').val(result.data.rtsp_port);
            $('#nvr_channel').val(result.data.nvr_channel);
            $('#protocol').val(result.data.protocol);
            $('#type').val(result.data.type);
            $('#subtype').val(result.data.subtype);
            $("#deviceName").html("配置设备-<font style='color: #00a3b6'>["+result.data.iotDevice.name+"]</font>");
		}
	});
});
