var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,monitorModules+'/graphSite/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"graph_site_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"graph_site_id",
				width:"50px"
			},
			{
				data:'graph_site_name',
                width:"120px"
			},
			{
				data:'graph_site_status',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>未发布</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>已发布</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
			},
			{
				data:'createBy',
                width:"50px"
			},
			{
				data:'cTime',
                width:"50px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
			},
			{
				data:'modifiedBy',
                width:"50px"
			},
			{
				data:'mTime',
                width:"50px",
                render:function(data, type, row, meta) {
                    return dateformat(data);
                }
			},
			/*{
				data:'background',
                render:function(data, type, row, meta) {
					if(data !=null){
						return "<label style='width: 20px;height:10px ;background: '"+data+"></label>";
					}else{
                        return "";
					}
                }
			},
			{
				data:'gridColor',
                render:function(data, type, row, meta) {
                    if(data !=null){
                        return "<label style='width: 20px;height:10px ;background: '"+data+"></label>";
                    }else{
                        return "";
                    }
                }
			},*/
			{
				data:'showGrid',
                width:"50px",
                render:function(data, type, row, meta) {
                    if(data == 0){
                        return "<span class='m-badge m-badge--info'>是</span>"
                    }
                    if(data == 1){
                        return "<span class='m-badge m-badge--success'>否</span>"
                    }
                    return "<span class='m-badge m-badge--info'>缺省</span>"
                }
			},
			{
				data:"graph_site_id",
				render:function(data, type, row, meta) {
                    var btn = '<button onclick=toGraphDesign("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="在线设计"><i class="flaticon-settings-1"></i></button>';
                    btn = btn+'<button onclick=toGraphPreview("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="预览"><i class="m-menu__link-icon flaticon-location"></i></button>';
                    return btn+"<button class='m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill' onclick=\"javascript:toGraphSiteDetail('"+ data +"')\" title='详情'><i class=\"flaticon-visible\"></button>";
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});

//新增
function toGraphSiteAdd(){
    tlocation(base_html_redirect+'/monitor/graph-site/graph-site-add.html');
}
//修改
function toGraphSiteUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/monitor/graph-site/graph-site-update.html?graph_site_id='+id);
}
//详情
function toGraphSiteDetail(id){
    tlocation(base_html_redirect+'/monitor/graph-site/graph-site-detail.html?graph_site_id='+id);
}

//删除
function delGraphSite(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {graph_site_id:id};
        ajaxBReq(monitorModules+'/graphSite/delete',params,['datatables'],null,"DELETE");
	})
}

/**
 * 在线设计
 * @param id
 */
function toGraphDesign(id){
    var url = base_html_redirect+'/monitor/graph-design/graph-design-list.html?graph_site_id='+id;
        window.open(url, '在线设计', 'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')
}
/**
 * 在线设计
 * @param id
 */
function toGraphPreview(id){
    var url = base_html_redirect+'/monitor/graph-preview/graph-preview-list.html?graph_site_id='+id;
    window.open(url, '预览', 'toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes')
}