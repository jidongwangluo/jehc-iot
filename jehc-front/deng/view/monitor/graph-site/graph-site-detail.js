//返回
function goback(){
    tlocation(base_html_redirect+'/monitor/graph-site/graph-site-list.html');
}

$(document).ready(function(){
    var graph_site_id = GetQueryString("graph_site_id");
    //加载表单数据
    ajaxBRequestCallFn(monitorModules+'/graphSite/get/'+graph_site_id,{},function(result){
        $('#graph_site_id').val(result.data.graph_site_id);
        $('#graph_site_name').val(result.data.graph_site_name);
        $('#graph_site_status').val(result.data.graph_site_status);
        $('#graph_site_mxgraph').val(result.data.graph_site_mxgraph);
        $('#cTime').val(result.data.cTime);
        $('#mTime').val(dateformat(result.data.mTime));
        $('#content').val(result.data.content);
        $('#background').val(result.data.background);
        $('#gridColor').val(result.data.gridColor);
        $('#showGrid').val(result.data.showGrid);
        $('#gridSize').val(result.data.gridSize);
        $('#createBy').val(result.data.createBy);
        $('#modifiedBy').val(result.data.modifiedBy);
    });
});
