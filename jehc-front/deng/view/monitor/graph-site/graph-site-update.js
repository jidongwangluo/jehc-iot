//返回
function goback(){
    tlocation(base_html_redirect+'/monitor/graph-site/graph-site-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateGraphSite(){
	submitBForm('defaultForm',monitorModules+'/graphSite/update',base_html_redirect+'/monitor/graph-site/graph-site-list.html',null,"PUT");
}

//初始化日期选择器
$(document).ready(function(){
    datetimeInit();
});

$(document).ready(function(){
    var graph_site_id = GetQueryString("graph_site_id");
    //加载表单数据
    ajaxBRequestCallFn(monitorModules+'/graphSite/get/'+graph_site_id,{},function(result){
        $('#graph_site_id').val(result.data.graph_site_id);
        $('#graph_site_name').val(result.data.graph_site_name);
        $('#graph_site_status').val(result.data.graph_site_status);
        $('#graph_site_mxgraph').val(result.data.graph_site_mxgraph);
        $('#xt_userinfo_id').val(result.data.xt_userinfo_id);
        $('#cTime').val(result.data.cTime);
        $('#mTime').val(dateformat(result.data.mTime));
        $('#update_userinfo_id').val(dateformat(result.data.update_userinfo_id));
        $('#content').val(result.data.content);
        $('#background').val(result.data.background);
        $('#gridColor').val(result.data.gridColor);
        $('#showGrid').val(result.data.showGrid);
        $('#gridSize').val(result.data.gridSize);
    });
});

