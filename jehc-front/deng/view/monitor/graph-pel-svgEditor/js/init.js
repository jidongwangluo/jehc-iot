init();

/**
 * 初始化
 */
function init(){
    var dims = state.get("canvasSize");
    state.set("canvasMode", "select")
    svgCanvas.clear();
    svgCanvas.setResolution(dims[0], dims[1]);
    editor.canvas.update(true);
    editor.zoom.reset();
    editor.panel.updateContextPanel();
    editor.paintBox.fill.prep();
    editor.paintBox.stroke.prep();
    svgCanvas.runExtensions('onNewDocument');
    initPelData();
}

/**
 *
 */
function initPelData() {
    var graph_pel_id = GetQueryString("graph_pel_id");
    $("#graph_pel_id").val(graph_pel_id);
    var title = GetQueryString("title");
    $.ajax({
        url:monitorModules+'/graphPel/get/'+graph_pel_id,
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        type:"GET",//PUT DELETE POST
        data:{},
        success:function(result){
            svgCanvas.setSvgString(result.data.svgHtml);
            svgCanvas.setDocumentTitle(result.data.graph_pel_title);
            state.set("canvasTitle", result.data.graph_pel_title);
        },
        error:function(){
        }
    })
}

//获取地址栏参数
function GetQueryString(name){
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);//search,查询？后面的参数，并匹配正则
    if(r!=null)return  unescape(r[2]); return null;
}

/**
 *
 */
function goback(){
    tlocation(base_html_redirect+'/monitor/graph-pel/graph-pel-list.html');
}

/**
 *
 * @param url
 */
function tlocation(url){
    document.location.href=url;
}

/**
 * 保存
 */
function savePelSvg() {
    var data = svgCanvas.getSvgString();
    var params = {};
    params.graph_pel_id = $("#graph_pel_id").val();
    params.svgHtml = data;
    swal({
        title: "确定要保存数据？",
        text: "是的，保存数据",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "确认",
        confirmButtonColor: "#ec6c62"
    }, function() {
        ajaxBRequestCallUrl(monitorModules+'/graphPel/updatePelSvg',base_html_redirect+'/monitor/graph-pel/graph-pel-list.html',params,null,"POST")
    });
}