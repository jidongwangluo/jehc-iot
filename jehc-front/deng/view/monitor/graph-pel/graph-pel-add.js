//返回
function goback(){
    tlocation(base_html_redirect+'/monitor/graph-pel/graph-pel-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

addGraphPelElementItems();

//保存
function addGraphPel(){
    submitBForm('defaultForm',monitorModules+'/graphPel/add',base_html_redirect+'/monitor/graph-pel/graph-pel-list.html');
}

function setVal(thiz,type,numbers){
    if(type == 1){
        $("#form_graphPelElement_"+numbers+"_fills").val(thiz.value);
    }else{
        $("#form_graphPelElement_"+numbers+"_stock").val(thiz.value);
    }
    preImages();
}

//$(document).ready(function(){
//    $("#file-input").change(function(e){
//        var file = e.target.files[0];
//        var reader = new FileReader();
//        reader.readAsDataURL(file); //读出 base64
//        reader.onloadend = function () {
//            var data_64= "data:image/png;base64,"+reader.result.substring(reader.result.indexOf(",")+1);
//            $("#image").val(data_64);
//            $("#imgCav").attr("src",data_64); 
//        };
//    });
//});

function preImages(){
    try{
        var svgPath = $('#svg').val();
        var stockFill = $('#pickColors').val();//线条颜色
        var fills = $('#fillColors').val();//填充
        var width=32;//图元宽度
        var height =32;//图元高度
        if(null == svgPath || '' ==svgPath){
            return;

        }
        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
        svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
        svg.setAttribute('xml:space', 'preserve')
        svg.setAttribute('width', width)
        svg.setAttribute('height', height)
        svg.setAttribute('version', '1.1')

        svg.setAttribute('viewBox', '0 0 '+width+' '+height)
        svg.setAttribute('preserveAspectRatio', 'none')
        // svg.setAttribute('style', 'shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd')
        svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink')

        var g = document.createElementNS("http://www.w3.org/2000/svg", "g")
        g.setAttribute('id', 'G_x0020_1')
        var metadata = document.createElementNS("http://www.w3.org/2000/svg", "metadata")
        metadata.setAttribute('id', 'CorelCorpID_0Corel-Layer')
        var cong = document.createElementNS("http://www.w3.org/2000/svg", "g")
        cong.setAttribute('id', '_1828549179696')

        // var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect")
        // rect.setAttribute('class', 'fil0 str0')
        // rect.setAttribute('x', '17')
        // rect.setAttribute('y', '17')
        // rect.setAttribute('width', '32')
        // rect.setAttribute('height', '32')
        var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
        // path.setAttribute('class', 'fil1')
        if(null != svgPath){
            path.setAttribute('d', svgPath)
            path.setAttribute('stroke', stockFill)
            if(null == fills || '' == fills || '#ffffff' == fills || '#fff' == fills){
                path.setAttribute('fill', "none")
            }else{
                path.setAttribute('fill', fills)
            }

        }


        // var defs = document.createElementNS("http://www.w3.org/2000/svg", "defs")
        // var style = document.createElementNS("http://www.w3.org/2000/svg", "style")
        // style.setAttribute('type', 'text/css')
        // style.innerHTML =
        //     ".str0 {" +
        //     "stroke: #fff; stroke-width:3;}" +
        //     ".fil0 {fill:none }" +
        //     ".fil1 {" +
        //     "fill: #FF0000;}"
        g.appendChild(metadata)
        g.appendChild(cong)
        // cong.appendChild(rect)
        cong.appendChild(path)
        // defs.appendChild(style)
        // svg.appendChild(defs)
        svg.appendChild(g);
        var data_64 = "data:image/svg+xml;base64," + btoa(unescape(encodeURIComponent(new XMLSerializer().serializeToString(svg))));
        $("#imgCav").attr("src",data_64);
        $("#image").val(data_64);
    }catch(e){

    }
}


function addGraphPelElementItems(){
    validatorDestroy('defaultForm');
    var uuid = guid();
    //点击添加新一行
    var removeBtn = '<a class="btn btn-secondary m-btn m-btn--icon" href=javascript:delGraphPelElementItems(this,"'+uuid+'")><span><i class="la la-close"></i><span>删 除</span></span></a>';
    var form =
        '<div id="form_graphPelElement_'+uuid+'" name="form_graphPelElement">'+
            '<div class="form-group m-form__group row"><label class="col-md-1 col-form-label"><span name="legendLabel">No.1</span></label><div class="col-md-1">'+removeBtn+'</div></div>'+
            '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型</label>'+
                '<div class="col-md-1">'+
                    '<select class="form-control" id="form_graphPelElement_'+uuid+'_graph_pel_element_categray" name="graphPelElements[][graph_pel_element_categray]"  placeholder="请选择"><option value="">请选择</option><option value="path">path</option><option value="ellipse">ellipse</option><option value="circle">circle</option><option value="rect">rect</option><option value="line">line</option><option value="polygon">polygon</option><option value="polyline">polyline</option><option value="text">text</option></select>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">填充颜色</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" value="#ffffff" id="form_graphPelElement_'+uuid+'_fills" name="graphPelElements[][fills]"  placeholder="请选择"/>'+
                '</div>'+
                '<div class="col-md-1">'+
                    '<input type="color" class="form-control" value="#ffffff" onchange="setVal(this,1,'+uuid+')" id="fillColor">'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线条颜色</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" value="#FF0000" id="form_graphPelElement_'+uuid+'_stock" name="graphPelElements[][stock]"  placeholder="请选择"/>'+
                '</div>'+
                '<div class="col-md-1">'+
                    '<input type="color" class="form-control" value="#FF0000" onchange="setVal(this,2,'+uuid+')" id="strokeColor">'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线框宽度</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_stroke_width" name="graphPelElements[][stroke_width]"  placeholder="请输入"/>'+
                '</div>'+
            '</div>'+
            '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">透&nbsp;明&nbsp;&nbsp;度</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="2" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'graph_pel_element_opacity" name="graphPelElements[][graph_pel_element_opacity]"  placeholder="请输入"/>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线条透明度</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="4" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'stroke_opacity" name="graphPelElements[][stroke_opacity]"  placeholder="请输入"/>'+
                '</div>' +
                '<div class="col-md-1"></div>'+
                '<label class="col-md-1 col-form-label">填充透明度</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="4" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'fill_opacity" name="graphPelElements[][fill_opacity]"  placeholder="请输入"/>'+
                '</div>'+
            '</div>'+
            '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">排&nbsp;序&nbsp;&nbsp;号</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="2" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_graph_pel_element_sort" name="graphPelElements[][graph_pel_element_sort]"  placeholder="请输入"/>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">cx</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="4" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_graph_pel_element_cx" name="graphPelElements[][graph_pel_element_cx]"  placeholder="请输入"/>'+
                '</div>' +
                '<div class="col-md-1"></div>'+
                '<label class="col-md-1 col-form-label">cy</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="4" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_graph_pel_element_cy" name="graphPelElements[][graph_pel_element_cy]"  placeholder="请输入"/>'+
                '</div>'+
                '<div class="col-md-1"></div>'+
                '<label class="col-md-1 col-form-label">r</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" maxlength="4" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_graph_pel_element_r" name="graphPelElements[][graph_pel_element_r]"  placeholder="请输入"/>'+
                '</div>'+
            '</div>'+
            '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">代&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码</label>'+
                '<div class="col-md-6">'+
                    '<textarea class="form-control" style="overflow: hidden; overflow-wrap: break-word; resize: none; height: 73px;" rows="1" id="form_graphPelElement_'+uuid+'_graph_pel_element_attrs" name="graphPelElements[][graph_pel_element_attrs]"  placeholder="请输入"/>'+
                '</div>'+
            '</div>'+
        '</div>'
    $(".form_graphPelElement").append(form);
    reValidator('defaultForm');
    setLegendLabel("legendLabel");
    setElementBackground("form_graphPelElement");
}


function delGraphPelElementItems(thiz,numbers){
    validatorDestroy('defaultForm');
    $("#form_graphPelElement_"+numbers).remove();
    var graphPelElement_removed_flag = $('#graphPelElement_removed_flag').val()
    if(null == graphPelElement_removed_flag || '' == graphPelElement_removed_flag){
        $('#graphPelElement_removed_flag').val(','+numbers+',');
    }else{
        $('#graphPelElement_removed_flag').val(graphPelElement_removed_flag+numbers+',')
    }
    reValidator('defaultForm');
    setLegendLabel("legendLabel");
    setElementBackground("form_graphPelElement");
}
