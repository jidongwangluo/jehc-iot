//返回
function goback(){
    tlocation(base_html_redirect+'/monitor/graph-pel/graph-pel-list.html');
}

//初始化日期选择器
$(document).ready(function(){
    datetimeInit();
});

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

$(document).ready(function(){
    var graph_pel_id = GetQueryString("graph_pel_id");
    //加载表单数据
    ajaxBRequestCallFn(monitorModules+'/graphPel/get/'+graph_pel_id,{},function(result){
        $('#graph_pel_id').val(result.data.graph_pel_id);
        $('#graph_pel_category').val(result.data.graph_pel_category);
        $('#graph_pel_cell_type').val(result.data.graph_pel_cell_type);
        $('#graph_pel_title').val(result.data.graph_pel_title);
        $('#graph_pel_key').val(result.data.graph_pel_key);
        $('#graph_pel_type').val(result.data.graph_pel_type);
        $('#cellWidth').val(result.data.width);
        $('#cellHeight').val(result.data.height);
        $('#graph_pel_status').val(result.data.graph_pel_status);
        $('#graph_pel_sort').val(result.data.graph_pel_sort);
        $('#hideLabel').val(result.data.hideLabel);
        $('#hideTitles').val(result.data.hideTitles);
        $('#image').val(result.data.image);
        $('#imgCav').val(result.data.imgCav);
        $('#svgHtml').val(result.data.svgHtml);
        $('#render_method').val(result.data.render_method);
        var graphPelElements = result.data.graphPelElements;
        for(var i in graphPelElements){
            var pelElement = graphPelElements[i];
            addGraphPelElementItems(pelElement);
        }
    });
});

function addGraphPelElementItems(pelElement){
    validatorDestroy('defaultForm');
    var uuid = guid();
    //点击添加新一行
    var form =
        '<div name="form_graphPelElement" id="form_graphPelElement_'+uuid+'" name="form_graphPelElement">'+
            '<div class="form-group m-form__group row"><label class="col-md-1 col-form-label"><span name="legendLabel">No.1</span></label></div>'+
            '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">节点类型</label>'+
                '<div class="col-md-1">'+
                    '<select class="form-control" id="form_graphPelElement_'+uuid+'_graph_pel_element_categray" name="graphPelElements[][graph_pel_element_categray]"  placeholder="请选择"><option value="">请选择</option><option value="path">path</option><option value="ellipse">ellipse</option></select>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">填充颜色</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" value="#ffffff" readOnly id="form_graphPelElement_'+uuid+'_fills" name="graphPelElements[][fills]"  placeholder="请选择"/>'+
                '</div>'+
                '<div class="col-md-1">'+
                    '<input type="color" class="form-control" value="#ffffff" onchange="setVal(this,1,'+uuid+')" id="fillColor">'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线条颜色</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" readOnly value="#FF0000" id="form_graphPelElement_'+uuid+'_stock" name="graphPelElements[][stock]"  placeholder="请选择"/>'+
                '</div>'+
                '<div class="col-md-1">'+
                    '<input type="color" class="form-control" value="#FF0000" onchange="setVal(this,2,'+uuid+')" id="strokeColor">'+
                '</div>'+
                '<label class="col-md-1 col-form-label">线框宽度</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_stroke_width" name="graphPelElements[][stroke_width]"  placeholder="请输入"/>'+
                '</div>'+
                '<label class="col-md-1 col-form-label">排序号</label>'+
                '<div class="col-md-1">'+
                    '<input class="form-control" data-bv-numeric data-bv-numeric-message="此项为数字类型" id="form_graphPelElement_'+uuid+'_graph_pel_element_sort" name="graphPelElements[][graph_pel_element_sort]"  placeholder="请输入"/>'+
                '</div>'+
                '</div>'+
                '<div class="form-group m-form__group row">'+
                '<label class="col-md-1 col-form-label">属性代码</label>'+
                '<div class="col-md-3">'+
                    '<textarea class="form-control" rows="1" id="form_graphPelElement_'+uuid+'_graph_pel_element_attrs" name="graphPelElements[][graph_pel_element_attrs]"  placeholder="请输入"/>'+
                '</div>'+
            '</div>'+
        '</div>'
    $(".form_graphPelElement").append(form);
    $('#form_graphPelElement_'+uuid+'_graph_pel_element_categray').val(pelElement.graph_pel_element_categray);
    $('#form_graphPelElement_'+uuid+'_fills').val(pelElement.fills);
    $('#form_graphPelElement_'+uuid+'_stock').val(pelElement.stock);
    $('#form_graphPelElement_'+uuid+'_stroke_width').val(pelElement.stroke_width);
    $('#form_graphPelElement_'+uuid+'_graph_pel_element_sort').val(pelElement.graph_pel_element_sort);
    $('#form_graphPelElement_'+uuid+'_graph_pel_element_attrs').val(pelElement.graph_pel_element_attrs);
    reValidator('defaultForm');
    setLegendLabel("legendLabel");
    setElementBackground("form_graphPelElement");
}
