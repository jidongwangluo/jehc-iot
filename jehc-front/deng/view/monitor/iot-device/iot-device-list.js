var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,monitorModules+'/iotDevice/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight2,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"device_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"device_id",
				width:"50px"
			},
			{
				data:'device_type_name'
			},
			{
				data:'device_code',
                width:"50px"
			},
			{
				data:'name'
			},
			{
				data:'short_name',
                width:"50px"
			},
			{
				data:'oem_name'
			},
			{
				data:'createBy',
                width:"50px"
			},
			{
				data:'create_time',
                width:"50px"
			},
			{
				data:'modifiedBy',
                width:"50px"
			},
			{
				data:'update_time',
                width:"50px"
			},
			{
				data:"device_id",
				width:"150px",
				render:function(data, type, row, meta) {
                    var btn = '<button onclick=toIotDeviceDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情"><i class="flaticon-visible"></i></button>';
                    btn = btn+'<button onclick=toIotDeviceConfig("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="配置属性"><i class="flaticon-settings-1"></i></button>';
                    return btn;
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
    getOemList();
    getDeviceTypeList();
});
//新增
function toIotDeviceAdd(){
	tlocation(base_html_redirect+'/monitor/iot-device/iot-device-add.html');
}
//修改
function toIotDeviceUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/monitor/iot-device/iot-device-update.html?device_id='+id);
}

//配置属性
function toIotDeviceConfig(id){
	tlocation(base_html_redirect+'/monitor/iot-device-config/iot-device-config-update.html?device_id='+id);
}

//详情
function toIotDeviceDetail(id){
    tlocation(base_html_redirect+'/monitor/iot-device/iot-device-detail.html?device_id='+id);
}

//删除
function delIotDevice(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {device_id:id,_method:'DELETE'};
		ajaxBReq(monitorModules+'/iotDevice/delete',params,['datatables'],null,"DELETE");
	})
}


function getDeviceTypeList(){
    //清空下拉数据
    $("#device_type_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceType/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_type_id + "'>" + item.device_type_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_type_id").append(str);
        },
        error:function(){}
    });
}

function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}

function getModelList(oem_id){
    //清空下拉数据
    $("#model_id").html("");
    var str = "<option value=''>请选择</option>";
    if(null == oem_id){
        //将数据添加到这个下拉框里面
        $("#model_id").append(str);
    	return;
	}
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotModel/list/"+oem_id,
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.model_id + "'>" + item.model_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#model_id").append(str);
        },
        error:function(){}
    });
}