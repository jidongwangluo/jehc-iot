//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device/iot-device-list.html');
}
$(document).ready(function(){
	var device_id = GetQueryString("device_id");
    getOemList();
    getDeviceTypeList();
    getDeviceLocationList();
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotDevice/get/'+device_id,{},function(result){
        $('#device_id').val(result.data.device_id);
		$('#device_type_id').val(result.data.device_type_id);
		$('#device_code').val(result.data.device_code);
		$('#name').val(result.data.name);
		$('#short_name').val(result.data.short_name);
		$('#oem_id').val(result.data.oem_id);
        $('#model_id').val(result.data.model_id);
        $('#produce_date').val(result.data.produce_date);
        $('#scrap_time').val(result.data.scrap_time);
		$('#modifiedBy').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#createBy').val(result.data.createBy);
		$('#update_time').val(result.data.update_time);
        $('#device_location_id').val(result.data.device_location_id);
        getModelList(result.data.oem_id,result.data.model_id);
	});
});


function getDeviceTypeList(){
    //清空下拉数据
    $("#device_type_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceType/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_type_id + "'>" + item.device_type_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_type_id").append(str);
        },
        error:function(){}
    });
}

function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}

function getModelList(oem_id,model_id){
    //清空下拉数据
    $("#model_id").html("");
    var str = "<option value=''>请选择</option>";
    if(null == oem_id){
        //将数据添加到这个下拉框里面
        $("#model_id").append(str);
        return;
    }
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotModel/list/"+oem_id,
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.model_id + "'>" + item.model_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#model_id").append(str);
            if(undefined != model_id && null != model_id && '' != model_id){
                $("#model_id").val(model_id);
            }
        },
        error:function(){}
    });
}


function getDeviceLocationList(){
    //清空下拉数据
    $("#device_location_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceLocation/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_location_id + "'>" + item.location_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_location_id").append(str);
        },
        error:function(){}
    });
}