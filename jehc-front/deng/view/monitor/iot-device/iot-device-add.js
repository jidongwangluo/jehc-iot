//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device/iot-device-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function addIotDevice(){
	submitBForm('defaultForm',monitorModules+'/iotDevice/add',base_html_redirect+'/monitor/iot-device/iot-device-list.html');
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    getOemList();
    getDeviceTypeList();
    getDeviceLocationList();
});



function getDeviceTypeList(){
    //清空下拉数据
    $("#device_type_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceType/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_type_id + "'>" + item.device_type_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_type_id").append(str);
        },
        error:function(){}
    });
}

function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}

function getModelList(oem_id){
    //清空下拉数据
    $("#model_id").html("");
    var str = "<option value=''>请选择</option>";
    if(null == oem_id){
        //将数据添加到这个下拉框里面
        $("#model_id").append(str);
        return;
    }
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotModel/list/"+oem_id,
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.model_id + "'>" + item.model_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#model_id").append(str);
        },
        error:function(){}
    });
}



function getDeviceLocationList(){
    //清空下拉数据
    $("#device_location_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceLocation/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_location_id + "'>" + item.location_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_location_id").append(str);
        },
        error:function(){}
    });
}