function appendFormInfo(cell){
    var baseContent =
        "<div class=\"m-portlet\" id='mportletId' style='height:160px;overflow: auto;width: 100%'>"+
            "<form class=\"m-form m-form--fit m-form--label-align-left m-form--group-seperator-dashed\" id=\"defaultForm\" method=\"post\">"+
                "<div class=\"m-portlet__body\">"+
                    //键
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"control-label\" >键</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" id='pixelKey' placeholder='暂无'  readonly>"+
                        "</div>"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"control-label\" >图元类别</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" id='pixelType' placeholder='暂无'  readonly>"+
                        "</div>"+

                    "</div>"+
                    //提示信息
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"control-label\" >提示信息</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" type=\"text\" placeholder='请输入' id=\"pixelMsg\">"+
                        "</div>"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"control-label\" >标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;签</label>"+
                        "</div>"+
                        "<div class=\"col-md-2\">"+
                            "<input class=\"form-control\" id='pixelLabel' placeholder='暂无'  type=\"text\" >"+
                        "</div>"+
                    "</div>"+
                    //绑定名称
                    "<div class=\"form-group row\">"+
                        "<div class=\"col-md-1\">"+
                            "<label class=\"control-label\" >绑定名称</label>"+
                        "</div>"+
                        "<div class=\"col-md-5\">"+
                            "<input class=\"form-control\" type=\"hidden\" id=\"pixelId\">"+
                            "<div class='form-group input-group'>" +
                                "<input class=\"form-control\" type=\"text\" maxlength=\"32\" readonly=\"readonly\" id =\"pixelName\"placeholder=\"请选择\">\n" +
                                "<span class=\"input-group-btn\">" +
                                    "<button class=\"btn btn-default\" type=\"button\" onclick=\"pixelSelect()\">选择</button>" +
                                "</span>" +
                            "</div>"+
                        "</div>"+
                    "</div>"+
                "</div>"+
            "</form>"+
        "</div>";
    var formInfo = "<div class='card-body'><div class='row' style='height:200px;overflow: auto;width: 100%'>"+ baseContent+ "</div></div>"
    $("#geSetContainer").empty();
	$("#geSetContainer").append(formInfo);
    initBaseFormData();
    nodeScroll();
}


function initFormInfo(cell,graph_refresh){
    appendFormInfo(cell);
}

/**
 * 设置Form基本信息
 */
function setBaseFormInfo(){
    var pixelKey = $("#pixelKey").val();
    var pixelType = $("#pixelType").val();
    var pixelMsg = $("#pixelMsg").val();
    var pixelLabel = $("#pixelLabel").val();
    var pixelId = $("#pixelId").val();
    var pixelName = $("#pixelName").val();

    $("#pixelKey").val(pixelKey);
    $("#pixelType").val(pixelType);
    $("#pixelMsg").val(pixelMsg);
    $("#pixelLabel").val(pixelLabel);
    $("#pixelId").val(pixelId);
    $("#pixelName").val(pixelName);

    try
    {
        var graph = new mxGraph();
        graph.getModel().beginUpdate();
        if(null != pixelKey && "" != pixelKey){
            JehcClickCell.pixelKey = pixelKey;
        }
        if(null != pixelType && "" != pixelType){
            JehcClickCell.pixelType = pixelType;
        }
        if(null != pixelMsg && "" != pixelMsg){
            JehcClickCell.pixelMsg = pixelMsg;
        }
        if(null != pixelLabel && "" != pixelLabel){
            JehcClickCell.pixelLabel = pixelLabel;
        }
        if(null != pixelId && '' != pixelId){
            JehcClickCell.pixelId = pixelId;
        }
        if(null != pixelName && '' != pixelName){
            JehcClickCell.pixelName = pixelName;
        }
        graph.startEditing();
    }
    finally
    {
        graph.getModel().endUpdate();
        graph_refresh.refresh();
    }
}

/**
 * 初始化基础Form数据
 */
function initBaseFormData(){
    if(null != JehcClickCell && undefined != JehcClickCell){
        var pixelKey = JehcClickCell.pixelKey;
        var pixelType = JehcClickCell.pixelType;
        var pixelMsg = JehcClickCell.pixelMsg;
        var pixelLabel = JehcClickCell.value;
        var pixelId = JehcClickCell.pixelId;;
        var pixelName = JehcClickCell.pixelName;
        $("#pixelKey").val(pixelKey);
        $("#pixelType").val(pixelType);
        $("#pixelMsg").val(pixelMsg);
        $("#pixelLabel").val(pixelLabel);
        $("#pixelId").val(pixelId);
        $("#pixelName").val(pixelName);
    }
}

//设备选择器
function pixelSelect(){
    var pixelSelectModalCount = 0 ;
    // $('#lcDesignPanelBody').height(reGetBodyHeight()*0.9);
    // $('#lcDesignModalLabel').html("在线设计---<font color=red>"+lc_process_title+"</font>");
    $('#pixelSelectModal').modal({backdrop:'static',keyboard:false});
    // $("#lcDesignIframe",document.body).attr("src",base_html_redirect+'/pc/lc-view/lc-design/lc-design.html?lc_process_id='+lc_process_id)
    $('#pixelSelectModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // // 是弹出框居中。。。
        // var $modal_dialog = $("#lcDesignModalDialog");
        // $modal_dialog.css({'width':reGetBodyWidth()*0.9+'px'});

        if(++pixelSelectModalCount == 1){
            $('#searchPixelForm')[0].reset();
            var opt = {
                searchformId:'searchPixelForm'
            };
            var options = DataTablesPaging.pagingOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,baseUrl+'/devices?substation=029A2A6CC5DD4CE8AF80D4B6D585B85B&type=&_=1565158464380',opt);},//渲染数据
                //在第一位置追加序列号
                fnRowCallback:function(nRow, aData, iDisplayIndex){
                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
                    return nRow;
                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
                    {
                        sClass:"text-center",
                        width:"20px",
                        data:"xt_userinfo_id",
                        render:function (data, type, full, meta) {
                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildDeletedUserinfo" value="' + data + '" /><span></span></label>';
                        },
                        bSortable:false
                    },
                    {
                        data:"xt_userinfo_id",
                        width:"20px"
                    },
                    {
                        data:'xt_userinfo_name'
                    },
                    {
                        data:'xt_userinfo_phone'
                    },
                    {
                        data:'xt_userinfo_origo'
                    },
                    {
                        data:'xt_userinfo_birthday'
                    },
                    {
                        data:'xt_userinfo_email'
                    }
                ]
            });
            grid=$('#pixelDataTables').dataTable(options);
            //实现单击行选中
            clickrowselected('pixelDataTables');
        }
    });
}