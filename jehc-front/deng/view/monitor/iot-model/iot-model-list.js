var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,monitorModules+'/iotModel/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"50px",
				data:"model_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"model_id",
				width:"50px"
			},
			{
				data:'model_name'
			},
			{
				data:'short_model_name'
			},
			{
				data:'oem_name'
			},
			{
				data:'createBy',
                width:"50px"
			},
			{
				data:'create_time',
                width:"50px"
			},
			{
				data:'modifiedBy',
                width:"50px"
			},
			{
				data:'update_time',
                width:"50px"
			},
			{
				data:"model_id",
				width:"150px",
				render:function(data, type, row, meta) {
                    var btn = '<button onclick=toIotModelDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情"><i class="flaticon-visible"></i></button>';
                    return btn;
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
    getOemList();
});
//新增
function toIotModelAdd(){
	tlocation(base_html_redirect+'/monitor/iot-model/iot-model-add.html');
}
//修改
function toIotModelUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+'/monitor/iot-model/iot-model-update.html?model_id='+id);
}
//详情
function toIotModelDetail(id){
	tlocation(base_html_redirect+'/monitor/iot-model/iot-model-detail.html?model_id='+id);
}
//删除
function delIotModel(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要删除的数据");
		return;
	}
	msgTishCallFnBoot("确定要删除所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {model_id:id,_method:'DELETE'};
		ajaxBReq(monitorModules+'/iotModel/delete',params,['datatables'],null,"DELETE");
	})
}

function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}