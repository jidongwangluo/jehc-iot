//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-model/iot-model-list.html');
}

$(document).ready(function(){
	var model_id = GetQueryString("model_id");
    getOemList();
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotModel/get/'+model_id,{},function(result){
        $('#model_id').val(result.data.model_id);
		$('#model_name').val(result.data.model_name);
		$('#short_model_name').val(result.data.short_model_name);
		$('#oem_id').val(result.data.oem_id);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
        $('#createBy').val(result.data.createBy);
        $('#modifiedBy').val(result.data.modifiedBy);
	});
});



function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}
