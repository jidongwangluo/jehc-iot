//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-model/iot-model-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function addIotModel(){
	submitBForm('defaultForm',monitorModules+'/iotModel/add',base_html_redirect+'/monitor/iot-model/iot-model-list.html');
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    getOemList();
});

function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}
