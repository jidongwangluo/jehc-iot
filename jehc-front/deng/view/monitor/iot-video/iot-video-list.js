var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,monitorModules+'/iotVideo/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"device_id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"device_id",
                width:"50px"
            },
            {
                data:'device_type_name'
            },
            {
                data:'device_code'
            },
            {
                data:'name'
            },
            {
                data:'short_name',
                width:"50px"
            },
            {
                data:'oem_name'
            },
            {
                data:'createBy',
                width:"50px"
            },
            {
                data:'create_time',
                width:"50px"
            },
            {
                data:'modifiedBy',
                width:"50px"
            },
            {
                data:'update_time',
                width:"50px"
            },
            {
                data:"device_id",
                width:"150px",
                render:function(data, type, row, meta) {
                    var name = row.name;
                    var tag = row.tag;
                    var btn = '<button onclick=play("'+data+'","'+name+'","'+tag+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="播放"><i class="la la-video-camera"></i></button>';
                    /**
                    btn = btn+'<button onclick=toIotDeviceConfig("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="配置属性"><i class="flaticon-settings-1"></i></button>';
                    **/
                     return btn;
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
    getOemList();
});

/*//播放
function play(id,name){
    var videoSelectModalCount = 0 ;
    $('#videoBody').height(reGetBodyHeight()*0.6);
    $('#videoModalLabel').html("<font color=red>"+name+"</font>");
    $('#videoModal').modal({backdrop:'static',keyboard:false});
    $('#videoModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++videoSelectModalCount == 1){
            $("#videoIframe",document.body).attr("src",base_html_redirect+'/monitor/iot-video/iot-video-play.html?device_id='+id)
        }
    });
}*/

function getOemList(){
    //清空下拉数据
    $("#oem_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotOem/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.oem_id + "'>" + item.oem_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#oem_id").append(str);
        },
        error:function(){}
    });
}

function getModelList(oem_id){
    //清空下拉数据
    $("#model_id").html("");
    var str = "<option value=''>请选择</option>";
    if(null == oem_id){
        //将数据添加到这个下拉框里面
        $("#model_id").append(str);
        return;
    }
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotModel/list/"+oem_id,
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.model_id + "'>" + item.model_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#model_id").append(str);
        },
        error:function(){}
    });
}

var videoElement = document.getElementById('videoElement');
var flvPlayer;
var httpURL;
var IntervalTimer;
var sameframesCount = 0;
var lastDecodedFrame = 0;
/**
 *
 */
function loadPlay(url){
    if (flvjs.isSupported()) {
        flvPlayer = flvjs.createPlayer({
           /*
            type: 'flv',
            url: url,
            fitVideoSize: 'auto',
            hasStart: true,
            autoplay: true, //自动播放，设置自动播放必要参数
            autoplayMuted: true, //自动播放静音，设置自动播放参数必要参数
            volume: 0,
            defaultMuted: true,
            isLive: true,
            playsinline: false,
            screenShot: false,
            fluid: true,
            aspectRatio: '16:9',
            whitelist: [''],
            ignores: ['time'],
            fixAudioTimestampGap: false,  //主要是这个配置，直播时音频的码率会被降低，直播游戏时背景音会有回响，但是说话声音没问题,默认值为：true。在网路允许的情况下，可以改为false。
            closeVideoClick: false,
            errorTips: '<span class="app-error">无视频源</span>',
            customConfig: {
                isClickPlayBack: false
            },
            flvOptionalConfig: {
                enableWorker: true, //是否多线程工作
                enableStashBuffer: false, //启用缓存
                stashInitialSize: 128, //默认384kb
                lazyLoad: false,
                lazyLoadMaxDuration: 40 * 60,
                autoCleanupSourceBuffer: true, //自动清除缓存
                autoCleanupMaxBackwardDuration: 35 * 60,
                autoCleanupMinBackwardDuration: 30 * 60,
                reuseRedirectedURL: true, //重用301/302重定向url，用于随后的请求，如查找、重新连接等。
            }
            */
            type: 'flv',
            isLive: true,
            hasAudio:false,
            hasVideo: true,
            url: url,
            fixAudioTimestampGap: false,  //主要是这个配置，直播时音频的码率会被降低，直播游戏时背景音会有回响，但是说话声音没问题,默认值为：true。在网路允许的情况下，可以改为false。
            enableWorker: true, // 是否多线程工作
            enableStashBuffer: false, // 是否启用缓存
            stashInitialSize: 256 // 缓存大小(kb)  默认384kb
        },{
            autoCleanupSourceBuffer:true,//对SourceBuffer进行自动清理
            autoCleanupMaxBackwardDuration:5,//    当向后缓冲区持续时间超过此值（以秒为单位）时，请对SourceBuffer进行自动清理
            autoCleanupMinBackwardDuration:2,//指示进行自动清除时为反向缓冲区保留的持续时间（以秒为单位）。
            enableStashBuffer: false, //关闭IO隐藏缓冲区
            isLive: true,
            lazyLoad: false,
        });
        flvPlayer.attachMediaElement(videoElement);
        flvPlayer.load(); //加载
        flvPlayer.play();
        $(".img-tu").css("display", "none");
        $(".load").hide();
    }

    setTimeout(function(){
        if(undefined != flvPlayer &&null != flvPlayer){
            flvPlayer.play();
        }
    }, 2000);

    IntervalTimer = setInterval(function(){
        if(undefined != flvPlayer &&null != flvPlayer){
            if (!videoElement.buffered.length) {
                return;
            }
            var end = videoElement.buffered.end(0);
            var diff = end - videoElement.currentTime;

            // videoElement.currentTime = videoElement.buffered.end(0);

             //暂时注释以下代码 采用上述时间追帧方式
            const diffCritical = 4; // 这里设定了超过4秒以上就进行跳转
            var diffSpeedUp = 0.5; // 这里设置了超过1秒以上则进行视频加速播放
            var maxPlaybackRate = 4;// 自定义设置允许的最大播放速度
            var playbackRate = 1.0; // 播放速度
            if (diff > diffCritical) {
               videoElement.currentTime = end-0.1;
               playbackRate = Math.max(1, Math.min(maxPlaybackRate, 16));
               console.log("相差超过4秒，进行跳转,加速度：",playbackRate,",相差时间：",diff-diffCritical);
            }else if(diff > diffSpeedUp){
               playbackRate = Math.max(1, Math.min(diff, maxPlaybackRate, 16))
               // console.log("相差超过1秒，进行加速,加速度：",playbackRate,",相差时间：",diff-diffSpeedUp);
            }
            videoElement.playbackRate = playbackRate;
            /*if (videoElement.paused) {
                flvPlayer.play()
            }*/
        }
    }, 1000);


    // flvPlayer.on('error', err => {
    //     console.log('err', err);
    // });



    /**
     * 监听媒体信息事件
     */
    flvPlayer.on(flvjs.Events.MEDIA_INFO, (res) => {
        console.log("媒体信息")
    });

    /**
     * 加载完成监听事件
     */
    flvPlayer.on(flvjs.Events.LOADING_COMPLETE, (res) => {
        // $(".loadMsg").css("display","flex");
        console.log("加载完成")
    });

    /**
     * 监听获取元数据事件
     */
    flvPlayer.on(flvjs.Events.METADATA_ARRIVED, (res) => {
        console.log("获取元数据")
    });

    /**
     * 监听恢复早期EOF事件
     */
    flvPlayer.on(flvjs.Events.RECOVERED_EARLY_EOF, (res) => {
        console.log("恢复早期EOF")
    });

    /**
     * 监听获取到脚本数据事件
     */
    flvPlayer.on(flvjs.Events.SCRIPTDATA_ARRIVED, (res) => {
        console.log("获取到脚本数据")
    });

    /**
     * 参考网上：
     * 视频直播时,有时候会遇到视频loading,卡顿,
     * 有可能是本地网络波动或者服务端流断开,
     * 我们可以通过监听flvjs.Events.ERROR来判断连接是否已经断开,继而进行断流重连,
     * 代码如下:
     */
    flvPlayer.on(flvjs.Events.ERROR, (errorType, errorDetail, errorInfo) => {
        console.log("errorType:", errorType);
        console.log("errorDetail:", errorDetail);
        console.log("errorInfo:", errorInfo);
        $(".loadMsg").css("display","flex");
        //视频出错后销毁重新创建
        if (flvPlayer) {
            flvPlayer.pause();
            flvPlayer.unload();
            flvPlayer.detachMediaElement();
            flvPlayer.destroy();
            flvPlayer= null;
            /*
            flvPlayer = flvjs.createPlayer(videoElement, this.url);
            */
            if(IntervalTimer){
                clearInterval(IntervalTimer);
            }
            loadPlay(httpURL)
        }
    });

    /**
    flvPlayer.on(flvjs.Events.MEDIA_SOURCE_CLOSE, (errorType, errorDetail, errorInfo) => {
        console.log("进入MEDIA_SOURCE_CLOSE.....", errorType);
        //视频出错后销毁重新创建
        if (flvPlayer) {
            flvPlayer.pause();
            flvPlayer.unload();
            flvPlayer.detachMediaElement();
            flvPlayer.destroy();
            flvPlayer= null;
            if(IntervalTimer){
                clearInterval(IntervalTimer);
            }
            loadPlay(httpURL)
        }
    });

    flvPlayer.on(flvjs.Events.VIDEO_RESOLUTION_CHANGED, (errorType, errorDetail, errorInfo) => {
        console.log("进入VIDEO_RESOLUTION_CHANGED.....", errorType);
        //视频出错后销毁重新创建
        if (flvPlayer) {
            flvPlayer.pause();
            flvPlayer.unload();
            flvPlayer.detachMediaElement();
            flvPlayer.destroy();
            flvPlayer= null;
            if(IntervalTimer){
                clearInterval(IntervalTimer);
            }
            loadPlay(httpURL)
        }
    });

    flvPlayer.on(flvjs.Events.VIDEO_FROZEN, (errorType, errorDetail, errorInfo) => {
        console.log("进入VIDEO_FROZEN.....", errorType);
        //视频出错后销毁重新创建
        if (flvPlayer) {
            flvPlayer.pause();
            flvPlayer.unload();
            flvPlayer.detachMediaElement();
            flvPlayer.destroy();
            flvPlayer= null;
            if(IntervalTimer){
                clearInterval(IntervalTimer);
            }
            loadPlay(httpURL)
        }
    });
    **/


    /**
     * 参考网上：
     * 如果控制台没有错误信息,而且查看network发现视频流没有断开,
     * 但是画面一直在loading或者卡住不动,我这边的原因是服务端推流突然断开,
     * 然后在很快的时间内继续推流,这个时候因为客户端的超时时间还没有结束,流会继续推送到原链接,
     * 这个时候我们的视频会卡在掉线的那个时间,不会继续播放.这个时候我们就需要监听推流的decodedFrame,
     * 如果decodedFrame不再发生变化,我们就销毁掉该实例并进行重新连接,
     * 代码如下:
     */
    flvPlayer.on("statistics_info", function (res) {
        if (lastDecodedFrame == 0) {
            // console.log("statistics_info=0");
            $(".loadMsg").css("display","flex");
            lastDecodedFrame = res.decodedFrames;
            return;
        }
        if (lastDecodedFrame != res.decodedFrames) {
            // console.log("statistics_info=1");
            $(".loadMsg").css("display","none");
            lastDecodedFrame = res.decodedFrames;
            sameframesCount = 0;
        } else {
            sameframesCount++;
            if(sameframesCount>=10){
                $(".loadMsg").css("display","flex");
                console.log("decodedFrames没有发生变化第",sameframesCount,"次，时间：",new Date(),"");
                console.log("statistics_info",res.decodedFrames,lastDecodedFrame);
                console.log("画面卡死，重新连接，时间：",new Date(),"");
                lastDecodedFrame = 0;
                sameframesCount = 0;
                if (flvPlayer) {
                    flvPlayer.pause();
                    flvPlayer.unload();
                    flvPlayer.detachMediaElement();
                    flvPlayer.destroy();
                    flvPlayer= null;
                    /*
                    flvPlayer = flvjs.createPlayer(videoElement, this.url);
                    */
                    if(IntervalTimer){
                        clearInterval(IntervalTimer);
                    }
                    loadPlay(httpURL);
                }
            }
        }
    });

}

/**
 * 开始播放
 */
function flv_start() {
    if( null != flvPlayer && undefined != flvPlayer){
        flvPlayer.play();
    }

}

/**
 * 暂停
 */
function flv_pause() {
    if( null != flvPlayer && undefined != flvPlayer){
        flvPlayer.pause();
    }
}

/**
 *
 */
function flv_seekto() {
    if(null != videoElement){
        videoElement.currentTime = new Date();
    }
}

//播放
function play(id,name,tag){
    if(tag == 'QCAM' || tag == 'YCAM'){
        ptzBase(false);
    }else{
        ptzBase(true);
    }
    var videoSelectModalCount = 0 ;
    $('#videoBody').height(450);
    $('#videoModalLabel').html("<font color=red>"+name+"</font>");
    $('#videoModal').modal({backdrop:'static',keyboard:false});
    $('#videoModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        if(++videoSelectModalCount == 1){
            ajaxBRequestCallFn(monitorModules+'/iotDevice/get/'+id,{},function(result){
                var transcribe = result.data.transcribe;
                if(transcribe == 1) {//当前状态为停止 则可开启录制
                    $("#transcribe").text('停止录屏');
                }else{
                    $("#transcribe").text('录屏');
                }
                ajaxBRequestCallFn(monitorModules+'/play/'+id,{},function(result){
                    httpURL = result.data.httpURL;
                    loadPlay(httpURL);
                    $("#device_id").val(id);
                });
            });
        }
    });
}

function closeWin(){
    $(".loadMsg").css("display","none");
    if(undefined != flvPlayer && null != flvPlayer){
        flvPlayer.pause();
        flvPlayer.unload();
        flvPlayer.detachMediaElement();
        flvPlayer.destroy();
        flvPlayer = null;
    }
    $('#videoModal').modal('hide');
    setTimeout(function(){
        $(".loadMsg").css("display","flex");
    },500)
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

/**
 * 云控
 */
function controlPTZ(start,pztType) {
    $('#start').val(start);
    $('#pztType').val(pztType);
    var bootform =  $('#defaultForm');
    var boostrapValidator =bootform.data('bootstrapValidator');
    boostrapValidator.validate();
    $.ajax({
        url:monitorModules+'/ptz/control',
        type:"POST",//PUT DELETE POST
        contentType:"application/json;charset=utf-8",
        timeout:1200000,//超时时间设置，单位毫秒（20分钟）
        data:JSON.stringify(bootform.serializeJSON()),//新版本写法
        success:function(result){

        },
        error:function(){
        }
    })
}

/**
 * 设置倍速
 * @param speed
 */
function setSpeed(speed) {
    $('#ptzSpeed').val(speed);
}

function ptzBase(hide) {
    if(hide){
        $("#ptzBase").css("display","none");//隐藏
    }else{
        $("#ptzBase").css("display","block");//显示
    }
}

/**
 * 录制视频 暂停视频
 */
function transcribeVideo(){
    var id = $("#device_id").val();
    ajaxBRequestCallFn(monitorModules+'/iotDevice/get/'+id,{},function(result){
        var transcribe = result.data.transcribe;
        if(transcribe == 0 || transcribe == null || transcribe == ''){//当前状态为停止 则可开启录制
            msgTishCallFnBoot("确定录制视频？",function(){
                ajaxBRequestCallFn(monitorModules+'/transcribe/'+id,{},function(result){
                    if(null != result.success && undefined != result.success && result.success){
                        $("#transcribe").text('停止录制');
                        toastrBoot(1,"录制成功");
                    }else{
                        toastrBoot(4,"录制失败");
                    }
                });
            });
        }
        if(transcribe == 1){
            msgTishCallFnBoot("确定暂停录制视频？",function(){
                ajaxBRequestCallFn(monitorModules+'/stopTranscribe/'+id,{},function(result){
                    if(null != result.success && undefined != result.success && result.success){
                        $("#transcribe").text('录屏');
                        toastrBoot(1,"停止录制成功");
                    }else{
                        toastrBoot(4,"停止录制失败");
                    }
                });
            });
        }
    });
}

/**
 * 截图
 */
function screenShot() {
    var id = $("#device_id").val();
    ajaxBRequestCallFn(monitorModules+'/screenShot/'+id,{},function(result){
        toastrBoot(1,"截图成功");
    });
}