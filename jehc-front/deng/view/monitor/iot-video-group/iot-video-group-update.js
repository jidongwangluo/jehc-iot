//返回
function goback(){
    tlocation(base_html_redirect+'/monitor/iot-video-group/iot-video-group-list.html');
}

$('#defaultForm').bootstrapValidator({
    message:'此值不是有效的'
});

//保存
function updateIotVideoGroup(){
    submitBForm('defaultForm',monitorModules+'/iotVideoGroup/update',base_html_redirect+'/monitor/iot-video-group/iot-video-group-list.html',null,"PUT");
}

//初始化日期选择器
$(document).ready(function(){
    datetimeInit();
});

$(document).ready(function(){
    var video_group_id = GetQueryString("video_group_id");
    //加载表单数据
    ajaxBRequestCallFn(monitorModules+'/iotVideoGroup/get/'+video_group_id,{},function(result){
        $('#video_group_id').val(result.data.video_group_id);
        $('#group_name').val(result.data.group_name);
        $('#remarks').val(result.data.remarks);
    });
});
