//返回
function goback(){
    tlocation(base_html_redirect+'/monitor/iot-video-group/iot-video-group-list.html');
}

$(document).ready(function(){
    var video_group_id = GetQueryString("video_group_id");
    //加载表单数据
    ajaxBRequestCallFn(monitorModules+'/iotVideoGroup/get/'+video_group_id,{},function(result){
        $('#video_group_id').val(result.data.video_group_id);
        $('#group_name').val(result.data.group_name);
        $('#remarks').val(result.data.remarks);
        $('#create_time').val(result.data.create_time);
        $('#update_time').val(result.data.update_time);
        $('#createBy').val(result.data.createBy);
        $('#modifiedBy').val(result.data.modifiedBy);
    });
});
