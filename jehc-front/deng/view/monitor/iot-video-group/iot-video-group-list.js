var grid;
$(document).ready(function() {
    /////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
    var opt = {
        searchformId:'searchForm'
    };
    var options = DataTablesPaging.pagingOptions({
        ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,monitorModules+'/iotVideoGroup/list',opt);},//渲染数据
        //在第一位置追加序列号
        fnRowCallback:function(nRow, aData, iDisplayIndex){
            jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
            return nRow;
        },
        order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
        //列表表头字段
        colums:[
            {
                sClass:"text-center",
                width:"50px",
                data:"video_group_id",
                render:function (data, type, full, meta) {
                    return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
                },
                bSortable:false
            },
            {
                data:"video_group_id",
                width:"50px"
            },
            {
                data:'group_name',
                width:"180px"
            },
            {
                data:'createBy',
                width:"50px"
            },
            {
                data:'create_time',
                width:"50px"
            },
            {
                data:'modifiedBy',
                width:"50px"
            },
            {
                data:'update_time',
                width:"50px"
            },
            {
                data:"video_group_id",
                render:function(data, type, row, meta) {
                    var group_name = row.group_name;
                    var btn = '<button onclick=toIotVideoGroupDetail("'+data+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="详情"><i class="la la-mobile"></i></button>';
                    btn = btn+ '<button onclick=videoSelect("'+data+'","'+group_name+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="查看相机"><i class="la la-wrench"></i></button>';
                    btn = btn+ '<button onclick=videoSelect("'+data+'","'+group_name+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="配置相机"><i class="la la-cogs"></i></button>';
                    return btn;
                }
            }
        ]
    });
    grid=$('#datatables').dataTable(options);
    //实现全选反选
    docheckboxall('checkall','checkchild');
    //实现单击行选中
    clickrowselected('datatables');
});
//新增
function toIotVideoGroupAdd(){
    tlocation(base_html_redirect+'/monitor/iot-video-group/iot-video-group-add.html');
}
//修改
function toIotVideoGroupUpdate(){
    if($(".checkchild:checked").length != 1){
        toastrBoot(4,"选择数据非法");
        return;
    }
    var id = $(".checkchild:checked").val();
    tlocation(base_html_redirect+'/monitor/iot-video-group/iot-video-group-update.html?video_group_id='+id);
}
//详情
function toIotVideoGroupDetail(id){
    tlocation(base_html_redirect+'/monitor/iot-video-group/iot-video-group-detail.html?video_group_id='+id);
}
//删除
function delIotVideoGroup(){
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要删除的数据");
        return;
    }
    msgTishCallFnBoot("确定要删除所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");
        var params = {video_group_id:id,_method:'DELETE'};
        ajaxBReq(monitorModules+'/iotVideoGroup/delete',params,['datatables'],null,"DELETE");
    })
}

function getDeviceTypeList(){
    //清空下拉数据
    $("#device_type_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceType/videoTypeList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_type_id + "'>" + item.device_type_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_type_id").append(str);
        },
        error:function(){}
    });
}

/**
 * 设备位置
 */
function getDeviceLocationList(){
    //清空下拉数据
    $("#device_location_id").html("");
    var str = "<option value=''>请选择</option>";
    $.ajax({
        type:"GET",
        url:monitorModules+"/iotDeviceLocation/allList",
        dataType:"JSON",
        async:false,
        success:function(data){
            data = data.data;
            //从服务器获取数据进行绑定
            $.each(data, function(i, item){
                str += "<option value='" + item.device_location_id + "'>" + item.location_name + "</option>";
            })
            //将数据添加到这个下拉框里面
            $("#device_location_id").append(str);
        },
        error:function(){}
    });
}


//相机设备选择器
function videoSelect(id,group_name){
    var videoSelectModalCount = 0 ;
    getDeviceLocationList();
    getDeviceTypeList();
    // $('#videoPanelBody').height(reGetBodyHeight()*0.9);
    $('#processBody').height(reGetBodyHeight()-128);
    $('#videoModalLabel').html("相机选择器---<font color=red>"+group_name+"</font>");
    $('#videoSelectModal').modal({backdrop:'static',keyboard:false});
    $('#videoSelectModal').modal({"backdrop":"static"}).modal('show').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#videoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
        //是弹出框居中。。。
        if(++videoSelectModalCount == 1){
            $('#searchVideoForm')[0].reset();
            var opt = {
                searchformId:'searchVideoForm'
            };
            var options = DataTablesList.listOptions({
                ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,monitorModules+'/iotVideoGroup/rightList',opt);},//渲染数据
                //在第一位置追加序列号
//                fnRowCallback:function(nRow, aData, iDisplayIndex){
//                    jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);
//                    return nRow;
//                },
                order:[],//取消默认排序查询,否则复选框一列会出现小箭头
                tableHeight:'120px',
                //列表表头字段
                colums:[
//                    {
//                        sClass:"text-center",
//                        width:"20px",
//                        data:"video_group_right_id",
//                        render:function (data, type, full, meta) {
//                            return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildDeletedUserinfo" value="' + data + '" /><span></span></label>';
//                        },
//                        bSortable:false
//                    },
//                    {
//                        data:"video_group_right_id",
//                        width:"20px"
//                    },
                    {
                        data:"device_id",
                        width:"80px",
                        render:function(data, type, row, meta) {
                            var id = row.video_group_right_id;
                            var name = row.name;
                            var btn = '<button onclick=remove("'+id+'","'+name+'") class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="删 除"><i class="la la-remove"></i></button>';
                            return btn;
                        }
                    },
                    {
                        data:'device_type_name'
                    },
                    {
                        data:"device_code",
                        width:"20px"
                    },
                    {
                        data:'name'
                    },
                    {
                        data:'location_name'
                    }
                ]
            });
            grid=$('#videoDataTables').dataTable(options);
            //实现单击行选中
            clickrowselected('videoDataTables');
        }
    });
}

function remove(id,name){
	msgTishCallFnBoot("确定选择该数据？",function(){
        var params = {video_group_right_id:id,_method:'DELETE'};
        ajaxBReq(monitorModules+'/iotVideoGroup/remove',params,['videoDataTables'],null,"DELETE");
	})
}