//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device-location/iot-device-location-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateIotDeviceLocation(){
	submitBForm('defaultForm',monitorModules+'/iotDeviceLocation/update',base_html_redirect+'/monitor/iot-device-location/iot-device-location-list.html',null,"PUT");
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

$(document).ready(function(){
	var device_location_id = GetQueryString("device_location_id");
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotDeviceLocation/get/'+device_location_id,{},function(result){
		$('#location_name').val(result.data.location_name);
		$('#remarks').val(result.data.remarks);
        $('#device_location_id').val(result.data.device_location_id);

	});
});
