//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device-location/iot-device-location-list.html');
}

$(document).ready(function(){
	var device_location_id = GetQueryString("device_location_id");
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotDeviceLocation/get/'+device_location_id,{},function(result){
		$('#location_name').val(result.data.location_name);
        $('#device_location_id').val(result.data.device_location_id);
		$('#createBy').val(result.data.createBy);
		$('#create_time').val(result.data.create_time);
		$('#modifiedBy').val(result.data.modifiedBy);
		$('#update_time').val(result.data.update_time);
        $('#remarks').val(result.data.remarks);

	});
});
