//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device-location/iot-device-location-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function addIotDeviceLocation(){
	submitBForm('defaultForm',monitorModules+'/iotDeviceLocation/add',base_html_redirect+'/monitor/iot-device-location/iot-device-location-list.html');
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

