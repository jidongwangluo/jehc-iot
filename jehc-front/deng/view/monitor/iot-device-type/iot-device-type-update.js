//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device-type/iot-device-type-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateIotDeviceType(){
	submitBForm('defaultForm',monitorModules+'/iotDeviceType/update',base_html_redirect+'/monitor/iot-device-type/iot-device-type-list.html',null,"PUT");
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

$(document).ready(function(){
	var device_type_id = GetQueryString("device_type_id");
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotDeviceType/get/'+device_type_id,{},function(result){
		$('#device_type_id').val(result.data.device_type_id);
		$('#device_type_name').val(result.data.device_type_name);
        $('#device_type_remarks').val(result.data.device_type_remarks);
        $('#tag').val(result.data.tag);
		$('#create_id').val(result.data.create_id);
		$('#create_time').val(result.data.create_time);
		$('#update_id').val(result.data.update_id);
		$('#update_time').val(result.data.update_time);
	});
});
