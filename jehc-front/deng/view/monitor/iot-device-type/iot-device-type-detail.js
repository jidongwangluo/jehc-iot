//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device-type/iot-device-type-list.html');
}

$(document).ready(function(){
	var device_type_id = GetQueryString("device_type_id");
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotDeviceType/get/'+device_type_id,{},function(result){
		$('#device_type_id').val(result.data.device_type_id);
		$('#device_type_name').val(result.data.device_type_name);
		$('#tag').val(result.data.tag);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
        $('#createBy').val(result.data.createBy);
        $('#modifiedBy').val(result.data.modifiedBy);

	});
});
