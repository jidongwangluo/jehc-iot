//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-device-type/iot-device-type-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function addIotDeviceType(){
	submitBForm('defaultForm',monitorModules+'/iotDeviceType/add',base_html_redirect+'/monitor/iot-device-type/iot-device-type-list.html');
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

