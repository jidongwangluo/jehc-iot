//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-oem/iot-oem-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function addIotOem(){
	submitBForm('defaultForm',monitorModules+'/iotOem/add',base_html_redirect+'/monitor/iot-oem/iot-oem-list.html');
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
    CallRegion(0);
    $('#xt_province_id_0').val($('#xt_provinceID_').val());
    getCity(0);
    $('#xt_city_id_0').val($('#xt_cityID_').val());
    getCounties(0);
    $('#xt_district_id_0').val($('#xt_districtID_').val());
});

