//返回
function goback(){
	tlocation(base_html_redirect+'/monitor/iot-oem/iot-oem-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateIotOem(){
	submitBForm('defaultForm',monitorModules+'/iotOem/update',base_html_redirect+'/monitor/iot-oem/iot-oem-list.html',null,"PUT");
}

//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});

$(document).ready(function(){
	var oem_id = GetQueryString("oem_id");
	//加载表单数据
	ajaxBRequestCallFn(monitorModules+'/iotOem/get/'+oem_id,{},function(result){
        $('#oem_id').val(result.data.oem_id);
		$('#oem_name').val(result.data.oem_name);
        $('#registration_date').val(result.data.registration_date);
        $('#oem_type').val(result.data.oem_type);
		$('#short_oem_name').val(result.data.short_oem_name);
		$('#oem_tel').val(result.data.oem_tel);
		$('#legal_person').val(result.data.legal_person);
        $("#xt_provinceID_").val(result.data.xt_provinceID);
        $("#xt_cityID_").val(result.data.xt_cityID);
        $("#xt_districtID_").val(result.data.xt_districtID);
        $("#xt_provinceID").val(result.data.xt_provinceID);
        $("#xt_cityID").val(result.data.xt_cityID);
        $("#xt_districtID").val(result.data.xt_districtID);
        CallRegion(0);
        $('#xt_province_id_0').val($('#xt_provinceID_').val());
        getCity(0);
        $('#xt_city_id_0').val($('#xt_cityID_').val());
        getCounties(0);
        $('#xt_district_id_0').val($('#xt_districtID_').val());
	});
});
