//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-key-info/oauth-key-info-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateOauthKeyInfo(){
	submitBForm('defaultForm',oauthModules+'/oauthKeyInfo/update',base_html_redirect+'/oauth/oauth-key-info/oauth-key-info-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var key_info_id = GetQueryString("key_info_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthKeyInfo/get/'+key_info_id,{},function(result){
		$('#key_info_id').val(result.data.key_info_id);
        $('#title').val(result.data.title);
		$('#key_name').val(result.data.key_name);
		$('#key_pass').val(result.data.key_pass);
		$('#key_exp_date').val(result.data.key_exp_date);
		$('#isUseExpDate').val(result.data.isUseExpDate);
		$('#status').val(result.data.status);
		$('#create_time').val(result.data.create_time);
		$('#last_update').val(result.data.last_update);

	});
});
