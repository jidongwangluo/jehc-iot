//返回
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-function-common/oauth-function-common-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateOauthFunctionCommon(){
	submitBForm('defaultForm',oauthModules+'/oauthFunctionCommon/update',base_html_redirect+'/oauth/oauth-function-common/oauth-function-common-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var function_common_id = GetQueryString("function_common_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthFunctionCommon/get/'+function_common_id,{},function(result){
		$('#function_common_id').val(result.data.function_common_id);
		$('#function_common_title').val(result.data.function_common_title);
		$('#function_common_url').val(result.data.function_common_url);
		$('#function_common_method').val(result.data.function_common_method);
		$('#function_common_status').val(result.data.function_common_status);
		$('#function_common_content').val(result.data.function_common_content);
		$('#sysmode_id').val(result.data.sysmode_id);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sysmode_id);
	});
});
