//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-sys-mode/oauth-sys-mode-list.html');
}
$(document).ready(function(){
	var sys_mode_id = GetQueryString("sys_mode_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthSysMode/get/'+sys_mode_id,{},function(result){
		$('#sys_mode_id').val(result.data.sys_mode_id);
		$('#sysmode').val(result.data.sysmode);
		$('#sysname').val(result.data.sysname);
		$('#sys_mode_status').val(result.data.sys_mode_status);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
        $('#sys_mode_icon').val(result.data.sys_mode_icon);
        $('#sys_mode_url').val(result.data.sys_mode_url);
        $('#sort').val(result.data.sort);
        initComboData("key_info_id",oauthModules+"/oauthKeyInfo/listAll","key_info_id","title",result.data.key_info_id);
	});
});
