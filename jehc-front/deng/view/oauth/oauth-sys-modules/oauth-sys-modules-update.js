//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-sys-modules/oauth-sys-modules-list.html');
}
$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});
//保存
function updateOauthSysModules(){
	submitBForm('defaultForm',oauthModules+'/oauthSysModules/update',base_html_redirect+'/oauth/oauth-sys-modules/oauth-sys-modules-list.html',null,"PUT");
}
//初始化日期选择器
$(document).ready(function(){
	datetimeInit();
});
$(document).ready(function(){
	var sys_modules_id = GetQueryString("sys_modules_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthSysModules/get/'+sys_modules_id,{},function(result){
		$('#sys_modules_id').val(result.data.sys_modules_id);
		$('#sys_modules_name').val(result.data.sys_modules_name);
		$('#sys_modules_mode').val(result.data.sys_modules_mode);
		$('#sys_modules_status').val(result.data.sys_modules_status);
		$('#sysmode_id').val(result.data.sysmode_id);
        $('#keyid').val(result.data.keyid);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sysmode_id);

    });
});
