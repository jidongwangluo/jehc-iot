//返回r
function goback(){
	tlocation(base_html_redirect+'/oauth/oauth-sys-modules/oauth-sys-modules-list.html');
}
$(document).ready(function(){
	var sys_modules_id = GetQueryString("sys_modules_id");
	//加载表单数据
	ajaxBRequestCallFn(oauthModules+'/oauthSysModules/get/'+sys_modules_id,{},function(result){
		$('#sys_modules_id').val(result.data.sys_modules_id);
		$('#sys_modules_name').val(result.data.sys_modules_name);
		$('#sys_modules_mode').val(result.data.sys_modules_mode);
		$('#sys_modules_status').val(result.data.sys_modules_status);
		$('#sysmode_id').val(result.data.sysmode_id);
        $('#keyid').val(result.data.keyid);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
        initComboData("sysmode_id",oauthModules+"/oauthSysMode/listAll","sys_mode_id","sysname",result.data.sysmode_id);

	});
});
