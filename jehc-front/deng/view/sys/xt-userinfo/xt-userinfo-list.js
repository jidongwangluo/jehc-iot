var grid;
$(document).ready(function() {
	/////////////jehc扩展属性目的可方便使用（boot.js文件datatablesCallBack方法使用） 如弹窗分页查找根据条件 可能此时的form发生变化 此时 可以解决该类问题
	var opt = {
		searchformId:'searchForm'
	};
	var options = DataTablesPaging.pagingOptions({
		ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/list',opt);},//渲染数据
			//在第一位置追加序列号
			fnRowCallback:function(nRow, aData, iDisplayIndex){
				jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
				return nRow;
		},
		order:[],//取消默认排序查询,否则复选框一列会出现小箭头
        tableHeight:datatablesDefaultHeight,
		//列表表头字段
		colums:[
			{
				sClass:"text-center",
				width:"20px",
				data:"xt_userinfo_id",
				render:function (data, type, full, meta) {
					return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchild " value="' + data + '" /><span></span></label>';
				},
				bSortable:false
			},
			{
				data:"xt_userinfo_id",
				width:"20px"
			},
			{
				data:'xt_userinfo_name'
			},
			{
				data:'xt_userinfo_realName'
			},
			{
				data:'xt_userinfo_phone'
			},
			{
				data:'xt_userinfo_state',
				render:function(data, type, row, meta) {
					var xt_data_dictionary_name="∨";
					if(data == null || data == ""){
						return xt_data_dictionary_name;
					}
					InitBDataCallFn(data,function(result){
					    xt_data_dictionary_name = result.xt_data_dictionary_name;
					    $("#datatables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(xt_data_dictionary_name);
//					    jQuery('td:eq('+meta.col+')', meta.row).html(xt_data_dictionary_name);   //通过异步渲染数据
					});
					return xt_data_dictionary_name;
				}
			},
			{
				data:'xt_userinfo_sex',
				render:function(data, type, row, meta) {
					var xt_data_dictionary_name="∨";
					if(data == null || data == ""){
						return xt_data_dictionary_name;
					}
					InitBDataCallFn(data,function(result){
					    xt_data_dictionary_name = result.xt_data_dictionary_name;
					    $("#datatables tbody > tr:eq("+meta.row+ ") > td:eq("+meta.col+")").html(xt_data_dictionary_name);
//					    jQuery('td:eq('+meta.col+')', meta.row).html(xt_data_dictionary_name); //通过异步渲染数据
					});
					return xt_data_dictionary_name;
				}
			},
			{
				data:'xt_userinfo_origo'
			},
			{
				data:'xt_userinfo_birthday'
			},
			{
				data:'xt_userinfo_email'
			},
			{
				data:"xt_userinfo_id",
				render:function(data, type, row, meta) {
					var xt_userinfo_name = row.xt_userinfo_name;
					var xt_userinfo_realName = row.xt_userinfo_realName;
					var btn = '<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=toXtUserinfoDetail("'+data+'") title="详情"><i class="la la-eye"></i></button> ';
						btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=initRoleinfo("'+data+'") title="角色权限"><i class="fa fa-user-plus"></i></button> ';
						btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=resetAccountPwd("'+data+'","'+xt_userinfo_realName+'","'+xt_userinfo_name+'") title="重置密码"><i class="fa fa-edit"></i></button>';
                    	// btn = btn +'<button class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" onclick=resetAccountPwd("'+data+'","'+xt_userinfo_realName+'","'+xt_userinfo_name+'") title="同步至授权中心"><i class="la la-arrow-up"></i></button>';
                    return btn;
				}
			}
		]
	});
	grid=$('#datatables').dataTable(options);
	//实现全选反选
	docheckboxall('checkall','checkchild');
	//实现单击行选中
	clickrowselected('datatables');
});
//新增
function toXtUserinfoAdd(){
	tlocation(base_html_redirect+'/sys/xt-userinfo/xt-userinfo-add.html');
}
//修改
function toXtUserinfoUpdate(){
	if($(".checkchild:checked").length != 1){
		toastrBoot(4,"选择数据非法");
		return;
	}
	var id = $(".checkchild:checked").val();
	tlocation(base_html_redirect+"/sys/xt-userinfo/xt-userinfo-update.html?xt_userinfo_id="+id);
}
//详情
function toXtUserinfoDetail(id){
	tlocation(base_html_redirect+'/sys/xt-userinfo/xt-userinfo-detail.html?xt_userinfo_id='+id);
}
//删除
function delXtUserinfo(){
	if(returncheckedLength('checkchild') <= 0){
		toastrBoot(4,"请选择要禁用的数据");
		return;
	}
	msgTishCallFnBoot("确定要禁用所选择的数据？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {xt_userinfo_id:id,_method:'DELETE'};
		ajaxBReq(sysModules+'/xtUserinfo/delete',params,['datatables'],null,"DELETE");
	})
}


//已禁用用户
function initListDeleted(){
	var deletedUserinfoSelectModalCount = 0 ;
    $('#deletedUserinfoBody').height(reGetBodyHeight()-218);
	$('#deletedUserinfoSelectModal').modal({backdrop: 'static', keyboard: false});
	$('#deletedUserinfoSelectModal').on("shown.bs.modal",function(){
        // 是弹出框居中。。。
        var $modal_dialog = $("#UserinfoModalDialog");
        $modal_dialog.css({'margin': 0 + 'px auto'});
        $modal_dialog.css({'width':reGetBodyWidth()+'px'});
		if(++deletedUserinfoSelectModalCount == 1){
			$('#searchFormDeletedUserinfo')[0].reset();
		    var opt = {
					searchformId:'searchFormDeletedUserinfo'
				};
			var options = DataTablesPaging.pagingOptions({
				ajax:function (data, callback, settings){datatablesCallBack(data, callback, settings,sysModules+'/xtUserinfo/deleted/list',opt);},//渲染数据
					//在第一位置追加序列号
					fnRowCallback:function(nRow, aData, iDisplayIndex){
						jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
						return nRow;
				},
				order:[],//取消默认排序查询,否则复选框一列会出现小箭头
				tableHeight:'120px',
				//列表表头字段
				colums:[
					{
						sClass:"text-center",
						width:"20px",
						data:"xt_userinfo_id",
						render:function (data, type, full, meta) {
							return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildDeletedUserinfo" value="' + data + '" /><span></span></label>';
						},
						bSortable:false
					},
					{
						data:"xt_userinfo_id",
						width:"20px"
					},
					{
						data:'xt_userinfo_name'
					},
					{
						data:'xt_userinfo_realName'
					},
					{
						data:'xt_userinfo_phone'
					},
					{
						data:'xt_userinfo_origo'
					},
					{
						data:'xt_userinfo_birthday'
					},
					{
						data:'xt_userinfo_email'
					}
				]
			});
			grid=$('#deletedUserinfoDatatables').dataTable(options);
			//实现全选反选
			docheckboxall('checkallDeletedUserinfo','checkchildDeletedUserinfo');
			//实现单击行选中
			clickrowselected('deletedUserinfoDatatables');
		}
	})
}

//恢复用户
function recoverXtUserinfo(){
	if(returncheckedLength('checkchildDeletedUserinfo') <= 0){
		toastrBoot(4,"请选择要恢复的用户");
		return;
	}
	msgTishCallFnBoot("确定要恢复所选择的用户？",function(){
		var id = returncheckIds('checkId').join(",");
		var params = {xt_userinfo_id:id};
		ajaxBRequestCallFn(sysModules+'/xtUserinfo/recover',params,function(result){
    		if(typeof(result.success) != "undefined"){
    			$('#deletedUserinfoSelectModal').modal('hide');
    			search('datatables');
    		}
		},null,"GET");
	})
}

//角色权限
function initRoleinfo(id){
	var userRoleModalCount = 0;
	$('#userRoleModal').modal({backdrop: 'static', keyboard: false});
	$('#userRoleModal').on("shown.bs.modal",function(){  
		if(++userRoleModalCount == 1){
            var opt = {
                searchformId:'searchFormRole'
            };
			var options = DataTablesList.listOptions({
				ajax:function (data, callback, settings){datatablesListCallBack(data, callback, settings,oauthModules+'/oauthRole/roleList/'+id,opt);},//渲染数据
					//在第一位置追加序列号
					fnRowCallback:function(nRow, aData, iDisplayIndex){
						jQuery('td:eq(1)', nRow).html(iDisplayIndex +1);  
						return nRow;
				},
				tableHeight:'200px',
				order:[],//取消默认排序查询,否则复选框一列会出现小箭头
				//列表表头字段
				colums:[
					{
						sClass:"text-center",
						data:"account_role_id",
						width:'20px',
						render:function (data, type, full, meta) {
							return '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="checkId" class="checkchildUserRole" value="' + data + '" /><span></span></label>';
						},
						bSortable:false
					},
					{
						width:'20px',
						data:"account_role_id"
					},
					{
						data:'role_name'
					}
				]
			});
			$('#userRoleDatatables').dataTable(options);
			//实现全选反选
			docheckboxall('checkallUserRole','checkchildUserRole');
			//实现单击行选中
			clickrowselected('userRoleDatatables');
		}
	});
}

//重置密码
function resetAccountPwd(id,account,name){
    msgTishCallFnBoot('确定要重置<br>姓名：'+name+'<br>登录账户：'+account+'的密码？',function(){
        var params = {account_id:id,account:account};
        ajaxBReq(oauthModules+'/oauthAccount/restPwd',params,['datatables'],null,"POST");
    })
}

/**
 * 同步信息之授权中心
 */
function syncOauth(id,name) {
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要同步的数据");
        return;
    }
    var oauthModalCount = 0;
    $('#oauthModal').modal({backdrop: 'static', keyboard: false});
    $('#oauthModal').on("shown.bs.modal",function(){
        if(++oauthModalCount == 1){
            if(returncheckedLength('checkchild') == 1){
                var id = returncheckIds('checkId').join(",")
                ajaxBRequestCallFn(oauthModules+"/oauthAccount/get/"+id,{},function(result){
                    var account_type_id = result.data.account_type_id;
                    initComboDataCallFn("account_type_idTemp",oauthModules+"/oauthAccountType/listAll","account_type_id","title",null,function (data) {
                        $("#account_type_idTemp").select2({
                            width: "100%", //设置下拉框的宽度
                            placeholder: "暂无账户类型", // 空值提示内容，选项值为 null
                            tags:true,
                            /*allowClear:!0,*/
                            createTag:function (decorated, params) {
                                return null;
                            }
                        });
                        if(null != account_type_id && '' != account_type_id){
                            var account_type_idArray = account_type_id.split(",");//注意：arr为select的id值组成的数组
                            $('#account_type_idTemp').val(account_type_idArray).trigger('change');
						}
                    });

                });
			}
            initComboDataCallFn("account_type_ids",oauthModules+"/oauthAccountType/listAll","account_type_id","title",null,function (data) {
                $("#account_type_ids").select2({
                    width: "100%", //设置下拉框的宽度
                    placeholder: "请选择账号类型", // 空值提示内容，选项值为 null
                    tags:true,
                    /*allowClear:!0,*/
                    createTag:function (decorated, params) {
                        return null;
                    }
                });
            });
        }
    });
}

/**
 * 同步数据
 */
function doSyncOauth() {
    if(returncheckedLength('checkchild') <= 0){
        toastrBoot(4,"请选择要同步的数据");
        return;
    }
    var account_type_ids = $('#account_type_ids').val();
    var ids = null;
    if(null != account_type_ids){
        for(var i = 0; i < account_type_ids.length; i++){
            if(ids ==  null){
                ids = account_type_ids[i];
            }else{
                ids =ids+ ","+account_type_ids[i];
            }
        }
    }

    if($("#sync").val()==1){
    	if(ids == null || ids == ""){
            window.parent.toastrBoot(4,"请选择账号类型");
    		return;
		}
	}
    msgTishCallFnBoot("确定要同步所选择的数据？",function(){
        var id = returncheckIds('checkId').join(",");

        var params = {xt_userinfo_id:id,account_type_id:ids,sync:$("#sync").val()};
        ajaxBRequestCallFn(sysModules+'/xtUserinfo/syncOauth',params,function(result){
            $('#oauthModal').modal("hide");
            search('datatables');
            try {
                if(typeof(result.success) != "undefined"){
                    if(result.success){
                        window.parent.toastrBoot(3,result.message);
                    }else{
                        //失败还在原位置页面
                        window.parent.toastrBoot(4,result.message);
                    }
                }
            } catch (e) {

            }
		},null,"POST");
    })
}


function checkSync() {
    if ($("#syncType").is(":checked")) {
        $("#account_type_id_sync").css("display","block");
        $("#sync").val(1);
    } else {
        $("#account_type_id_sync").css("display","none");
        $("#sync").val(0);
    }
}