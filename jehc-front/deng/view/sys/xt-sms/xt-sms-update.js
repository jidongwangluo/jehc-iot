//返回r
function goback(){
	tlocation(base_html_redirect+'/sys/xt-sms/xt-sms-list.html');
}

$('#defaultForm').bootstrapValidator({
	message:'此值不是有效的'
});

//保存
function updateXtSms(){
	submitBForm('defaultForm',sysModules+'/xtSms/update',base_html_redirect+'/sys/xt-sms/xt-sms-list.html',null,"PUT");
}


$(document).ready(function(){
	var xt_sms_id = GetQueryString("xt_sms_id");
	//加载表单数据
    ajaxBRequestCallFn(sysModules+"/xtSms/get/"+xt_sms_id,{},function(result){
        $("#xt_sms_id").val(result.data.xt_sms_id);
        $("#name").val(result.data.name);
        $("#password").val(result.data.password);
        $("#url").val(result.data.url);
        $("#company").val(result.data.company);
        $("#tel").val(result.data.tel);
        $("#address").val(result.data.address);
        $("#value").val(result.data.value);
        $("#contacts").val(result.data.contacts);
        $("#type").val(result.data.type);
    });
});
