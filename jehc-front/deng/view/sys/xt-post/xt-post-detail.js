//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-post/xt-post-list.html');
}


$(document).ready(function(){
	var xt_post_id = GetQueryString("xt_post_id");
	//加载表单数据
	ajaxBRequestCallFn(sysModules+"/xtPost/get/"+xt_post_id,{},function(result){
	    $("#xt_departinfo_id").val(result.data.xt_departinfo_id);
	    $("#xt_departinfo_name").val(result.data.xt_departinfo_name);
        $("#p_code").val(result.data.p_code);
	    $("#xt_post_id").val(result.data.xt_post_id);
	    $("#xt_post_parentId").val(result.data.xt_post_parentId);
	    $("#xt_post_desc").val(result.data.xt_post_desc);
	    $("#xt_post_name").val(result.data.xt_post_name);
	    $("#xt_post_maxNum").val(result.data.xt_post_maxNum);
	    $("#xt_post_grade").val(result.data.xt_post_grade);
        $('#create_id').val(result.data.createBy);
        $('#create_time').val(result.data.create_time);
        $('#update_id').val(result.data.modifiedBy);
        $('#update_time').val(result.data.update_time);
	    if($('#xt_post_parentId').val() != "" && $('#xt_post_parentId').val() != null && $('#xt_post_parentId').val() != 0){
	    	 $.ajax({
	  		   type:"GET",
	  		   url:sysModules+"/xtPost/get/"+$('#xt_post_parentId').val(),
	  		   success: function(res){
	  			   if(res != null && res != ''){
	  				   $('#xt_post_parentName').val(res.data.xt_post_name);
	  			   }else{
	  				   $('#xt_post_parentName').val("无");
	  				   $('#xt_post_parentId').val("0");
	  			   }
	  		   }
	  		});
	    }
	});

});