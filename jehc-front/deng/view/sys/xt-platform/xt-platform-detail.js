//返回
function goback(){
	tlocation(base_html_redirect+'/sys/xt-platform/xt-platform-list.html');
}
$(document).ready(function(){
	var xt_platform_id = GetQueryString("xt_platform_id");
	//加载表单数据
	ajaxBRequestCallFn(sysModules+'/xtPlatform/get/'+xt_platform_id,{},function(result){
		$('#xt_platform_id').val(result.data.xt_platform_id);
		$('#title').val(result.data.title);
		$('#status').val(result.data.status);
        $('#remark').val(result.data.remark);
		$('#create_id').val(result.data.createBy);
		$('#update_id').val(result.data.modifiedBy);
		$('#create_time').val(result.data.create_time);
		$('#update_time').val(result.data.update_time);
	});
});
