package jehc.djshi.iot.monitor.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.common.util.RestTemplateUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.live.model.CameraEntity;
import jehc.djshi.iot.live.service.PlayService;
import jehc.djshi.iot.live.service.PtzService;
import jehc.djshi.iot.live.unvif.entity.UnVifInfo;
import jehc.djshi.iot.monitor.model.IotDevice;
import jehc.djshi.iot.monitor.model.IotDeviceConfig;
import jehc.djshi.iot.monitor.service.IotDeviceConfigService;
import jehc.djshi.iot.monitor.service.IotDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
/**
 * @Desc 播放控制
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@Api(value = "播放控制",tags = "播放控制", description = "播放控制")
public class IotPlayController extends BaseAction {
    @Autowired
    private IotDeviceService iotDeviceService;

    @Autowired
    private IotDeviceConfigService iotDeviceConfigService;

    @Autowired
    RestTemplateUtil restTemplateUtil;

    @Autowired
    PlayService playService;

    @Autowired
    PtzService ptzService;

    /**
     * 播放视频
     * @param device_id
     */
    @ApiOperation(value="播放视频", notes="播放视频")
    @NeedLoginUnAuth
    @GetMapping(value="/play/{device_id}")
    public BaseResult getIotDeviceById(@PathVariable("device_id")String device_id,HttpServletRequest request){
        if(StringUtil.isEmpty(device_id)){
            throw new ExceptionUtil("未能获取到设备id");
        }
        IotDevice iotDevice = iotDeviceService.getIotDeviceById(device_id);
        if(null == iotDevice){
            throw new ExceptionUtil("未能获取到设备对象");
        }
        IotDeviceConfig iotDeviceConfig = iotDeviceConfigService.getIotDeviceConfigByDeviceId(iotDevice.getDevice_id());
        if(null == iotDeviceConfig){
            throw new ExceptionUtil("未能获取到设备配置");
        }
        CameraEntity cameraEntity = new CameraEntity();
        cameraEntity.setStream(iotDeviceConfig.getDevice_id());
        cameraEntity.setIp(iotDeviceConfig.getIp());
        cameraEntity.setPassword(iotDeviceConfig.getPassword());
        cameraEntity.setPort(iotDeviceConfig.getRtsp_port());
        cameraEntity.setUsername(iotDeviceConfig.getUser_name());
        cameraEntity.setChannel(""+iotDeviceConfig.getChannel_no());
        cameraEntity.setType(""+iotDeviceConfig.getType());
        cameraEntity.setSubtype(iotDeviceConfig.getSubtype());
        BaseResult baseResult = playService.play(cameraEntity);
        return baseResult;
    }


    /**
     * 云台控制
     * @param unVifInfo
     */
    @ApiOperation(value="云台控制", notes="云台控制")
    @NeedLoginUnAuth
    @PostMapping(value="/ptz/control")
    public BaseResult control(@RequestBody UnVifInfo unVifInfo ,HttpServletRequest request){
        if(StringUtil.isEmpty(unVifInfo.getDevice_id())){
            throw new ExceptionUtil("未能获取到设备id");
        }
        IotDevice iotDevice = iotDeviceService.getIotDeviceById(unVifInfo.getDevice_id());
        if(null == iotDevice){
            throw new ExceptionUtil("未能获取到设备对象");
        }
        IotDeviceConfig iotDeviceConfig = iotDeviceConfigService.getIotDeviceConfigByDeviceId(iotDevice.getDevice_id());
        if(null == iotDeviceConfig){
            throw new ExceptionUtil("未能获取到设备配置");
        }
        unVifInfo.setIp(iotDeviceConfig.getIp());
        unVifInfo.setPassword(iotDeviceConfig.getPassword());
        unVifInfo.setPort(iotDeviceConfig.getPort());
        unVifInfo.setUserName(iotDeviceConfig.getUser_name());
        BaseResult baseResult = ptzService.control(unVifInfo);
        return baseResult;
    }


    /**
     * 录制视频
     * @param device_id
     */
    @ApiOperation(value="录制视频", notes="录制视频")
    @NeedLoginUnAuth
    @GetMapping(value="/transcribe/{device_id}")
    public BaseResult transcribe(@PathVariable("device_id")String device_id,HttpServletRequest request){
        if(StringUtil.isEmpty(device_id)){
            throw new ExceptionUtil("未能获取到设备id");
        }
        IotDevice iotDevice = iotDeviceService.getIotDeviceById(device_id);
        if(null == iotDevice){
            throw new ExceptionUtil("未能获取到设备对象");
        }

        if(null != iotDevice.getTranscribe()){
            if(iotDevice.getTranscribe() == 1){
                throw new ExceptionUtil("已经录制中,请停止后再录制！");
            }
        }

        IotDeviceConfig iotDeviceConfig = iotDeviceConfigService.getIotDeviceConfigByDeviceId(iotDevice.getDevice_id());
        if(null == iotDeviceConfig){
            throw new ExceptionUtil("未能获取到设备配置");
        }
        CameraEntity cameraEntity = new CameraEntity();
        cameraEntity.setVideoName(iotDevice.getName()+"-"+getSimpleDateFormatT());
        cameraEntity.setVideoType("mp4");
        cameraEntity.setStream(iotDeviceConfig.getDevice_id());
        cameraEntity.setIp(iotDeviceConfig.getIp());
        cameraEntity.setPassword(iotDeviceConfig.getPassword());
        cameraEntity.setPort(iotDeviceConfig.getRtsp_port());
        cameraEntity.setPath(getXtPathCache("VIDEO_UPLOAD_PATH").get(0).getXt_path());
        cameraEntity.setUsername(iotDeviceConfig.getUser_name());
        cameraEntity.setChannel(""+iotDeviceConfig.getChannel_no());
        cameraEntity.setAudio(0);
        cameraEntity.setControlTranscribe(true);
        cameraEntity.setType(""+iotDeviceConfig.getType());
        cameraEntity.setSubtype(iotDeviceConfig.getSubtype());
        BaseResult baseResult = playService.transcribe(cameraEntity);
        if(baseResult.getSuccess()){
            IotDevice iotDevicePam = new IotDevice();
            iotDevicePam.setDevice_id(device_id);
            iotDevicePam.setTranscribe(1);
            iotDeviceService.updateIotDeviceBySelective(iotDevicePam);
        }
        return baseResult;
    }

    /**
     * 停止录制
     * @param device_id
     */
    @ApiOperation(value="停止录制", notes="停止录制")
    @NeedLoginUnAuth
    @GetMapping(value="/stopTranscribe/{device_id}")
    public BaseResult stopTranscribe(@PathVariable("device_id")String device_id,HttpServletRequest request){
        if(StringUtil.isEmpty(device_id)){
            throw new ExceptionUtil("未能获取到设备id");
        }
        IotDevice iotDevice = iotDeviceService.getIotDeviceById(device_id);
        if(null == iotDevice){
            throw new ExceptionUtil("未能获取到设备对象");
        }
        if("1".equals(iotDevice.getTranscribe())){
            throw new ExceptionUtil("已经停止录制！");
        }
        CameraEntity cameraEntity = new CameraEntity();
        cameraEntity.setStream(device_id);
        cameraEntity.setControlTranscribe(false);
        BaseResult baseResult = playService.stopTranscribe(cameraEntity);
        if(baseResult.getSuccess()){
            IotDevice iotDevicePam = new IotDevice();
            iotDevicePam.setDevice_id(device_id);
            iotDevicePam.setTranscribe(0);
            iotDeviceService.updateIotDeviceBySelective(iotDevicePam);
        }
        return baseResult;
    }

    /**
     * 截图
     * @param device_id
     */
    @ApiOperation(value="截图", notes="截图")
    @NeedLoginUnAuth
    @GetMapping(value="/screenShot/{device_id}")
    public BaseResult screenShot(@PathVariable("device_id")String device_id,HttpServletRequest request){
        if(StringUtil.isEmpty(device_id)){
            throw new ExceptionUtil("未能获取到设备id");
        }
        IotDevice iotDevice = iotDeviceService.getIotDeviceById(device_id);
        if(null == iotDevice){
            throw new ExceptionUtil("未能获取到设备对象");
        }
        IotDeviceConfig iotDeviceConfig = iotDeviceConfigService.getIotDeviceConfigByDeviceId(iotDevice.getDevice_id());
        if(null == iotDeviceConfig){
            throw new ExceptionUtil("未能获取到设备配置");
        }
        CameraEntity cameraEntity = new CameraEntity();
        cameraEntity.setStream(device_id);
        cameraEntity.setIp(iotDeviceConfig.getIp());
        cameraEntity.setPassword(iotDeviceConfig.getPassword());
        cameraEntity.setPort(iotDeviceConfig.getRtsp_port());
        cameraEntity.setPath(getXtPathCache("VIDEO_IMAGE_UPLOAD_PATH").get(0).getXt_path());
        cameraEntity.setUsername(iotDeviceConfig.getUser_name());
        cameraEntity.setChannel(""+iotDeviceConfig.getChannel_no());
        cameraEntity.setType(""+iotDeviceConfig.getType());
        cameraEntity.setImageType("jpg");
        cameraEntity.setSubtype(iotDeviceConfig.getSubtype());
        cameraEntity.setVideoName(iotDevice.getName()+"-"+getSimpleDateFormatT());
        BaseResult baseResult = playService.screenShot(cameraEntity);
        return baseResult;
    }
}
