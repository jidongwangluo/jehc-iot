package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotModelDao;
import jehc.djshi.iot.monitor.model.IotModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.monitor.service.IotModelService;
/**
 * @Desc 设备型号
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotModelService")
public class IotModelServiceImpl extends BaseService implements IotModelService{
	@Autowired
	private IotModelDao iotModelDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<IotModel> getIotModelListByCondition(Map<String,Object> condition){
		try{
			return iotModelDao.getIotModelListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param model_id 
	* @return
	*/
	public IotModel getIotModelById(String model_id){
		try{
			IotModel iotModel = iotModelDao.getIotModelById(model_id);
			return iotModel;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param iotModel 
	* @return
	*/
	public int addIotModel(IotModel iotModel){
		int i = 0;
		try {
			iotModel.setCreate_id(getXtUid());
			i = iotModelDao.addIotModel(iotModel);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param iotModel 
	* @return
	*/
	public int updateIotModel(IotModel iotModel){
		int i = 0;
		try {
			iotModel.setUpdate_id(getXtUid());
			i = iotModelDao.updateIotModel(iotModel);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param iotModel 
	* @return
	*/
	public int updateIotModelBySelective(IotModel iotModel){
		int i = 0;
		try {
			iotModel.setUpdate_id(getXtUid());
			i = iotModelDao.updateIotModelBySelective(iotModel);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delIotModel(Map<String,Object> condition){
		int i = 0;
		try {
			i = iotModelDao.delIotModel(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param iotModelList 
	* @return
	*/
	public int updateBatchIotModel(List<IotModel> iotModelList){
		int i = 0;
		try {
			i = iotModelDao.updateBatchIotModel(iotModelList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param iotModelList 
	* @return
	*/
	public int updateBatchIotModelBySelective(List<IotModel> iotModelList){
		int i = 0;
		try {
			i = iotModelDao.updateBatchIotModelBySelective(iotModelList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
