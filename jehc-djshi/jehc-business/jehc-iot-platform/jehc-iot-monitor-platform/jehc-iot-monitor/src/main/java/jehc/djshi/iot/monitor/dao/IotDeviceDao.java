package jehc.djshi.iot.monitor.dao;
import java.util.List;
import java.util.Map;

import jehc.djshi.iot.monitor.model.IotDevice;

/**
 * @Desc 设备基础信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotDeviceDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<IotDevice> getIotDeviceListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param device_id 
	* @return
	*/
	IotDevice getIotDeviceById(String device_id);
	/**
	* 添加
	* @param iotDevice 
	* @return
	*/
	int addIotDevice(IotDevice iotDevice);
	/**
	* 修改
	* @param iotDevice 
	* @return
	*/
	int updateIotDevice(IotDevice iotDevice);
	/**
	* 修改（根据动态条件）
	* @param iotDevice 
	* @return
	*/
	int updateIotDeviceBySelective(IotDevice iotDevice);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delIotDevice(Map<String, Object> condition);
	/**
	* 批量修改
	* @param iotDeviceList 
	* @return
	*/
	int updateBatchIotDevice(List<IotDevice> iotDeviceList);
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceList 
	* @return
	*/
	int updateBatchIotDeviceBySelective(List<IotDevice> iotDeviceList);

	/**
	 * 视频
	 * @param condition
	 * @return
	 */
	List<IotDevice> getIotVideoListByCondition(Map<String, Object> condition);
}
