package jehc.djshi.iot.monitor.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.iot.monitor.model.IotModel;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.iot.monitor.service.IotModelService;
/**
 * @Desc 设备型号
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotModel")
@Api(value = "设备型号",tags = "设备型号", description = "设备型号")
public class IotModelController extends BaseAction{
	@Autowired
	private IotModelService iotModelService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getIotModelListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<IotModel> iotModelList = iotModelService.getIotModelListByCondition(condition);
		for(IotModel iotModel:iotModelList){
			if(!StringUtil.isEmpty(iotModel.getCreate_id())){
				OauthAccountEntity createBy = getAccount(iotModel.getCreate_id());
				if(null != createBy){
					iotModel.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(iotModel.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(iotModel.getUpdate_id());
				if(null != modifiedBy){
					iotModel.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<IotModel> page = new PageInfo<IotModel>(iotModelList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param model_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{model_id}")
	public BaseResult getIotModelById(@PathVariable("model_id")String model_id){
		IotModel iotModel = iotModelService.getIotModelById(model_id);
		if(!StringUtil.isEmpty(iotModel.getCreate_id())){
			OauthAccountEntity createBy = getAccount(iotModel.getCreate_id());
			if(null != createBy){
				iotModel.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(iotModel.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(iotModel.getUpdate_id());
			if(null != modifiedBy){
				iotModel.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(iotModel);
	}
	/**
	* 添加
	* @param iotModel 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addIotModel(@RequestBody IotModel iotModel){
		int i = 0;
		if(null != iotModel){
			iotModel.setCreate_time(getDate());
			iotModel.setModel_id(toUUID());
			i=iotModelService.addIotModel(iotModel);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param iotModel 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateIotModel(@RequestBody IotModel iotModel){
		int i = 0;
		if(null != iotModel){
			iotModel.setUpdate_time(getDate());
			i=iotModelService.updateIotModel(iotModel);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param model_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delIotModel(String model_id){
		int i = 0;
		if(!StringUtil.isEmpty(model_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("model_id",model_id.split(","));
			i=iotModelService.delIotModel(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 根据生产商查找
	 * @param oem_id
	 */
	@ApiOperation(value="根据生产商查找", notes="根据生产商查找")
	@NeedLoginUnAuth
	@GetMapping(value="/list/{oem_id}")
	public BaseResult listByOemId(@PathVariable("oem_id")String oem_id){
		Map<String,Object> condition = new HashMap<>();
		condition.put("oem_id",oem_id);
		List<IotModel> iotModelList = iotModelService.getIotModelListByCondition(condition);
		return new BaseResult(iotModelList);
	}
}
