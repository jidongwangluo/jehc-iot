package jehc.djshi.iot.monitor.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.monitor.model.GraphSite;
import jehc.djshi.iot.monitor.service.GraphSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 站点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/graphSite")
@Api(value = "站点",tags = "站点", description = "站点")
public class GraphSiteController extends BaseAction {
	@Autowired
	private GraphSiteService graphSiteService;

	/**
	* 查询并分页
	* @param baseSearch
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@PostMapping(value="/list")
	@NeedLoginUnAuth
	public BasePage getGraphSiteListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<GraphSite> graphSiteList = graphSiteService.getGraphSiteListByCondition(condition);
		for(GraphSite graphSite:graphSiteList){
			if(!StringUtil.isEmpty(graphSite.getXt_userinfo_id())){
				OauthAccountEntity createBy = getAccount(graphSite.getXt_userinfo_id());
				if(null != createBy){
					graphSite.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(graphSite.getUpdate_userinfo_id())){
				OauthAccountEntity modifiedBy = getAccount(graphSite.getUpdate_userinfo_id());
				if(null != modifiedBy){
					graphSite.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<GraphSite> page = new PageInfo<GraphSite>(graphSiteList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param graph_site_id
	*/
	@NeedLoginUnAuth
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@GetMapping(value="/get/{graph_site_id}")
	public BaseResult getGraphSiteById(@PathVariable("graph_site_id")String graph_site_id){
		GraphSite graphSite = graphSiteService.getGraphSiteById(graph_site_id);
		if(!StringUtil.isEmpty(graphSite.getXt_userinfo_id())){
			OauthAccountEntity createBy = getAccount(graphSite.getXt_userinfo_id());
			if(null != createBy){
				graphSite.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(graphSite.getUpdate_userinfo_id())){
			OauthAccountEntity modifiedBy = getAccount(graphSite.getUpdate_userinfo_id());
			if(null != modifiedBy){
				graphSite.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(graphSite);
	}
	/**
	* 添加
	* @param graphSite
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addGraphSite(@RequestBody GraphSite graphSite){
		int i = 0;
		if(null != graphSite){
			graphSite.setGraph_site_id(toUUID());
			graphSite.setCTime(new Date());
			graphSite.setXt_userinfo_id(getXtUid());
			i=graphSiteService.addGraphSite(graphSite);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param graphSite
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateGraphSite(@RequestBody GraphSite graphSite){
		int i = 0;
		if(null != graphSite){
			graphSite.setMTime(new Date());
			graphSite.setUpdate_userinfo_id(getXtUid());
			i=graphSiteService.updateGraphSite(graphSite);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param graph_site_id
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delGraphSite(String graph_site_id){
		int i = 0;
		if(!StringUtil.isEmpty(graph_site_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("graph_site_id",graph_site_id.split(","));
			i=graphSiteService.delGraphSite(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询全部
	 */
	@ApiOperation(value="查询全部", notes="查询全部")
	@PostMapping(value="/substations")
	@NeedLoginUnAuth
	public BaseResult substations(){
		Map<String,Object> condition = new HashMap<>();
		List<GraphSite> graphSiteList = graphSiteService.getGraphSiteListByCondition(condition);
		return new BaseResult(graphSiteList);
	}
}
