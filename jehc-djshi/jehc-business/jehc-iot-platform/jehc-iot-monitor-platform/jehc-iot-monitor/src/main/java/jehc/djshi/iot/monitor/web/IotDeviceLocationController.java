package jehc.djshi.iot.monitor.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.iot.monitor.model.IotDeviceLocation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.iot.monitor.service.IotDeviceLocationService;
/**
 * @Desc 设备位置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotDeviceLocation")
@Api(value = "设备位置",tags = "设备位置", description = "设备位置")
public class IotDeviceLocationController extends BaseAction{
	@Autowired
	private IotDeviceLocationService iotDeviceLocationService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getIotDeviceLocationListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<IotDeviceLocation> iotDeviceLocationList = iotDeviceLocationService.getIotDeviceLocationListByCondition(condition);
		for(IotDeviceLocation iotDeviceLocation:iotDeviceLocationList){
			if(!StringUtil.isEmpty(iotDeviceLocation.getCreate_id())){
				OauthAccountEntity createBy = getAccount(iotDeviceLocation.getCreate_id());
				if(null != createBy){
					iotDeviceLocation.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(iotDeviceLocation.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(iotDeviceLocation.getUpdate_id());
				if(null != modifiedBy){
					iotDeviceLocation.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<IotDeviceLocation> page = new PageInfo<IotDeviceLocation>(iotDeviceLocationList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param device_location_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{device_location_id}")
	public BaseResult getIotDeviceLocationById(@PathVariable("device_location_id")String device_location_id){
		IotDeviceLocation iotDeviceLocation = iotDeviceLocationService.getIotDeviceLocationById(device_location_id);
		if(!StringUtil.isEmpty(iotDeviceLocation.getCreate_id())){
			OauthAccountEntity createBy = getAccount(iotDeviceLocation.getCreate_id());
			if(null != createBy){
				iotDeviceLocation.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(iotDeviceLocation.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(iotDeviceLocation.getUpdate_id());
			if(null != modifiedBy){
				iotDeviceLocation.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(iotDeviceLocation);
	}
	/**
	* 添加
	* @param iotDeviceLocation 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addIotDeviceLocation(@RequestBody IotDeviceLocation iotDeviceLocation){
		int i = 0;
		if(null != iotDeviceLocation){
			iotDeviceLocation.setDevice_location_id(toUUID());
			i=iotDeviceLocationService.addIotDeviceLocation(iotDeviceLocation);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param iotDeviceLocation 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateIotDeviceLocation(@RequestBody IotDeviceLocation iotDeviceLocation){
		int i = 0;
		if(null != iotDeviceLocation){
			i=iotDeviceLocationService.updateIotDeviceLocation(iotDeviceLocation);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param device_location_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delIotDeviceLocation(String device_location_id){
		int i = 0;
		if(!StringUtil.isEmpty(device_location_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("device_location_id",device_location_id.split(","));
			i=iotDeviceLocationService.delIotDeviceLocation(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询所有记录
	 */
	@ApiOperation(value="查询所有记录", notes="查询所有记录")
	@NeedLoginUnAuth
	@GetMapping(value="/allList")
	public BaseResult getList(){
		Map<String, Object> condition = new HashMap<>();
		List<IotDeviceLocation> iotDeviceLocationList = iotDeviceLocationService.getIotDeviceLocationListByCondition(condition);
		return new BaseResult(iotDeviceLocationList);
	}
}
