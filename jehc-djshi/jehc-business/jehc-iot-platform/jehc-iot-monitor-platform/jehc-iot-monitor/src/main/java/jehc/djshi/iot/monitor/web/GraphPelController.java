package jehc.djshi.iot.monitor.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.AuthUneedLogin;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.monitor.model.GraphPel;
import jehc.djshi.iot.monitor.service.GraphPelElementService;
import jehc.djshi.iot.monitor.service.GraphPelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 图元管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "图元管理", tags = "图元管理", description = "图元管理")
@RestController
@RequestMapping("/graphPel")
public class GraphPelController extends BaseAction{
    @Autowired
    GraphPelService graphPelService;
    @Autowired
    GraphPelElementService graphPelElementService;


    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @PostMapping(value="/list")
    @NeedLoginUnAuth
    public BasePage getGraphPelListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<GraphPel> graphPelList = graphPelService.getGraphPelListByCondition(condition);
        PageInfo<GraphPel> page = new PageInfo<GraphPel>(graphPelList);
        return outPageBootStr(page,baseSearch);
    }

    /**
     * 查询单条记录
     * @param graph_pel_id
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @GetMapping(value="/get/{graph_pel_id}")
    @AuthUneedLogin
    public BaseResult getGraphPelById(@PathVariable("graph_pel_id")String graph_pel_id){
        GraphPel graphPel = graphPelService.getGraphPelById(graph_pel_id);
        Map<String,Object> condition = new HashMap<>();
        condition.put("graph_pel_id",graphPel.getGraph_pel_id());
        graphPel.setGraphPelElements(graphPelElementService.getGraphPelElementListByCondition(condition));
        return outDataStr(graphPel);
    }

    /**
     * 添加
     * @param graphPel
     * @return
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addGraphPel(@RequestBody GraphPel graphPel){
        int i = 0;
        if(null != graphPel){
            graphPel.setGraph_pel_id(toUUID());
            i=graphPelService.addGraphPel(graphPel);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 修改
     * @param graphPel
     * @return
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateGraphPel(@RequestBody GraphPel graphPel){
        int i = 0;
        if(null != graphPel){
            i=graphPelService.updateGraphPel(graphPel);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 修改SVG
     * @param graphPel
     * @return
     */
    @ApiOperation(value="修改SVG", notes="修改SVG")
    @PostMapping(value="/updatePelSvg")
    public BaseResult updatePelSvg(@RequestBody GraphPel graphPel){
        int i = 0;
        if(null != graphPel){
            GraphPel pel = graphPelService.getGraphPelById(graphPel.getGraph_pel_id());
            pel.setSvgHtml(graphPel.getSvgHtml());
            i=graphPelService.updateGraphPel(pel);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }


    /**
     * 删除
     * @param graph_pel_id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delGraphPel(String graph_pel_id){
        int i = 0;
        if(!StringUtil.isEmpty(graph_pel_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("graph_pel_id",graph_pel_id.split(","));
            i=graphPelService.delGraphPel(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }

    /**
     * 查询全部
     */
    @ApiOperation(value="查询全部", notes="查询全部")
    @NeedLoginUnAuth
    @GetMapping(value="/listAll")
    public BaseResult getGraphPelList(String graph_pel_category){
        Map<String,Object> condition = new HashMap<>();
        condition.put("graph_pel_category",graph_pel_category);
        List<GraphPel> graphPelList = graphPelService.getGraphPelListByCondition(condition);
        for(GraphPel graphPel:graphPelList){
            condition = new HashMap<>();
            condition.put("graph_pel_id",graphPel.getGraph_pel_id());
            graphPel.setGraphPelElements(graphPelElementService.getGraphPelElementListByCondition(condition));
        }
        return outItemsStr(graphPelList);
    }
}
