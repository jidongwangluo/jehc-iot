package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotDeviceConfigDao;
import jehc.djshi.iot.monitor.model.IotDeviceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.monitor.service.IotDeviceConfigService;

/**
 * @Desc 设备配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotDeviceConfigService")
public class IotDeviceConfigServiceImpl extends BaseService implements IotDeviceConfigService{
	@Autowired
	private IotDeviceConfigDao iotDeviceConfigDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<IotDeviceConfig> getIotDeviceConfigListByCondition(Map<String,Object> condition){
		try{
			return iotDeviceConfigDao.getIotDeviceConfigListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param device_config_id 
	* @return
	*/
	public IotDeviceConfig getIotDeviceConfigById(String device_config_id){
		try{
			IotDeviceConfig iotDeviceConfig = iotDeviceConfigDao.getIotDeviceConfigById(device_config_id);
			return iotDeviceConfig;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param iotDeviceConfig 
	* @return
	*/
	public int addIotDeviceConfig(IotDeviceConfig iotDeviceConfig){
		int i = 0;
		try {
			i = iotDeviceConfigDao.addIotDeviceConfig(iotDeviceConfig);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param iotDeviceConfig 
	* @return
	*/
	public int updateIotDeviceConfig(IotDeviceConfig iotDeviceConfig){
		int i = 0;
		try {
			i = iotDeviceConfigDao.updateIotDeviceConfig(iotDeviceConfig);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param iotDeviceConfig 
	* @return
	*/
	public int updateIotDeviceConfigBySelective(IotDeviceConfig iotDeviceConfig){
		int i = 0;
		try {
			i = iotDeviceConfigDao.updateIotDeviceConfigBySelective(iotDeviceConfig);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delIotDeviceConfig(Map<String,Object> condition){
		int i = 0;
		try {
			i = iotDeviceConfigDao.delIotDeviceConfig(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param iotDeviceConfigList 
	* @return
	*/
	public int updateBatchIotDeviceConfig(List<IotDeviceConfig> iotDeviceConfigList){
		int i = 0;
		try {
			i = iotDeviceConfigDao.updateBatchIotDeviceConfig(iotDeviceConfigList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceConfigList 
	* @return
	*/
	public int updateBatchIotDeviceConfigBySelective(List<IotDeviceConfig> iotDeviceConfigList){
		int i = 0;
		try {
			i = iotDeviceConfigDao.updateBatchIotDeviceConfigBySelective(iotDeviceConfigList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 根据设备id查找对象
	 * @param device_id
	 * @return
	 */
	public IotDeviceConfig getIotDeviceConfigByDeviceId(String device_id){
		return iotDeviceConfigDao.getIotDeviceConfigByDeviceId(device_id);
	}
}
