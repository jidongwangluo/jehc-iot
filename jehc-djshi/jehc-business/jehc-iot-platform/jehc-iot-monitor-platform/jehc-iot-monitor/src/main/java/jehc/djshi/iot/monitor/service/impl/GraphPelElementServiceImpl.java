package jehc.djshi.iot.monitor.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.iot.monitor.dao.GraphPelElementDao;
import jehc.djshi.iot.monitor.model.GraphPelElement;
import jehc.djshi.iot.monitor.service.GraphPelElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Desc 图元节点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("graphPelElementService")
public class GraphPelElementServiceImpl extends BaseService implements GraphPelElementService {
    @Autowired
    GraphPelElementDao graphPelElementDao;

    /**
     * 分页查询
     * @param condition
     * @return
     */
    public List<GraphPelElement> getGraphPelElementListByCondition(Map<String,Object> condition){
        return graphPelElementDao.getGraphPelElementListByCondition(condition);
    }

    /**
     * 查询单条记录
     * @param graph_pel_element_id
     * @return
     */
    public GraphPelElement getGraphPelElementById(String graph_pel_element_id){
        return graphPelElementDao.getGraphPelElementById(graph_pel_element_id);
    }

    /**
     * 添加
     * @param graphPelElement
     * @return
     */
    public int addGraphPelElement(GraphPelElement graphPelElement){
        return graphPelElementDao.addGraphPelElement(graphPelElement);
    }

    /**
     * 修改
     * @param graphPelElement
     * @return
     */
    public int updateGraphPelElement(GraphPelElement graphPelElement){
        return graphPelElementDao.updateGraphPelElement(graphPelElement);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delGraphPelElement(Map<String,Object> condition){
        return graphPelElementDao.delGraphPelElement(condition);
    }

    /**
     * 删除
     * @param condition
     * @return
     */
    public int delGraphPelElements(Map<String,Object> condition){
        return graphPelElementDao.delGraphPelElements(condition);
    }
}
