package jehc.djshi.iot.monitor.dao;

import jehc.djshi.iot.monitor.model.IotVideoGroupRight;

import java.util.List;
import java.util.Map;
/**
 * @Desc 相机组关联
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotVideoGroupRightDao {
    /**
     * 查询列表
     * @param condition
     * @return
     */
    List<IotVideoGroupRight> getIotVideoGroupRightByCondition(Map<String,Object> condition);

    /**
     * 新增
     * @param iotVideoGroupRight
     * @return
     */
    int addIotVideoGroupRight(IotVideoGroupRight iotVideoGroupRight);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delIotVideoGroupRight(Map<String,Object> condition);

    /**
     * 根据组id删除
     * @param condition
     * @return
     */
    int delIotVideoGroupRightByGroup(Map<String,Object> condition);


}
