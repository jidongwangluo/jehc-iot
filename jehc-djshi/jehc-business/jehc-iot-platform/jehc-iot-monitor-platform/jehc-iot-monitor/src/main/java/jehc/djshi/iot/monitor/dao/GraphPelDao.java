package jehc.djshi.iot.monitor.dao;

import jehc.djshi.iot.monitor.model.GraphPel;

import java.util.List;
import java.util.Map;
/**
 * @Desc 图元管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface GraphPelDao {
    /**
     * 分页查询
     * @param condition
     * @return
     */
    List<GraphPel> getGraphPelListByCondition(Map<String,Object> condition);

    /**
     * 查询单条记录
     * @param graph_pel_id
     * @return
     */
    GraphPel getGraphPelById(String graph_pel_id);

    /**
     * 添加
     * @param graphPel
     * @return
     */
    int addGraphPel(GraphPel graphPel);

    /**
     * 修改
     * @param graphPel
     * @return
     */
    int updateGraphPel(GraphPel graphPel);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delGraphPel(Map<String,Object> condition);
}
