package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotDeviceTypeDao;
import jehc.djshi.iot.monitor.model.IotDeviceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.monitor.service.IotDeviceTypeService;

/**
 * @Desc 设备类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotDeviceTypeService")
public class IotDeviceTypeServiceImpl extends BaseService implements IotDeviceTypeService{
	@Autowired
	private IotDeviceTypeDao iotDeviceTypeDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<IotDeviceType> getIotDeviceTypeListByCondition(Map<String,Object> condition){
		try{
			return iotDeviceTypeDao.getIotDeviceTypeListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param device_type_id 
	* @return
	*/
	public IotDeviceType getIotDeviceTypeById(String device_type_id){
		try{
			IotDeviceType iotDeviceType = iotDeviceTypeDao.getIotDeviceTypeById(device_type_id);
			return iotDeviceType;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param iotDeviceType 
	* @return
	*/
	public int addIotDeviceType(IotDeviceType iotDeviceType){
		int i = 0;
		try {
			iotDeviceType.setCreate_id(getXtUid());
			i = iotDeviceTypeDao.addIotDeviceType(iotDeviceType);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param iotDeviceType 
	* @return
	*/
	public int updateIotDeviceType(IotDeviceType iotDeviceType){
		int i = 0;
		try {
			iotDeviceType.setUpdate_id(getXtUid());
			i = iotDeviceTypeDao.updateIotDeviceType(iotDeviceType);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param iotDeviceType 
	* @return
	*/
	public int updateIotDeviceTypeBySelective(IotDeviceType iotDeviceType){
		int i = 0;
		try {
			iotDeviceType.setUpdate_id(getXtUid());
			i = iotDeviceTypeDao.updateIotDeviceTypeBySelective(iotDeviceType);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delIotDeviceType(Map<String,Object> condition){
		int i = 0;
		try {
			i = iotDeviceTypeDao.delIotDeviceType(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param iotDeviceTypeList 
	* @return
	*/
	public int updateBatchIotDeviceType(List<IotDeviceType> iotDeviceTypeList){
		int i = 0;
		try {
			i = iotDeviceTypeDao.updateBatchIotDeviceType(iotDeviceTypeList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceTypeList 
	* @return
	*/
	public int updateBatchIotDeviceTypeBySelective(List<IotDeviceType> iotDeviceTypeList){
		int i = 0;
		try {
			i = iotDeviceTypeDao.updateBatchIotDeviceTypeBySelective(iotDeviceTypeList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 查询相机类型
	 * @param condition
	 * @return
	 */
	public List<IotDeviceType> getVideoTypeList(Map<String, Object> condition){
		try{
			return iotDeviceTypeDao.getVideoTypeList(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
