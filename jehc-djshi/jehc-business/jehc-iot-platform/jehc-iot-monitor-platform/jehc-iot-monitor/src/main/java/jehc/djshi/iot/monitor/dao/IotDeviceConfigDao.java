package jehc.djshi.iot.monitor.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.iot.monitor.model.IotDeviceConfig;

/**
 * @Desc 设备配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotDeviceConfigDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<IotDeviceConfig> getIotDeviceConfigListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param device_config_id 
	* @return
	*/
	IotDeviceConfig getIotDeviceConfigById(String device_config_id);
	/**
	* 添加
	* @param iotDeviceConfig 
	* @return
	*/
	int addIotDeviceConfig(IotDeviceConfig iotDeviceConfig);
	/**
	* 修改
	* @param iotDeviceConfig 
	* @return
	*/
	int updateIotDeviceConfig(IotDeviceConfig iotDeviceConfig);
	/**
	* 修改（根据动态条件）
	* @param iotDeviceConfig 
	* @return
	*/
	int updateIotDeviceConfigBySelective(IotDeviceConfig iotDeviceConfig);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delIotDeviceConfig(Map<String, Object> condition);
	/**
	* 批量修改
	* @param iotDeviceConfigList 
	* @return
	*/
	int updateBatchIotDeviceConfig(List<IotDeviceConfig> iotDeviceConfigList);
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceConfigList 
	* @return
	*/
	int updateBatchIotDeviceConfigBySelective(List<IotDeviceConfig> iotDeviceConfigList);

	/**
	 * 根据设备id查找对象
	 * @param device_id
	 * @return
	 */
	IotDeviceConfig getIotDeviceConfigByDeviceId(String device_id);
}
