package jehc.djshi.iot.monitor.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.iot.monitor.model.IotOem;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.iot.monitor.service.IotOemService;
/**
 * @Desc 生产商
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotOem")
@Api(value = "生产商",tags = "生产商", description = "生产商")
public class IotOemController extends BaseAction{
	@Autowired
	private IotOemService iotOemService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getIotOemListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<IotOem> iotOemList = iotOemService.getIotOemListByCondition(condition);
		for(IotOem iotOem:iotOemList){
			if(!StringUtil.isEmpty(iotOem.getCreate_id())){
				OauthAccountEntity createBy = getAccount(iotOem.getCreate_id());
				if(null != createBy){
					iotOem.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(iotOem.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(iotOem.getUpdate_id());
				if(null != modifiedBy){
					iotOem.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<IotOem> page = new PageInfo<IotOem>(iotOemList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param oem_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{oem_id}")
	public BaseResult getIotOemById(@PathVariable("oem_id")String oem_id){
		IotOem iotOem = iotOemService.getIotOemById(oem_id);
		if(!StringUtil.isEmpty(iotOem.getCreate_id())){
			OauthAccountEntity createBy = getAccount(iotOem.getCreate_id());
			if(null != createBy){
				iotOem.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(iotOem.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(iotOem.getUpdate_id());
			if(null != modifiedBy){
				iotOem.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(iotOem);
	}
	/**
	* 添加
	* @param iotOem 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addIotOem(@RequestBody IotOem iotOem){
		int i = 0;
		if(null != iotOem){
			iotOem.setOem_id(toUUID());
			iotOem.setCreate_time(getDate());
			i=iotOemService.addIotOem(iotOem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param iotOem 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateIotOem(@RequestBody IotOem iotOem){
		int i = 0;
		if(null != iotOem){
			iotOem.setUpdate_time(getDate());
			i=iotOemService.updateIotOem(iotOem);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param oem_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delIotOem(String oem_id){
		int i = 0;
		if(!StringUtil.isEmpty(oem_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("oem_id",oem_id.split(","));
			i=iotOemService.delIotOem(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询全部生产商
	 */
	@ApiOperation(value="查询全部生产商", notes="查询全部生产商")
	@NeedLoginUnAuth
	@GetMapping(value="/allList")
	public BaseResult allList(){
		Map<String,Object> condition = new HashMap<>();
		List<IotOem> iotOemList = iotOemService.getIotOemListByCondition(condition);
		return new BaseResult(iotOemList);
	}
}
