package jehc.djshi.iot.monitor.service;

import jehc.djshi.iot.monitor.model.IotVideoGroup;

import java.util.List;
import java.util.Map;
/**
 * @Desc 相机组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotVideoGroupService {
    /**
     * 分页
     * @param condition
     * @return
     */
    List<IotVideoGroup> getIotVideoGroupByCondition(Map<String, Object> condition);
    /**
     * 查询对象
     * @param video_group_id
     * @return
     */
    IotVideoGroup getIotVideoGroupById(String video_group_id);
    /**
     * 添加
     * @param iotVideoGroup
     * @return
     */
    int addIotVideoGroup(IotVideoGroup iotVideoGroup);
    /**
     * 修改
     * @param iotVideoGroup
     * @return
     */
    int updateIotVideoGroup(IotVideoGroup iotVideoGroup);
    /**
     * 删除
     * @param condition
     * @return
     */
    int delIotVideoGroup(Map<String, Object> condition);
}
