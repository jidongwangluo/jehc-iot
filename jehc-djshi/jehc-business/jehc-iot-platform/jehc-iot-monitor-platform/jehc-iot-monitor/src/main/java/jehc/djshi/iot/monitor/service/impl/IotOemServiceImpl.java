package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotOemDao;
import jehc.djshi.iot.monitor.model.IotOem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.monitor.service.IotOemService;

/**
 * @Desc 生产商
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotOemService")
public class IotOemServiceImpl extends BaseService implements IotOemService{
	@Autowired
	private IotOemDao iotOemDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<IotOem> getIotOemListByCondition(Map<String,Object> condition){
		try{
			return iotOemDao.getIotOemListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param oem_id 
	* @return
	*/
	public IotOem getIotOemById(String oem_id){
		try{
			IotOem iotOem = iotOemDao.getIotOemById(oem_id);
			return iotOem;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param iotOem 
	* @return
	*/
	public int addIotOem(IotOem iotOem){
		int i = 0;
		try {
			iotOem.setCreate_id(getXtUid());
			i = iotOemDao.addIotOem(iotOem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param iotOem 
	* @return
	*/
	public int updateIotOem(IotOem iotOem){
		int i = 0;
		try {
			iotOem.setUpdate_id(getXtUid());
			i = iotOemDao.updateIotOem(iotOem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param iotOem 
	* @return
	*/
	public int updateIotOemBySelective(IotOem iotOem){
		int i = 0;
		try {
			iotOem.setUpdate_id(getXtUid());
			i = iotOemDao.updateIotOemBySelective(iotOem);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delIotOem(Map<String,Object> condition){
		int i = 0;
		try {
			i = iotOemDao.delIotOem(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param iotOemList 
	* @return
	*/
	public int updateBatchIotOem(List<IotOem> iotOemList){
		int i = 0;
		try {
			i = iotOemDao.updateBatchIotOem(iotOemList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param iotOemList 
	* @return
	*/
	public int updateBatchIotOemBySelective(List<IotOem> iotOemList){
		int i = 0;
		try {
			i = iotOemDao.updateBatchIotOemBySelective(iotOemList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
