package jehc.djshi.iot.monitor.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.monitor.model.GraphSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.iot.monitor.service.GraphSiteService;
import java.util.Date;
/**
 * @Desc 拓扑图设计平台API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "拓扑图设计平台",tags = "拓扑图设计平台", description = "拓扑图设计平台")
@RestController
@RequestMapping("/graphDesign")
public class GraphDesignController  extends BaseAction {

    @Autowired
    private GraphSiteService graphSiteService;

    /**
     * 修改
     * @param graphSite
     */
    @PostMapping(value="/saveGraphSite")
    @ApiOperation(value="保存", notes="保存")
    public BaseResult saveGraphSite(@RequestBody GraphSite graphSite){
        int i = 0;
        if(null != graphSite){
            //编辑
            if(!StringUtil.isEmpty(graphSite.getGraph_site_id())){
                graphSite.setMTime(new Date());
                graphSite.setUpdate_userinfo_id(getXtUid());
                i=graphSiteService.updateGraphSiteBySelective(graphSite);
            }else{
                //新增
                graphSite.setGraph_site_id(toUUID());
                graphSite.setCTime(new Date());
                graphSite.setXt_userinfo_id(getXtUid());
                i=graphSiteService.addGraphSite(graphSite);
            }
        }
        if(i>0){
            return outAudStr(true,"保存成功",graphSite.getGraph_site_id());
        }else{
            return outAudStr(true,"保存失败");
        }
    }
}
