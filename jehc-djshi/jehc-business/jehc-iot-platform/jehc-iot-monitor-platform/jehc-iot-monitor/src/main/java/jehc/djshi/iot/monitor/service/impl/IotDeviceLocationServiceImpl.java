package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotDeviceLocationDao;
import jehc.djshi.iot.monitor.model.IotDeviceLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.monitor.service.IotDeviceLocationService;
/**
 * @Desc 设备位置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotDeviceLocationService")
public class IotDeviceLocationServiceImpl extends BaseService implements IotDeviceLocationService{
	@Autowired
	private IotDeviceLocationDao iotDeviceLocationDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<IotDeviceLocation> getIotDeviceLocationListByCondition(Map<String,Object> condition){
		try{
			return iotDeviceLocationDao.getIotDeviceLocationListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param device_location_id 
	* @return
	*/
	public IotDeviceLocation getIotDeviceLocationById(String device_location_id){
		try{
			IotDeviceLocation iotDeviceLocation = iotDeviceLocationDao.getIotDeviceLocationById(device_location_id);
			return iotDeviceLocation;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param iotDeviceLocation 
	* @return
	*/
	public int addIotDeviceLocation(IotDeviceLocation iotDeviceLocation){
		int i = 0;
		try {
			iotDeviceLocation.setCreate_id(getXtUid());
			iotDeviceLocation.setCreate_time(getDate());
			i = iotDeviceLocationDao.addIotDeviceLocation(iotDeviceLocation);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param iotDeviceLocation 
	* @return
	*/
	public int updateIotDeviceLocation(IotDeviceLocation iotDeviceLocation){
		int i = 0;
		try {
			iotDeviceLocation.setUpdate_id(getXtUid());
			iotDeviceLocation.setUpdate_time(getDate());
			i = iotDeviceLocationDao.updateIotDeviceLocation(iotDeviceLocation);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param iotDeviceLocation 
	* @return
	*/
	public int updateIotDeviceLocationBySelective(IotDeviceLocation iotDeviceLocation){
		int i = 0;
		try {
			i = iotDeviceLocationDao.updateIotDeviceLocationBySelective(iotDeviceLocation);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delIotDeviceLocation(Map<String,Object> condition){
		int i = 0;
		try {
			i = iotDeviceLocationDao.delIotDeviceLocation(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param iotDeviceLocationList 
	* @return
	*/
	public int updateBatchIotDeviceLocation(List<IotDeviceLocation> iotDeviceLocationList){
		int i = 0;
		try {
			i = iotDeviceLocationDao.updateBatchIotDeviceLocation(iotDeviceLocationList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceLocationList 
	* @return
	*/
	public int updateBatchIotDeviceLocationBySelective(List<IotDeviceLocation> iotDeviceLocationList){
		int i = 0;
		try {
			i = iotDeviceLocationDao.updateBatchIotDeviceLocationBySelective(iotDeviceLocationList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
