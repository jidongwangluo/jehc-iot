package jehc.djshi.iot.monitor.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.iot.monitor.model.IotModel;

/**
 * @Desc 设备型号
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotModelService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<IotModel> getIotModelListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param model_id 
	* @return
	*/
	IotModel getIotModelById(String model_id);
	/**
	* 添加
	* @param iotModel 
	* @return
	*/
	int addIotModel(IotModel iotModel);
	/**
	* 修改
	* @param iotModel 
	* @return
	*/
	int updateIotModel(IotModel iotModel);
	/**
	* 修改（根据动态条件）
	* @param iotModel 
	* @return
	*/
	int updateIotModelBySelective(IotModel iotModel);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delIotModel(Map<String, Object> condition);
	/**
	* 批量修改
	* @param iotModelList 
	* @return
	*/
	int updateBatchIotModel(List<IotModel> iotModelList);
	/**
	* 批量修改（根据动态条件）
	* @param iotModelList 
	* @return
	*/
	int updateBatchIotModelBySelective(List<IotModel> iotModelList);
}
