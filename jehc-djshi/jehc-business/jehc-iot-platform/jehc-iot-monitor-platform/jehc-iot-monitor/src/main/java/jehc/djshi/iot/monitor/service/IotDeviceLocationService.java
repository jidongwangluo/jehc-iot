package jehc.djshi.iot.monitor.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.iot.monitor.model.IotDeviceLocation;

/**
 * @Desc 设备位置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotDeviceLocationService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<IotDeviceLocation> getIotDeviceLocationListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param device_location_id 
	* @return
	*/
	IotDeviceLocation getIotDeviceLocationById(String device_location_id);
	/**
	* 添加
	* @param iotDeviceLocation 
	* @return
	*/
	int addIotDeviceLocation(IotDeviceLocation iotDeviceLocation);
	/**
	* 修改
	* @param iotDeviceLocation 
	* @return
	*/
	int updateIotDeviceLocation(IotDeviceLocation iotDeviceLocation);
	/**
	* 修改（根据动态条件）
	* @param iotDeviceLocation 
	* @return
	*/
	int updateIotDeviceLocationBySelective(IotDeviceLocation iotDeviceLocation);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delIotDeviceLocation(Map<String, Object> condition);
	/**
	* 批量修改
	* @param iotDeviceLocationList 
	* @return
	*/
	int updateBatchIotDeviceLocation(List<IotDeviceLocation> iotDeviceLocationList);
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceLocationList 
	* @return
	*/
	int updateBatchIotDeviceLocationBySelective(List<IotDeviceLocation> iotDeviceLocationList);
}
