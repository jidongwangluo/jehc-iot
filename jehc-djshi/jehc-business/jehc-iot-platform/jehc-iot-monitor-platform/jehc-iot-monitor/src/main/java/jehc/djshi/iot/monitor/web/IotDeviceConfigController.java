package jehc.djshi.iot.monitor.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.iot.monitor.model.IotDevice;
import jehc.djshi.iot.monitor.service.IotDeviceService;
import jehc.djshi.iot.monitor.model.IotDeviceConfig;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.iot.monitor.service.IotDeviceConfigService;

/**
 * @Desc 设备配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotDeviceConfig")
@Api(value = "设备配置",tags = "设备配置", description = "设备配置")
public class IotDeviceConfigController extends BaseAction{
	@Autowired
	private IotDeviceConfigService iotDeviceConfigService;

	@Autowired
	private IotDeviceService iotDeviceService;

	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getIotDeviceConfigListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<IotDeviceConfig> iotDeviceConfigList = iotDeviceConfigService.getIotDeviceConfigListByCondition(condition);
		PageInfo<IotDeviceConfig> page = new PageInfo<IotDeviceConfig>(iotDeviceConfigList);
		return outPageBootStr(page,baseSearch);
	}
//	/**
//	* 查询单条记录
//	* @param device_config_id
//	*/
//	@ApiOperation(value="查询单条记录", notes="查询单条记录")
//	@NeedLoginUnAuth
//	@GetMapping(value="/get/{device_config_id}")
//	public BaseResult getIotDeviceConfigById(@PathVariable("device_config_id")String device_config_id){
//		IotDeviceConfig iotDeviceConfig = iotDeviceConfigService.getIotDeviceConfigById(device_config_id);
//		return outDataStr(iotDeviceConfig);
//	}
	/**
	* 添加
	* @param iotDeviceConfig 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addIotDeviceConfig(@RequestBody IotDeviceConfig iotDeviceConfig){
		int i = 0;
		if(null != iotDeviceConfig){
			iotDeviceConfig.setDevice_config_id(toUUID());
			i=iotDeviceConfigService.addIotDeviceConfig(iotDeviceConfig);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param iotDeviceConfig 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateIotDeviceConfig(@RequestBody IotDeviceConfig iotDeviceConfig){
		int i = 0;
		if(null != iotDeviceConfig){
			if(StringUtil.isEmpty(iotDeviceConfig.getDevice_config_id())){
				iotDeviceConfig.setDevice_config_id(toUUID());
				i=iotDeviceConfigService.addIotDeviceConfig(iotDeviceConfig);
			}else{
				i=iotDeviceConfigService.updateIotDeviceConfig(iotDeviceConfig);
			}
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param device_config_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delIotDeviceConfig(String device_config_id){
		int i = 0;
		if(!StringUtil.isEmpty(device_config_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("device_config_id",device_config_id.split(","));
			i=iotDeviceConfigService.delIotDeviceConfig(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查询单条记录
	 * @param device_id
	 */
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{device_id}")
	public BaseResult getIotDeviceConfigByDeviceId(@PathVariable("device_id")String device_id){
		IotDeviceConfig iotDeviceConfig = iotDeviceConfigService.getIotDeviceConfigByDeviceId(device_id);
		IotDevice iotDevice = iotDeviceService.getIotDeviceById(device_id);
		if(null == iotDeviceConfig){
			iotDeviceConfig = new IotDeviceConfig();
		}
		iotDeviceConfig.setIotDevice(iotDevice);
		return outDataStr(iotDeviceConfig);
	}
}
