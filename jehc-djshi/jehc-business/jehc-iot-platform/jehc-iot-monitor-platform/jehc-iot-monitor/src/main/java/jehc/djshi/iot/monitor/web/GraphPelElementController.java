package jehc.djshi.iot.monitor.web;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BaseAction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * @Desc 图元节点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Api(value = "图元节点", tags = "图元节点", description = "图元节点")
@Controller
@RequestMapping("/graphPelElement")
public class GraphPelElementController extends BaseAction {

}
