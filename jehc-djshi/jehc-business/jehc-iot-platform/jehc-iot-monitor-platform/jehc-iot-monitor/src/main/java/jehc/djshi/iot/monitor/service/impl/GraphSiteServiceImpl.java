package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.GraphSiteDao;
import jehc.djshi.iot.monitor.service.GraphSiteService;
import jehc.djshi.iot.monitor.model.GraphSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * @Desc 站点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("graphSiteService")
public class GraphSiteServiceImpl extends BaseService implements GraphSiteService {
	@Autowired
	private GraphSiteDao graphSiteDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<GraphSite> getGraphSiteListByCondition(Map<String,Object> condition){
		try{
			return graphSiteDao.getGraphSiteListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询单条记录
	* @param graph_site_id 
	* @return
	*/
	public GraphSite getGraphSiteById(String graph_site_id){
		try{
			GraphSite graphSite = graphSiteDao.getGraphSiteById(graph_site_id);
			return graphSite;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param graphSite 
	* @return
	*/
	public int addGraphSite(GraphSite graphSite){
		int i = 0;
		try {
			i = graphSiteDao.addGraphSite(graphSite);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param graphSite 
	* @return
	*/
	public int updateGraphSite(GraphSite graphSite){
		int i = 0;
		try {
			i = graphSiteDao.updateGraphSite(graphSite);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param graphSite 
	* @return
	*/
	public int updateGraphSiteBySelective(GraphSite graphSite){
		int i = 0;
		try {
			i = graphSiteDao.updateGraphSiteBySelective(graphSite);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delGraphSite(Map<String,Object> condition){
		int i = 0;
		try {
			i = graphSiteDao.delGraphSite(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量添加
	* @param graphSiteList 
	* @return
	*/
	public int addBatchGraphSite(List<GraphSite> graphSiteList){
		int i = 0;
		try {
			i = graphSiteDao.addBatchGraphSite(graphSiteList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param graphSiteList 
	* @return
	*/
	public int updateBatchGraphSite(List<GraphSite> graphSiteList){
		int i = 0;
		try {
			i = graphSiteDao.updateBatchGraphSite(graphSiteList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param graphSiteList 
	* @return
	*/
	public int updateBatchGraphSiteBySelective(List<GraphSite> graphSiteList){
		int i = 0;
		try {
			i = graphSiteDao.updateBatchGraphSiteBySelective(graphSiteList);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
