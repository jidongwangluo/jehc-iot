package jehc.djshi.iot.monitor.dao;


import jehc.djshi.iot.monitor.model.GraphPelElement;

import java.util.List;
import java.util.Map;
/**
 * @Desc 图元节点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface GraphPelElementDao {

    /**
     * 分页查询
     * @param condition
     * @return
     */
    List<GraphPelElement> getGraphPelElementListByCondition(Map<String,Object> condition);

    /**
     * 查询单条记录
     * @param graph_pel_element_id
     * @return
     */
    GraphPelElement getGraphPelElementById(String graph_pel_element_id);

    /**
     * 添加
     * @param graphPelElement
     * @return
     */
    int addGraphPelElement(GraphPelElement graphPelElement);

    /**
     * 修改
     * @param graphPelElement
     * @return
     */
    int updateGraphPelElement(GraphPelElement graphPelElement);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delGraphPelElement(Map<String,Object> condition);

    /**
     * 删除
     * @param condition
     * @return
     */
    int delGraphPelElements(Map<String,Object> condition);
}
