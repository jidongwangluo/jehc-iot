package jehc.djshi.iot.monitor.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;

import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.iot.monitor.model.IotDevice;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.iot.monitor.service.IotDeviceService;
/**
 * @Desc 设备基础信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotDevice")
@Api(value = "设备基础信息",tags = "设备基础信息", description = "设备基础信息")
public class IotDeviceController extends BaseAction{
	@Autowired
	private IotDeviceService iotDeviceService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getIotDeviceListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<IotDevice> iotDeviceList = iotDeviceService.getIotDeviceListByCondition(condition);
		for(IotDevice iotDevice:iotDeviceList){
			if(!StringUtil.isEmpty(iotDevice.getCreate_id())){
				OauthAccountEntity createBy = getAccount(iotDevice.getCreate_id());
				if(null != createBy){
					iotDevice.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(iotDevice.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(iotDevice.getUpdate_id());
				if(null != modifiedBy){
					iotDevice.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<IotDevice> page = new PageInfo<IotDevice>(iotDeviceList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param device_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{device_id}")
	public BaseResult getIotDeviceById(@PathVariable("device_id")String device_id){
		IotDevice iotDevice = iotDeviceService.getIotDeviceById(device_id);
		if(!StringUtil.isEmpty(iotDevice.getCreate_id())){
			OauthAccountEntity createBy = getAccount(iotDevice.getCreate_id());
			if(null != createBy){
				iotDevice.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(iotDevice.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(iotDevice.getUpdate_id());
			if(null != modifiedBy){
				iotDevice.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(iotDevice);
	}
	/**
	* 添加
	* @param iotDevice 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addIotDevice(@RequestBody IotDevice iotDevice){
		int i = 0;
		if(null != iotDevice){
			iotDevice.setDevice_id(toUUID());
			i=iotDeviceService.addIotDevice(iotDevice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param iotDevice 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateIotDevice(@RequestBody IotDevice iotDevice){
		int i = 0;
		if(null != iotDevice){
			i=iotDeviceService.updateIotDevice(iotDevice);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param device_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delIotDevice(String device_id){
		int i = 0;
		if(!StringUtil.isEmpty(device_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("device_id",device_id.split(","));
			i=iotDeviceService.delIotDevice(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
