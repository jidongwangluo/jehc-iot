package jehc.djshi.iot.monitor.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotDeviceDao;
import jehc.djshi.iot.monitor.model.IotDevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.monitor.service.IotDeviceService;
/**
 * @Desc 设备基础信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotDeviceService")
public class IotDeviceServiceImpl extends BaseService implements IotDeviceService{
	@Autowired
	private IotDeviceDao iotDeviceDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<IotDevice> getIotDeviceListByCondition(Map<String,Object> condition){
		try{
			return iotDeviceDao.getIotDeviceListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param device_id 
	* @return
	*/
	public IotDevice getIotDeviceById(String device_id){
		try{
			IotDevice iotDevice = iotDeviceDao.getIotDeviceById(device_id);
			return iotDevice;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param iotDevice 
	* @return
	*/
	public int addIotDevice(IotDevice iotDevice){
		int i = 0;
		try {
			iotDevice.setCreate_id(getXtUid());
			iotDevice.setCreate_time(getDate());
			i = iotDeviceDao.addIotDevice(iotDevice);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param iotDevice 
	* @return
	*/
	public int updateIotDevice(IotDevice iotDevice){
		int i = 0;
		try {
			iotDevice.setUpdate_id(getXtUid());
			iotDevice.setUpdate_time(getDate());
			i = iotDeviceDao.updateIotDevice(iotDevice);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param iotDevice 
	* @return
	*/
	public int updateIotDeviceBySelective(IotDevice iotDevice){
		int i = 0;
		try {
			iotDevice.setUpdate_id(getXtUid());
			iotDevice.setUpdate_time(getDate());
			i = iotDeviceDao.updateIotDeviceBySelective(iotDevice);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delIotDevice(Map<String,Object> condition){
		int i = 0;
		try {
			i = iotDeviceDao.delIotDevice(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param iotDeviceList 
	* @return
	*/
	public int updateBatchIotDevice(List<IotDevice> iotDeviceList){
		int i = 0;
		try {
			i = iotDeviceDao.updateBatchIotDevice(iotDeviceList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceList 
	* @return
	*/
	public int updateBatchIotDeviceBySelective(List<IotDevice> iotDeviceList){
		int i = 0;
		try {
			i = iotDeviceDao.updateBatchIotDeviceBySelective(iotDeviceList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}

	/**
	 * 视频
	 * @param condition
	 * @return
	 */
	public List<IotDevice> getIotVideoListByCondition(Map<String, Object> condition){
		try{
			return iotDeviceDao.getIotVideoListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
}
