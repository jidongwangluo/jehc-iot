package jehc.djshi.iot.monitor.dao;
import jehc.djshi.iot.monitor.model.GraphSite;

import java.util.List;
import java.util.Map;
/**
 * @Desc 站点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface GraphSiteDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<GraphSite> getGraphSiteListByCondition(Map<String, Object> condition);
	/**
	* 查询单条记录
	* @param graph_site_id 
	* @return
	*/
	GraphSite getGraphSiteById(String graph_site_id);
	/**
	* 添加
	* @param graphSite 
	* @return
	*/
	int addGraphSite(GraphSite graphSite);
	/**
	* 修改
	* @param graphSite 
	* @return
	*/
	int updateGraphSite(GraphSite graphSite);
	/**
	* 修改（根据动态条件）
	* @param graphSite 
	* @return
	*/
	int updateGraphSiteBySelective(GraphSite graphSite);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delGraphSite(Map<String, Object> condition);
	/**
	* 批量添加
	* @param graphSiteList 
	* @return
	*/
	int addBatchGraphSite(List<GraphSite> graphSiteList);
	/**
	* 批量修改
	* @param graphSiteList 
	* @return
	*/
	int updateBatchGraphSite(List<GraphSite> graphSiteList);
	/**
	* 批量修改（根据动态条件）
	* @param graphSiteList 
	* @return
	*/
	int updateBatchGraphSiteBySelective(List<GraphSite> graphSiteList);
}
