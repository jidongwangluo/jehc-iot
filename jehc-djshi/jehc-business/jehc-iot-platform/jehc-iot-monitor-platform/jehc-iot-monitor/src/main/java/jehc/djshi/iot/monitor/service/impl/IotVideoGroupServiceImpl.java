package jehc.djshi.iot.monitor.service.impl;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.monitor.dao.IotVideoGroupDao;
import jehc.djshi.iot.monitor.model.IotVideoGroup;
import jehc.djshi.iot.monitor.service.IotVideoGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * @Desc 相机组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("iotVideoGroupService")
public class IotVideoGroupServiceImpl extends BaseService implements IotVideoGroupService {
    @Autowired
    IotVideoGroupDao iotVideoGroupDao;

    /**
     * 分页
     * @param condition
     * @return
     */
    public List<IotVideoGroup> getIotVideoGroupByCondition(Map<String, Object> condition){
        return iotVideoGroupDao.getIotVideoGroupByCondition(condition);
    }
    /**
     * 查询对象
     * @param video_group_id
     * @return
     */
    public IotVideoGroup getIotVideoGroupById(String video_group_id){
        return iotVideoGroupDao.getIotVideoGroupById(video_group_id);
    }
    /**
     * 添加
     * @param iotVideoGroup
     * @return
     */
    public int addIotVideoGroup(IotVideoGroup iotVideoGroup){
        int i = 0;
        try{
            i = iotVideoGroupDao.addIotVideoGroup(iotVideoGroup);
        } catch (Exception e) {
            /**捕捉异常并回滚**/
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }
    /**
     * 修改
     * @param iotVideoGroup
     * @return
     */
    public int updateIotVideoGroup(IotVideoGroup iotVideoGroup){
        int i = 0;
        try{
            i = iotVideoGroupDao.updateIotVideoGroup(iotVideoGroup);
        } catch (Exception e) {
            /**捕捉异常并回滚**/
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }
    /**
     * 删除
     * @param condition
     * @return
     */
    public int delIotVideoGroup(Map<String, Object> condition){
        int i = 0;
        try{
            i = iotVideoGroupDao.delIotVideoGroup(condition);
        } catch (Exception e) {
            /**捕捉异常并回滚**/
            throw new ExceptionUtil(e.getMessage(),e.getCause());
        }
        return i;
    }
}
