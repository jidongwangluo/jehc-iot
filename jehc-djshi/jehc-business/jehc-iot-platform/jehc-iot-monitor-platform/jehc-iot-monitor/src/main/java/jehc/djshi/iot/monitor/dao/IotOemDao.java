package jehc.djshi.iot.monitor.dao;
import java.util.List;
import java.util.Map;
import jehc.djshi.iot.monitor.model.IotOem;

/**
 * @Desc 生产商
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotOemDao{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<IotOem> getIotOemListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param oem_id 
	* @return
	*/
	IotOem getIotOemById(String oem_id);
	/**
	* 添加
	* @param iotOem 
	* @return
	*/
	int addIotOem(IotOem iotOem);
	/**
	* 修改
	* @param iotOem 
	* @return
	*/
	int updateIotOem(IotOem iotOem);
	/**
	* 修改（根据动态条件）
	* @param iotOem 
	* @return
	*/
	int updateIotOemBySelective(IotOem iotOem);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delIotOem(Map<String, Object> condition);
	/**
	* 批量修改
	* @param iotOemList 
	* @return
	*/
	int updateBatchIotOem(List<IotOem> iotOemList);
	/**
	* 批量修改（根据动态条件）
	* @param iotOemList 
	* @return
	*/
	int updateBatchIotOemBySelective(List<IotOem> iotOemList);
}
