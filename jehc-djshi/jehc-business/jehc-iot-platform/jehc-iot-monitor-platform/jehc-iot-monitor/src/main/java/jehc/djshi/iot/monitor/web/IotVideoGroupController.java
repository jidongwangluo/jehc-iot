package jehc.djshi.iot.monitor.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.monitor.dao.IotVideoGroupRightDao;
import jehc.djshi.iot.monitor.model.IotVideoGroup;
import jehc.djshi.iot.monitor.model.IotVideoGroupRight;
import jehc.djshi.iot.monitor.service.IotVideoGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * @Desc 相机组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotVideoGroup")
@Api(value = "相机组",tags = "相机组", description = "相机组")
public class IotVideoGroupController  extends BaseAction {
    @Autowired
    IotVideoGroupService iotVideoGroupService;

    @Autowired
    IotVideoGroupRightDao iotVideoGroupRightDao;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    public BasePage getIotVideoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<IotVideoGroup> iotVideoGroups = iotVideoGroupService.getIotVideoGroupByCondition(condition);
        for(IotVideoGroup iotVideoGroup:iotVideoGroups){
            if(!StringUtil.isEmpty(iotVideoGroup.getCreate_id())){
                OauthAccountEntity createBy = getAccount(iotVideoGroup.getCreate_id());
                if(null != createBy){
                    iotVideoGroup.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(iotVideoGroup.getUpdate_id())){
                OauthAccountEntity modifiedBy = getAccount(iotVideoGroup.getUpdate_id());
                if(null != modifiedBy){
                    iotVideoGroup.setModifiedBy(modifiedBy.getName());
                }
            }
        }
        PageInfo<IotVideoGroup> page = new PageInfo<IotVideoGroup>(iotVideoGroups);
        return outPageBootStr(page,baseSearch);
    }


    /**
     * 查询单条记录
     * @param video_group_id
     */
    @ApiOperation(value="查询单条记录", notes="查询单条记录")
    @NeedLoginUnAuth
    @GetMapping(value="/get/{video_group_id}")
    public BaseResult getIotVideoGroupById(@PathVariable("video_group_id")String video_group_id){
        IotVideoGroup iotVideoGroup = iotVideoGroupService.getIotVideoGroupById(video_group_id);
        if(!StringUtil.isEmpty(iotVideoGroup.getCreate_id())){
            OauthAccountEntity createBy = getAccount(iotVideoGroup.getCreate_id());
            if(null != createBy){
                iotVideoGroup.setCreateBy(createBy.getName());
            }
        }
        if(!StringUtil.isEmpty(iotVideoGroup.getUpdate_id())){
            OauthAccountEntity modifiedBy = getAccount(iotVideoGroup.getUpdate_id());
            if(null != modifiedBy){
                iotVideoGroup.setModifiedBy(modifiedBy.getName());
            }
        }
        return outDataStr(iotVideoGroup);
    }
    /**
     * 添加
     * @param iotVideoGroup
     */
    @ApiOperation(value="添加", notes="添加")
    @PostMapping(value="/add")
    public BaseResult addIotVideoGroup(@RequestBody IotVideoGroup iotVideoGroup){
        int i = 0;
        if(null != iotVideoGroup){
            iotVideoGroup.setCreate_id(getXtUid());
            iotVideoGroup.setVideo_group_id(toUUID());
            iotVideoGroup.setCreate_time(getDate());
            i=iotVideoGroupService.addIotVideoGroup(iotVideoGroup);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 修改
     * @param iotVideoGroup
     */
    @ApiOperation(value="修改", notes="修改")
    @PutMapping(value="/update")
    public BaseResult updateIotVideoGroup(@RequestBody IotVideoGroup iotVideoGroup){
        int i = 0;
        if(null != iotVideoGroup){
            iotVideoGroup.setUpdate_time(getDate());
            iotVideoGroup.setUpdate_id(getXtUid());
            i=iotVideoGroupService.updateIotVideoGroup(iotVideoGroup);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
    /**
     * 删除
     * @param video_group_id
     */
    @ApiOperation(value="删除", notes="删除")
    @DeleteMapping(value="/delete")
    public BaseResult delIotVideoGroup(String video_group_id){
        int i = 0;
        if(!StringUtil.isEmpty(video_group_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("video_group_id",video_group_id.split(","));
            i=iotVideoGroupService.delIotVideoGroup(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }


    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询已配置相机", notes="查询已配置相机")
    @NeedLoginUnAuth
    @PostMapping(value="/rightList")
    public BaseResult getIotVideoRightListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        List<IotVideoGroupRight> iotVideoGroupRights = iotVideoGroupRightDao.getIotVideoGroupRightByCondition(condition);
        return new BaseResult(iotVideoGroupRights);
    }

    /**
     * 删除单个摄像头
     * @param video_group_right_id
     */
    @ApiOperation(value="删除单个摄像头", notes="删除单个摄像头")
    @DeleteMapping(value="/remove")
    public BaseResult delIotVideoGroupRight(String video_group_right_id){
        int i = 0;
        if(!StringUtil.isEmpty(video_group_right_id)){
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("video_group_right_id",video_group_right_id.split(","));
            i=iotVideoGroupRightDao.delIotVideoGroupRight(condition);
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
