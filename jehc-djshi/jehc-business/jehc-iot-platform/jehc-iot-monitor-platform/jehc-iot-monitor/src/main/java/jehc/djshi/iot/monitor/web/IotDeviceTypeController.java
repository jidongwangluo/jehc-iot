package jehc.djshi.iot.monitor.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.monitor.model.IotDeviceType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import com.github.pagehelper.PageInfo;
import jehc.djshi.iot.monitor.service.IotDeviceTypeService;
/**
 * @Desc 设备类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/iotDeviceType")
@Api(value = "设备类型",tags = "设备类型", description = "设备类型")
public class IotDeviceTypeController extends BaseAction {
	@Autowired
	private IotDeviceTypeService iotDeviceTypeService;
	/**
	 * 查询并分页
	 * @param baseSearch
	 */
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getIotDeviceTypeListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<IotDeviceType> iotDeviceTypeList = iotDeviceTypeService.getIotDeviceTypeListByCondition(condition);
		for(IotDeviceType iotDeviceType:iotDeviceTypeList){
			if(!StringUtil.isEmpty(iotDeviceType.getCreate_id())){
				OauthAccountEntity createBy = getAccount(iotDeviceType.getCreate_id());
				if(null != createBy){
					iotDeviceType.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(iotDeviceType.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(iotDeviceType.getUpdate_id());
				if(null != modifiedBy){
					iotDeviceType.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<IotDeviceType> page = new PageInfo<IotDeviceType>(iotDeviceTypeList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param device_type_id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{device_type_id}")
	public BaseResult getIotDeviceTypeById(@PathVariable("device_type_id")String device_type_id){
		IotDeviceType iotDeviceType = iotDeviceTypeService.getIotDeviceTypeById(device_type_id);
		if(!StringUtil.isEmpty(iotDeviceType.getCreate_id())){
			OauthAccountEntity createBy = getAccount(iotDeviceType.getCreate_id());
			if(null != createBy){
				iotDeviceType.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(iotDeviceType.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(iotDeviceType.getUpdate_id());
			if(null != modifiedBy){
				iotDeviceType.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(iotDeviceType);
	}
	/**
	* 添加
	* @param iotDeviceType 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addIotDeviceType(@RequestBody IotDeviceType iotDeviceType){
		int i = 0;
		if(null != iotDeviceType){
			iotDeviceType.setCreate_time(getDate());
			iotDeviceType.setDevice_type_id(toUUID());
			i=iotDeviceTypeService.addIotDeviceType(iotDeviceType);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param iotDeviceType 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateIotDeviceType(@RequestBody IotDeviceType iotDeviceType){
		int i = 0;
		if(null != iotDeviceType){
			iotDeviceType.setUpdate_time(getDate());
			i=iotDeviceTypeService.updateIotDeviceType(iotDeviceType);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param device_type_id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delIotDeviceType(String device_type_id){
		int i = 0;
		if(!StringUtil.isEmpty(device_type_id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("device_type_id",device_type_id.split(","));
			i=iotDeviceTypeService.delIotDeviceType(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}

	/**
	 * 查找全部类型
	 */
	@ApiOperation(value="查找全部类型", notes="查找全部类型")
	@NeedLoginUnAuth
	@GetMapping(value="/allList")
	public BaseResult allList(){
		Map<String,Object> condition = new HashMap<>();
		List<IotDeviceType> iotDeviceTypeList = iotDeviceTypeService.getIotDeviceTypeListByCondition(condition);
		return new BaseResult(iotDeviceTypeList);
	}

	/**
	 * 查找相机全部类型
	 */
	@ApiOperation(value="查找相机全部类型", notes="查找相机全部类型")
	@NeedLoginUnAuth
	@GetMapping(value="/videoTypeList")
	public BaseResult getVideoTypeList(){
		Map<String,Object> condition = new HashMap<>();
		List<IotDeviceType> iotDeviceTypeList = iotDeviceTypeService.getVideoTypeList(condition);
		return new BaseResult(iotDeviceTypeList);
	}
}
