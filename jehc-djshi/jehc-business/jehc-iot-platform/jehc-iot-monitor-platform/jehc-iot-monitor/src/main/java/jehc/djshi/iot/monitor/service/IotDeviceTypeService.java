package jehc.djshi.iot.monitor.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.iot.monitor.model.IotDeviceType;

/**
 * @Desc 设备类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface IotDeviceTypeService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<IotDeviceType> getIotDeviceTypeListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param device_type_id 
	* @return
	*/
	IotDeviceType getIotDeviceTypeById(String device_type_id);
	/**
	* 添加
	* @param iotDeviceType 
	* @return
	*/
	int addIotDeviceType(IotDeviceType iotDeviceType);
	/**
	* 修改
	* @param iotDeviceType 
	* @return
	*/
	int updateIotDeviceType(IotDeviceType iotDeviceType);
	/**
	* 修改（根据动态条件）
	* @param iotDeviceType 
	* @return
	*/
	int updateIotDeviceTypeBySelective(IotDeviceType iotDeviceType);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delIotDeviceType(Map<String, Object> condition);
	/**
	* 批量修改
	* @param iotDeviceTypeList 
	* @return
	*/
	int updateBatchIotDeviceType(List<IotDeviceType> iotDeviceTypeList);
	/**
	* 批量修改（根据动态条件）
	* @param iotDeviceTypeList 
	* @return
	*/
	int updateBatchIotDeviceTypeBySelective(List<IotDeviceType> iotDeviceTypeList);
	/**
	 * 查询相机类型
	 * @param condition
	 * @return
	 */
	List<IotDeviceType> getVideoTypeList(Map<String, Object> condition);
}
