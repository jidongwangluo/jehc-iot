package jehc.djshi.iot.monitor.web;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.common.entity.OauthAccountEntity;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.iot.monitor.model.IotDevice;
import jehc.djshi.iot.monitor.service.IotDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 视频
 * 2021-02-24 15:37:15  邓纯杰
 */
@RestController
@RequestMapping("/iotVideo")
@Api(value = "视频",tags = "视频", description = "视频")
public class IotVideoController extends BaseAction {
    @Autowired
    private IotDeviceService iotDeviceService;

    /**
     * 查询并分页
     * @param baseSearch
     */
    @ApiOperation(value="查询并分页", notes="查询并分页")
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    public BasePage getIotVideoListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
        Map<String, Object> condition = baseSearch.convert();
        commonHPager(baseSearch);
        List<IotDevice> iotDeviceList = iotDeviceService.getIotVideoListByCondition(condition);
        for(IotDevice iotDevice:iotDeviceList){
            if(!StringUtil.isEmpty(iotDevice.getCreate_id())){
                OauthAccountEntity createBy = getAccount(iotDevice.getCreate_id());
                if(null != createBy){
                    iotDevice.setCreateBy(createBy.getName());
                }
            }
            if(!StringUtil.isEmpty(iotDevice.getUpdate_id())){
                OauthAccountEntity modifiedBy = getAccount(iotDevice.getUpdate_id());
                if(null != modifiedBy){
                    iotDevice.setModifiedBy(modifiedBy.getName());
                }
            }
        }
        PageInfo<IotDevice> page = new PageInfo<IotDevice>(iotDeviceList);
        return outPageBootStr(page,baseSearch);
    }
}
