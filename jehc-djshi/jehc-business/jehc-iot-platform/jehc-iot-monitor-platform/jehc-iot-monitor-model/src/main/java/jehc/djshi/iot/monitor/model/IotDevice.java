package jehc.djshi.iot.monitor.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
/**
 * @Desc 设备基础信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotDevice extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String device_id;/**设备id**/
	private String device_type_id;/**设备类型id**/
	private String device_code;/**设备编号**/
	private String name;/**设备名称**/
	private String short_name;/**简写**/
	private String remarks;/**备注**/
	private String build_pos;/**桩号**/
	private String build_pos_content;/**桩号描述**/
	private String oem_id;/**生产商id**/
	private String model_id;/**型号id**/
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date produce_date;/**生产日期**/
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date scrap_time;/**设备报废时间**/
	private Integer transcribe;/**视频录制0否1录制中**/
	private String oem_name;/**生产商名称**/
	private String model_name;/**型号名称**/
	private String device_type_name;/**设备类型名称**/
	private String device_location_id;/**设备位置id**/
	private String location_name;/**位置名称id**/
	private String tag;//设备类型标记

}
