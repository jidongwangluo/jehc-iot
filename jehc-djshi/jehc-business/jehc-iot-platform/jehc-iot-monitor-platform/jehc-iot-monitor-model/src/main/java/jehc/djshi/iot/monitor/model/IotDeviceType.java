package jehc.djshi.iot.monitor.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 设备类型
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotDeviceType extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String device_type_id;/**主键**/
	private String device_type_name;/**类型名称**/
	private String device_type_remarks;/**设备类型描述**/
	private String tag;/**标签唯一**/
}
