package jehc.djshi.iot.monitor.model;
import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
/**
 * @Desc 生产商
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotOem extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String oem_id;/**主键**/
	private String oem_name;/**公司名称**/
	private String short_oem_name;/**简写**/
	private String remarks;/**描述**/
	private String xt_provinceID;/**省份id**/
	private String xt_cityID;/**城市id**/
	private String xt_districtID;/**区县id**/
	private String address;/**详细地址**/
	private String oem_tel;/**公司电话**/
	private String legal_person;/**法人**/
	private String oem_type;/**公司性质**/
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date registration_date;/**成立时间**/
}
