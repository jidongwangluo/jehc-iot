package jehc.djshi.iot.monitor.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
/**
 * @Desc 设备位置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotDeviceLocation extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String device_location_id;/**主键**/
	private String location_name;/**位置名称**/
	private String remarks;/**描述**/
	private String create_id;/**创建人**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	private String update_id;/**修改人**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private Integer del_flag;/**删除标记：0正常1删除**/
}
