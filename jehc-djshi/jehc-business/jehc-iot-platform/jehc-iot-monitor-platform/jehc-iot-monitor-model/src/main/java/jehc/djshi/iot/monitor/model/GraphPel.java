package jehc.djshi.iot.monitor.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
/**
 * @Desc 图元管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class GraphPel extends BaseEntity implements Serializable {
    private String graph_pel_id;
    private String graph_pel_title;//标题
    private String graph_pel_key;//键
    private int width;//宽
    private int height;//高
    private String hideLabel;//隐藏Label
    private String hideTitles;//隐藏标题
    private String svgHtml;//SVG（存放SVG文件）
    private String graph_pel_type;//图元类型
    private String graph_pel_deleted;//是否删除0正常1删除
    private int graph_pel_sort;//排序
    private String graph_pel_cell_type;//图元节点类型（0.svg,1.image,2.其它）
    private String graph_pel_category;//图元分类（0其它，1电力，2工业）
    private String graph_pel_status;//状态 0未发布 1已发布
    private int render_method;//svg渲染方式0按元素列表组装渲染 1按svghtml文件渲染
    private List<GraphPelElement> graphPelElements;
}
