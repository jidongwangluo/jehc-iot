package jehc.djshi.iot.monitor.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 设备型号
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotModel extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String model_id;/**型号id**/
	private String model_name;/**型号名称**/
	private String short_model_name;/**简写**/
	private String oem_id;/**生产商id**/
	private String oem_name;/**生产商名称**/
}
