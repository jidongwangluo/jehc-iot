package jehc.djshi.iot.monitor.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
/**
 * @Desc 站点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class GraphSite extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String graph_site_id;/**站点主键**/
	private String graph_site_name;/**名称**/
	private Integer graph_site_status;/**状态（0关闭1发布）**/
	private String graph_site_mxgraph;/**存储值**/
	private String xt_userinfo_id;/**创建人**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date cTime;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date mTime;/**最后修改时间**/
	private String update_userinfo_id;/**最后修改人**/
	private String content;/**内容**/
	private String background;/**背景颜色**/
	private String gridColor;/**网格颜色**/
	private Integer showGrid;/**显示网格（0是1否）**/
	private Integer isDelete;/**是否删除0正常1删除**/
	private Integer gridSize;//网格大小
	private String createUser;
	private String updateUser;
}
