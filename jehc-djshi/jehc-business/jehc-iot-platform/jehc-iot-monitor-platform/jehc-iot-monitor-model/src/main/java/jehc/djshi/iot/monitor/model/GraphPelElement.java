package jehc.djshi.iot.monitor.model;


import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @Desc 图元节点
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class GraphPelElement extends BaseEntity implements Serializable {
    private String graph_pel_element_id;
    private String graph_pel_id;//图元id外键
    private String graph_pel_element_categray;//节点类型（如Path,ellipse,circle,rect,line,polygon,polyline,text）
    private String graph_pel_element_attrs;//节点属性（如：{d:"M734,931 l0,2 -28,13 -32,7 -32,4 -36,4 -28,3 -29,2 -31,1 -32,1 -34,1 -35,1 -18,0 -18,0 -36,0 -35,-1 -34,-1 -32,-1 -31,-1 -29,-2 -27,-3 -37,-4 -32,-4 -32,-7 -24,-9 -4,-4 0,-2 0,-785 11,-29 24,-22 25,-15 32,-13 37,-12 27,-7 29,-5 31,-5 32,-5 34,-2 35,-2 18,-1 18,0 36,1 35,2 34,2 32,5 31,5 29,5 28,7 25,8 33,13 28,14 21,16 17,22 4,12 0,6 0,785z",style:"fill:#b2b29b;"}）
    private String fills;//填充颜色
    private String stock;//线条颜色
    private int stroke_width;//线框宽度
    private int graph_pel_element_sort;//排序

    private int graph_pel_element_cx;//cx定义圆心横坐标
    private int graph_pel_element_cy;//cy定义圆心纵坐标
    private int graph_pel_element_r;//r定义圆的半径
    private String graph_pel_element_opacity;//透明度 如.4
    private String stroke_opacity;//线透明度如.4
    private String fill_opacity;//填充透明度如.4
}
