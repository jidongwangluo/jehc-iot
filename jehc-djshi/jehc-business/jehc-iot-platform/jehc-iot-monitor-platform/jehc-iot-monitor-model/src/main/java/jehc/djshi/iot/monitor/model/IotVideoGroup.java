package jehc.djshi.iot.monitor.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
/**
 * @Desc 相机组
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotVideoGroup extends BaseEntity implements Serializable {
    private String video_group_id;/**主键**/
    private String group_name;/**组名称**/
    private String remarks;/**描述**/
}
