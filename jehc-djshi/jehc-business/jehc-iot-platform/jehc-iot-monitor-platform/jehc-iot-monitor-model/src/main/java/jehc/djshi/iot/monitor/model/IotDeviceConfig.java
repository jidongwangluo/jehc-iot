package jehc.djshi.iot.monitor.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
/**
 * @Desc 设备配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotDeviceConfig extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String device_config_id;/**主键**/
	private String ip;/**ip地址**/
	private int port;/**端口**/
	private String user_name;/**用户名**/
	private String password;/**密码**/
	private int channel_no;/**摄像机对应视频服务器的通道号（只用于摄像机）**/
	private String device_id;/**设备id**/
	private int rtsp_port;/**rtsp取流端口**/
	private int nvr_channel;/**NVR存储通道号**/
	private String protocol;/**通信协议：udp,tcp,rtu等等**/
	private IotDevice iotDevice;/**设备**/
	private int type;/**类型（摄像机：0海康1大华2宇视，其它：待定）**/
	private Integer subtype=1;//码流类型0主码流1子码流2辅码流（默认子码流）
}
