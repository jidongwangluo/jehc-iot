package jehc.djshi.iot.monitor.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
/**
 * @Desc 相机组关联
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class IotVideoGroupRight extends BaseEntity implements Serializable {
    private String video_group_right_id;/**主键**/
    private String video_group_id;/**相机组主键**/
    private String device_id;/**设备编号**/
    private Integer sort;/**排序号**/


    private String device_code;/**设备编号**/
    private String name;/**设备名称**/
    private String device_type_name;/**设备类型名称**/
    private String device_location_id;/**设备位置id**/
    private String location_name;/**位置名称id**/
}
