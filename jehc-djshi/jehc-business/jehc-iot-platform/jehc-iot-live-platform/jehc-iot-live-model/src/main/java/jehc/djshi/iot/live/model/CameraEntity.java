package jehc.djshi.iot.live.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import jehc.djshi.common.util.StringUtil;
import lombok.Data;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;

import java.io.Serializable;
import java.util.Date;

/**
 * @Desc 摄像机参配置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class CameraEntity implements Serializable {
	private static final long serialVersionUID = 8183688502930584159L;
	@ApiModelProperty(value="摄像头账号",required=true)
	private String username;// 摄像头账号

	@ApiModelProperty(value="摄像头密码",required=true)
	private String password;// 摄像头密码

	@ApiModelProperty(value="端口",required=true)
	private Integer port;//端口

	@ApiModelProperty(value="摄像头ip",required=true)
	private String ip;// 摄像头ip

	@ApiModelProperty(value="摄像头通道号",required=true)
	private String channel;// 摄像头通道（通道号）

	@ApiModelProperty(value="类型",required=true)
	private String type;// 类型（0海康1大华2宇视 以此类推）

	@ApiModelProperty(value="唯一编号",required=true)
	private String stream;// 摄像头码流 （即推送至Nginx服务返回URL最后参数（采用deviceId））

	@ApiModelProperty(value="rtsp",hidden=true)
	private String rtsp;// rtsp地址

	@ApiModelProperty(value="rtmp",hidden=true)
	private String rtmp;// rtmp地址

	@ApiModelProperty(value="url")
	private String url;// 播放地址 作为参数用于录制地址传入

	@ApiModelProperty(value="回放开始时间",name="startTime")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date startTime;// 回放开始时间

	@ApiModelProperty(value="回放结束时间",name="endTime")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date endTime;// 回放结束时间

	@ApiModelProperty(value="本次打开时间",name="openTime")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date openTime;// 打开时间

	@ApiModelProperty(value="使用人数",hidden=true)
	private Integer count = 0;// 使用人数

	@ApiModelProperty(value="失败次数",hidden=true)
	private int errorCount=0;//失败次数

	@ApiModelProperty(value="token",name="token")
	private String token;

	@ApiModelProperty(value="httpURL",hidden=true)
	private String httpURL;//HTTP地址

	@ApiModelProperty(value="是否重播",name="times")
	private Boolean reStart = false;//是否重播（用于历史回放）

	@ApiModelProperty(value="已回放时长",name="times")
	private Long times;//已播放时长（用于回放）

	@ApiModelProperty(value="Graber",hidden=true)
	private FFmpegFrameGrabber grabber;

	@ApiModelProperty(value="Graber",hidden=true)
	private FFmpegFrameRecorder record;

	@ApiModelProperty(value="是否开启录制",name="controlTranscribe")
	private Boolean controlTranscribe;//是否开启录制 true开启录制 false停止录制

	@ApiModelProperty(value="视频存储路径",name="path")
	private String path;//视频存储路径

	@ApiModelProperty(value="视频名称",name="videoName")
	private String videoName;//视频名称

	@ApiModelProperty(value="视频类型",name="videoType")
	private String videoType;//视频类型如：mp4等

	@ApiModelProperty(value="是否录制音频0:不录制/1:录制",name="audio")
	private int audio = 1;//是否录制音频0:不录制/1:录制

	@ApiModelProperty(value="第几帧（用于截图）",name="frame")
	private int frame = 5;//第几帧（用于截图）

	@ApiModelProperty(value="图片类型",name="imageType")
	private String imageType = "jpg";//图片类型（用于截图）,默认jpg

	@ApiModelProperty(value="是否推送音频（默认否）",name="pushAudio")
	private boolean pushAudio = false;//是否推送音频（默认否）

	@ApiModelProperty(value="码流",name="subtype")
	private Integer subtype=1;//码流类型0主码流1子码流2辅码流（默认子码流）

	/**
	 * 获取 Rtsp组装后的地址
	 * @return
	 */
	public String getRtspUrl(){
		String rtspUrl = null;
		String type = this.getType();
		if(!StringUtil.isEmpty(type)){
			switch (type){
				case "0"://海康
					rtspUrl = "rtsp://" + this.username + ":" + this.password + "@" + ip + ":"+port+"/h264/" + this.channel + "/" + this.stream + "/av_stream";//海康取码
					break;
				case "1"://大华
					rtspUrl = "rtsp://" + this.username + ":" + this.password + "@" + ip + ":"+port+"/cam/realmonitor?channel=" + this.channel + "&subtype=0";//大华取码
					break;
				case "2"://宇视
				/*
				rtsp://{用户名}:{密码}@{ip}:{port}/video1/2/3，分别对应主/辅/三码流；
				rtsp://admin:admin@192.168.8.8:554/video1，就表示主码流；
				rtsp://admin:admin@192.168.8.8:554/video2，表示子码流；
				rtsp://admin:admin@192.168.8.8:554/video3，表示3码流；
				*/
					rtspUrl = "rtsp://" + this.username + ":" + this.password + "@" + ip + ":"+port+"/video2";//宇视取码
					break;
				default:
					break;
			}
		}
		return rtspUrl;
	}
}
