package jehc.djshi.iot.live.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Data
public class Config {
    @Value("${jehc.stream.nginx.ip:}")
    private String streamNginxIp;//目标地址（推送Ngin地址）
    @Value("${jehc.stream.nginx.tcp.port:}")
    private String streamNginxTcpPort;//目标端口（推送Ngin端口）
    @Value("${jehc.stream.ipExtra:}")
    private String ipExtra;//推送额外地址
    @Value("${jehc.stream.mainCode:}")
    private String mainCode;// 主码流最大码率
    @Value("${jehc.stream.subCode:}")
    private String subCode;// 主码流最大码率

}
