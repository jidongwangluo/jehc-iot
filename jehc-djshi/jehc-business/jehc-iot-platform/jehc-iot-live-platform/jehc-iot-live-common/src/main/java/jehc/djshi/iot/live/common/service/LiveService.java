package jehc.djshi.iot.live.common.service;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.iot.live.model.CameraEntity;
/**
 * @Desc 视频直播流
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface LiveService {
    /**
     * 实时预览
     * @param cameraEntity
     * @return
     */
    BaseResult live(CameraEntity cameraEntity);

    /**
     * 关闭实时预览
     * @param cameraEntity
     * @return
     */
    BaseResult stopLive(CameraEntity cameraEntity);

    /**
     * 录制视频
     * @param cameraEntity
     * @return
     */
    BaseResult startTranscribe(CameraEntity cameraEntity);

    /**
     * 停止录制
     * @param cameraEntity
     * @return
     */
    BaseResult stopTranscribe(CameraEntity cameraEntity);

    /**
     * 截图
     * @param cameraEntity
     * @return
     */
    BaseResult screenShot(CameraEntity cameraEntity);
}
