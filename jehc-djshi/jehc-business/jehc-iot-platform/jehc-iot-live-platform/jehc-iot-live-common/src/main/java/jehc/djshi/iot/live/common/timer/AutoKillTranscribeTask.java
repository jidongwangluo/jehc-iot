package jehc.djshi.iot.live.common.timer;

import jehc.djshi.iot.live.common.util.CacheUtil;
import jehc.djshi.iot.live.model.CameraEntity;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
/**
 * @Desc 定时任务（杀进程使用,针对录制视频停止使用）
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
public class AutoKillTranscribeTask implements CommandLineRunner {
    private final static Logger logger = LoggerFactory.getLogger(AutoKillTranscribeTask.class);
    public static Timer timer;
    @Override
    public void run(String... args) throws Exception {
        timer = new Timer("timeTimer");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                killTask(null);
            }
        }, 1, 1000 * 60);
    }


    public void killTask(String token){
		/*
		logger.info("当前线程总数..."+Thread.getAllStackTraces().keySet().size());
		int nbRunning = 0;
		for (Thread t : Thread.getAllStackTraces().keySet()) {
			if (t.getState()==Thread.State.RUNNABLE){
				logger.info("当前线程运行状况..."+t.getName()+"----"+t.getId());
				nbRunning++;
			}
			logger.info("当前线程运行状况，getName---"+t.getName()+"--getId--"+t.getId()+"--getState--"+t.getState()+"--isAlive--"+t.isAlive()+"--isInterrupted--"+t.isInterrupted());
		}
		*/
        if(StringUtils.isEmpty(token)){
            // 管理缓存
			if (null != CacheUtil.transcribeMap && 0 != CacheUtil.transcribeMap.size()) {
				//废弃0人访问终止线程
				Set<String> keys = CacheUtil.transcribeMap.keySet();
				for (String key : keys) {
					// 如果通道使用人数为0，则关闭推流
					if (CacheUtil.transcribeMap.get(key).getCount() == 0) {// 结束线程
                        logger.info("--------终止录制视频开始：{}",token);
                        CameraEntity cameraEntity = CacheUtil.transcribeMap.get(token);
						try {
                            cameraEntity.setErrorCount(101);
                            CacheUtil.transcribeJobMap.get(key).setInterrupted(cameraEntity);
						}catch (Exception e){
							logger.info("--------终止录制视频线程异常：{}",e);
						}
                        logger.info("--------终止录制视频完毕:{}",token);
					}
				}
			}
        }else{
            if (null != CacheUtil.transcribeMap && null != CacheUtil.transcribeMap.get(token)) {
                logger.info("--------终止录制视频开始：{}",token);
                // 结束线程
                if(null != CacheUtil.transcribeJobMap && null != CacheUtil.transcribeJobMap.get(token)){
                    CameraEntity cameraEntity = CacheUtil.transcribeMap.get(token);
                    try {
                        cameraEntity.setErrorCount(101);
                        CacheUtil.transcribeJobMap.get(token).setInterrupted(cameraEntity);
                    }catch (Exception e){
                        logger.info("--------终止录制视频线程异常：",token);
                    }
                }
                logger.info("--------终止录制视频完毕:{}",token);
            }
        }
    }
}
