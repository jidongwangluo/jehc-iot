package jehc.djshi.iot.live.common.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
/**
 * @Desc IP工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class IpUtil {
	public static String IpConvert(String domainName) {
		String ip = domainName;
		try {
			ip = InetAddress.getByName(domainName).getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return domainName;
		}
		return ip;
	}
}
