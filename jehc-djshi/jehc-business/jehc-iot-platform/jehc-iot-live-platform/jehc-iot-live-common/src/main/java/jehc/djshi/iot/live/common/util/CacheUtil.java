package jehc.djshi.iot.live.common.util;

import jehc.djshi.iot.live.common.worker.TranscribePusher;
import jehc.djshi.iot.live.model.CameraEntity;
import jehc.djshi.iot.live.common.worker.FlexPusher;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/**
 * @Desc 推流缓存信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public final class CacheUtil {
    /**
     * 存放任务 线程
     */
    public static Map<String, FlexPusher.MyRunnable> jobMap = new ConcurrentHashMap<>();

    /*
     * 保存已经开始推的流
     */
    public static Map<String, CameraEntity> STREAMMAP = new ConcurrentHashMap<String, CameraEntity>();

    /*
     * 保存服务启动时间
     */
    public static long STARTTIME;

    /*
     * 保存已录制视频线程
     */
    public static Map<String, CameraEntity> transcribeMap = new ConcurrentHashMap<String, CameraEntity>();

    /**
     * 存放任务 录制线程
     */
    public static Map<String,TranscribePusher.TranscribePusherRunnable> transcribeJobMap = new ConcurrentHashMap<>();

    /**
     * 视频当前播放人数
     */
    public static Map<String,Long> onLineJobMap = new ConcurrentHashMap<>();
}
