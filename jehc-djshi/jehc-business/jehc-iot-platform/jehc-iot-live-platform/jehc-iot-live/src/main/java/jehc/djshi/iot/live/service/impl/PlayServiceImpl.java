package jehc.djshi.iot.live.service.impl;

import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.iot.live.common.service.LiveService;
import jehc.djshi.iot.live.model.CameraEntity;
import jehc.djshi.iot.live.service.PlayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Desc 视频流接口
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("playService")
public class PlayServiceImpl implements PlayService {

    @Autowired
    private LiveService liveService;

    /**
     * 视频流包含直播，回放等
     * @param cameraEntity
     * @return
     */
    @PostMapping("/play")
    @NeedLoginUnAuth
    @ApiOperation(value="视频流",notes = "视频流包含直播，回放等")
    public BaseResult<String> play(CameraEntity cameraEntity){
        BaseResult baseResult = liveService.live(cameraEntity);
        return baseResult;
    }

    /**
     * 关闭实时预览视频
     * @param cameraEntity
     * @return
     */
    @GetMapping("/stop")
    @NeedLoginUnAuth
    @ApiOperation(value="停止实时视频",notes = "停止实时视频")
    public BaseResult<String> stop(CameraEntity cameraEntity){
        BaseResult baseResult = liveService.stopLive(cameraEntity);
        return baseResult;
    }

    /**
     * 开始录制视频
     * @param cameraEntity
     * @return
     */
    public BaseResult<String> transcribe(CameraEntity cameraEntity){
        BaseResult baseResult = liveService.startTranscribe(cameraEntity);
        return baseResult;
    }

    /**
     * 停止录制
     * @param cameraEntity
     * @return
     */
    public BaseResult<String> stopTranscribe(CameraEntity cameraEntity){
        BaseResult baseResult = liveService.stopTranscribe(cameraEntity);
        return baseResult;
    }

    /**
     * 截图
     * @param cameraEntity
     * @return
     */
    public BaseResult<String> screenShot(CameraEntity cameraEntity){
        BaseResult baseResult = liveService.screenShot(cameraEntity);
        return baseResult;
    }
}
