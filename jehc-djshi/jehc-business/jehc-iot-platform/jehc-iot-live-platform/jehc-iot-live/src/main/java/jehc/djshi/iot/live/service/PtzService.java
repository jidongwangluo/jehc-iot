package jehc.djshi.iot.live.service;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.iot.live.unvif.entity.UnVifInfo;

/**
 * @Desc 云控
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface PtzService {
    /**
     * 开始控制
     * @param unVifInfo
     * @return
     */
    BaseResult<String> control(UnVifInfo unVifInfo);
}
