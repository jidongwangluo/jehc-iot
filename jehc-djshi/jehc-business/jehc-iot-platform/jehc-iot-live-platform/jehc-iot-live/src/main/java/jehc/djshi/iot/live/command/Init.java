package jehc.djshi.iot.live.command;

import jehc.djshi.iot.live.common.worker.KeepAliveWorker;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
/**
 * @Desc 初始化保活线程
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
public class Init implements CommandLineRunner {

    /**
     * 执行线程
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        KeepAliveWorker keepAliveWorker = new KeepAliveWorker();
        keepAliveWorker.start();
    }
}