package jehc.djshi.iot.live.command;
import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Slf4j
public class FFmpegInit implements CommandLineRunner {


    /**
     * 初始化FFmpeg
     * @param args
     */
    @Override
    public void run(String... args){
        try {
            log.info("初始化FFmpeg开始...");
            FFmpegFrameGrabber.tryLoad();
            FFmpegFrameRecorder.tryLoad();
            log.info("初始化FFmpeg结束...");
        }catch (Exception e){
            e.printStackTrace();
            log.error("初始化FFmpegFrame失败，"+e.getMessage());
        }
    }
}
