package jehc.djshi.iot.live.service;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.iot.live.model.CameraEntity;

public interface PlayService {
    /**
     * 视频流包含直播，回放等
     * @param cameraEntity
     * @return
     */
    BaseResult<String> play(CameraEntity cameraEntity);

    /**
     * 关闭实时预览视频
     * @param cameraEntity
     * @return
     */
    BaseResult<String> stop(CameraEntity cameraEntity);

    /**
     * 开始录制视频
     * @param cameraEntity
     * @return
     */
    BaseResult<String> transcribe(CameraEntity cameraEntity);

    /**
     * 停止录制
     * @param cameraEntity
     * @return
     */
    BaseResult<String> stopTranscribe(CameraEntity cameraEntity);

    /**
     * 截图
     * @param cameraEntity
     * @return
     */
    BaseResult<String> screenShot(CameraEntity cameraEntity);

}
