package jehc.djshi.iot.live.service.impl;

import jehc.djshi.common.base.BaseResult;
import jehc.djshi.iot.live.service.PtzService;
import jehc.djshi.iot.live.unvif.entity.UnVifInfo;
import jehc.djshi.iot.live.unvif.util.UnVifUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Desc 云控
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("ptzService")
public class PtzServiceImpl implements PtzService{

    @Autowired
    UnVifUtil unVifUtil;

    /**
     * 开始控制
     * @param unVifInfo
     * @return
     */
    public BaseResult<String> control(UnVifInfo unVifInfo){
        BaseResult baseResult = unVifUtil.control(unVifInfo);
        return baseResult;
    }
}
