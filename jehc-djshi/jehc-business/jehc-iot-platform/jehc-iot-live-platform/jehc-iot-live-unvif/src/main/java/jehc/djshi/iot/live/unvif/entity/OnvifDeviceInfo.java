package jehc.djshi.iot.live.unvif.entity;

import de.onvif.soap.OnvifDevice;
import lombok.Data;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OnvifDeviceInfo {
    private OnvifDevice onvifDevice;
    private String profileToken;
}
