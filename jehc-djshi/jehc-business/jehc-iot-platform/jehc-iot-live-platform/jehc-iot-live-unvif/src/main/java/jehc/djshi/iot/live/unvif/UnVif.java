package jehc.djshi.iot.live.unvif;

import jehc.djshi.iot.live.unvif.entity.OnvifDeviceInfo;
import jehc.djshi.iot.live.unvif.entity.OnvifDeviceInfo;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class UnVif {
    /**
     * OnvifDevice Hash
     */
    public static final Map<String,OnvifDeviceInfo> OnvifDeviceCache = new ConcurrentHashMap<>();

    /**
     * 获取OnvifDevice
     * @param key
     * @return
     */
    public static OnvifDeviceInfo getOnvifDevice(String key){
        if(CollectionUtils.isEmpty(OnvifDeviceCache)){
            return null;
        }
        return OnvifDeviceCache.get(key);
    }
}
