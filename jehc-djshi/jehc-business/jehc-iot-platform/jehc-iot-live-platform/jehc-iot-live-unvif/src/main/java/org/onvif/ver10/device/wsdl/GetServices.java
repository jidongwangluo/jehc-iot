package org.onvif.ver10.device.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java-Klasse f�r anonymous complex type.
 * 
 * <p>
 * Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * <complexType>
 *   <complexContent>
 *     <restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       <sequence>
 *         <element name="IncludeCapability" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       </sequence>
 *     </restriction>
 *   </complexContent>
 * </complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "includeCapability" })
@XmlRootElement(name = "GetServices")
public class GetServices {

	@XmlElement(name = "IncludeCapability")
	protected boolean includeCapability;

	/**
	 * Ruft den Wert der includeCapability-Eigenschaft ab.
	 * 
	 */
	public boolean isIncludeCapability() {
		return includeCapability;
	}

	/**
	 * Legt den Wert der includeCapability-Eigenschaft fest.
	 * 
	 */
	public void setIncludeCapability(boolean value) {
		this.includeCapability = value;
	}

}
