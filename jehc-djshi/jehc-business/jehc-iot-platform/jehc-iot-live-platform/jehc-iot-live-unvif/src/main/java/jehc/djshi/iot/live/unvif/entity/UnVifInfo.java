package jehc.djshi.iot.live.unvif.entity;

import lombok.Data;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class UnVifInfo {
    /**
     * 摄像机编号 全局唯一
     */
    private String device_id;

    /**
     * 是否控制：0停 1控
     */
    private int start;

    /**
     * 转动方式如下：
     * TILT_UP 上仰
     * TILT_DOWN 下俯
     * PAN_LEFT 左转
     * PAN_RIGHT 右转
     * UP_LEFT 左上转
     * UP_RIGHT 右上转
     * DOWN_LEFT 左下转
     * DOWN_RIGHT 右下转
     * ZOOM_IN 放大
     * ZOOM_OUT 缩小
     */
    private String pztType;

    /**
     * 速度1,2,3,4,5默认1
     */
    private Integer ptzSpeed;

    /**
     * 摄像机地址 （摄像机ip 如果端口非80则ip格式如：127.0.0.1:8081）
     */
    private String ip;

    /**
     * web 端口
     */
    private int port;

    /**
     * 摄像机登录用户名
     */
    private String userName;

    /**
     * 摄像机登录密码
     */
    private String password;

}
