package jehc.djshi.iot.common.vo;
/**
 * @Desc
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class ModBus4jConstant {

    /////////寄存器类型/////////
    public static final String REGISTER = "Register";//保持寄存器
    public static final String INPUT ="Input";//输入状态开关量
    public static final String COIL="Coil";//线圈状态开关量
    public static final String INPUT_REGISTER="InputRegister";//输入寄存器



    /////////通信协议/////////
    public static final String RTU = "RTU";//RTU协议
    public static final String UDP ="UDP";//UDP协议
    public static final String ASCII="ASCII";//ASCII
    public static final String TCP="TCP";//TCP协议
}
