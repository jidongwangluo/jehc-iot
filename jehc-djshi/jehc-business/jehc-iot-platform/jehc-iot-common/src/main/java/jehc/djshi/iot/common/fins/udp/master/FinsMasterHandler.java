package jehc.djshi.iot.common.fins.udp.master;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import jehc.djshi.iot.common.fins.core.FinsFrame;
import jehc.djshi.iot.common.fins.core.FinsFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FinsMasterHandler extends SimpleChannelInboundHandler<FinsFrame> {

	final static Logger logger = LoggerFactory.getLogger(FinsMasterHandler.class);
	
	private final FinsNettyUdpMaster master;

	public FinsMasterHandler(final FinsNettyUdpMaster master) {
		this.master = master;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FinsFrame frame) throws Exception {
		this.master.getSendFuture().complete(frame);
		ReferenceCountUtil.release(frame);
	}	
	
}
