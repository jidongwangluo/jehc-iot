package jehc.djshi.iot.common.fins.core;

public enum FinsResponseAction {
	RESPONSE_REQUIRED, RESPONSE_NOT_REQUIRED;
}
