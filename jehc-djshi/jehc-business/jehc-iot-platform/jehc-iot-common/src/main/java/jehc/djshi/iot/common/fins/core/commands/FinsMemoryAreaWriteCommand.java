package jehc.djshi.iot.common.fins.core.commands;

import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsIoAddress;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsIoAddress;

public abstract class FinsMemoryAreaWriteCommand extends SimpleAddressableFinsCommand {

	public FinsMemoryAreaWriteCommand(FinsCommandCode commandCode, FinsIoAddress ioAddress) {
		super(commandCode, ioAddress);
	}
	
}
