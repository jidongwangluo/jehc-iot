package jehc.djshi.iot.common.fins.core.commands;

import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsIoAddress;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsIoAddress;

public abstract class SimpleAddressableFinsCommand extends SimpleFinsCommand {

	private final FinsIoAddress ioAddress;

	public SimpleAddressableFinsCommand(final FinsCommandCode commandCode, final FinsIoAddress ioAddress) {
		super(commandCode);
		this.ioAddress = ioAddress;
	}

	public FinsIoAddress getIoAddress() {
		return this.ioAddress;
	}

}
