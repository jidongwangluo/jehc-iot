package jehc.djshi.iot.common.fins.core.commands;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;

public abstract class SimpleFinsCommand implements FinsCommand {

	private final FinsCommandCode commandCode;

	protected SimpleFinsCommand(FinsCommandCode commandCode) {
		this.commandCode = commandCode;
	}

	@Override
	public FinsCommandCode getCommandCode() {
		return this.commandCode;
	}

}
