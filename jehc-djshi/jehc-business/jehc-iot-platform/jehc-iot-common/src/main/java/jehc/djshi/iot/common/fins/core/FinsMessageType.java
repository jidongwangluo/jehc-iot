package jehc.djshi.iot.common.fins.core;

public enum FinsMessageType {
	COMMAND, RESPONSE;
}
