package jehc.djshi.iot.common.fins.core.commands;


import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;

public interface FinsCommand {

	FinsCommandCode getCommandCode();
	
	byte[] getBytes();
	
}
