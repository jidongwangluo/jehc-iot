package jehc.djshi.iot.common.vo;

import lombok.Data;

import java.util.List;

/**
 * @Desc ModBus4j实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class ModBus4jEntity {
    private String ip;/**IP地址**/
    private Integer port;/**端口**/
    private String protocol;/**通信协议如：RTU，UDP，ASCII，TCP等等**/

    private Integer slaveId;/**slaveId，从站编号一般从1,2,3...开始**/
    private Integer offset;/**读取位置或起始位置偏移量值（即地址）**/
    private Integer dataType;/**数据类型,来自com.serotonin.modbus4j.code.DataType**/
    private Integer bit;/**BIT位**/
    private String registerType;/**寄存器类型（Coil:线圈状态开关量，Input：输入状态开关量，Register：保持寄存器，InputRegister：输入寄存器值**/

    private Boolean writeCoilValue;/**写开关数据值 布尔类型：true/false **/
    private Short writeValue;/**写数据值**/

    private Number value;/**写入数字类型的模拟量（如:写入Float类型的模拟量、Double类型模拟量、整数类型Short、Integer、Long）**/

    private Integer timeout;/**设置超时时间**/
    private Integer retries;/**设置重试次数**/

    private List<ModBus4jEntity> modBus4jEntityList;



    /////////////构造RTU及ASCII ModBusMaster参数开始/////////////////
    private String commPortId = "COM2";
    private Integer baudRate = 9600;
    private Integer flowControlIn = 0;
    private Integer flowControlOut = 0;
    private Integer dataBits = 8;
    private Integer stopBits = 1;
    private Integer parity = 0;
    /////////////构造RTU及ASCII ModBusMaster参数结束/////////////////
}
