package jehc.djshi.iot.common.fins.test;

import jehc.djshi.iot.common.fins.core.FinsIoAddress;
import jehc.djshi.iot.common.fins.core.FinsIoMemoryArea;
import jehc.djshi.iot.common.fins.core.FinsMasterException;
import jehc.djshi.iot.common.fins.core.FinsNodeAddress;
import jehc.djshi.iot.common.fins.udp.master.FinsNettyUdpMaster;
import jehc.djshi.iot.common.fins.core.FinsIoAddress;
import jehc.djshi.iot.common.fins.core.FinsIoMemoryArea;
import jehc.djshi.iot.common.fins.core.FinsMasterException;
import jehc.djshi.iot.common.fins.core.FinsNodeAddress;
import jehc.djshi.iot.common.fins.udp.master.FinsNettyUdpMaster;

import java.net.InetSocketAddress;

/**
 * UDP 测试
 */
public class UdpMasterTest {
    public static void main(String... args) throws FinsMasterException {
        FinsNettyUdpMaster finsMaster = new FinsNettyUdpMaster(
                new InetSocketAddress("192.168.89.251", 9600),
                new InetSocketAddress("0.0.0.0", 9601),
                new FinsNodeAddress(0,  2,  0)
        );

        FinsNodeAddress destNode = new FinsNodeAddress(0,  10,  0);

        finsMaster.connect();
//		//short d = finsMaster.readWord(destNode, new FinsIoAddress(FinsIoMemoryArea.DM_WORD, 13000));
        String s = finsMaster.readString(destNode, new FinsIoAddress(FinsIoMemoryArea.DM_WORD, 1), 20);
        System.out.println(String.format("%s", s.trim()));

//        Integer addr = Integer.parseInt(args[1]);
//        Integer num = Integer.parseInt(args[2]);
//        List<Short> words = finsMaster.readWords(destNode, new FinsIoAddress(FinsIoMemoryArea.DM_WORD, addr), num);
//
//        System.out.println(String.format("Read %d words starting at address %d", num, addr));
//        System.out.println(words.stream().map(s -> s.toString()).collect(Collectors.joining(", ")));
//
        finsMaster.disconnect();
    }
}
