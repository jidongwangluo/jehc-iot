package jehc.djshi.iot.common.fins.core;

import jehc.djshi.iot.common.fins.core.commands.FinsMemoryAreaWriteCommand;
import jehc.djshi.iot.common.fins.core.commands.FinsMemoryAreaWriteResponse;

@FunctionalInterface
public interface MemoryAreaWriteCommandHandler {

	FinsMemoryAreaWriteResponse handle(FinsMemoryAreaWriteCommand command);
	
}
