package jehc.djshi.iot.common.fins.core.commands;

import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsEndCode;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsEndCode;

public interface FinsResponse {

	public FinsCommandCode getCommandCode();
	
	public FinsEndCode getEndCode();
	
	public byte[] getBytes();
	
}
