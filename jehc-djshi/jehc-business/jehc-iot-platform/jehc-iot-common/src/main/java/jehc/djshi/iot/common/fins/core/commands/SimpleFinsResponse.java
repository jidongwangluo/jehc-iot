package jehc.djshi.iot.common.fins.core.commands;

import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsEndCode;
import jehc.djshi.iot.common.fins.core.FinsCommandCode;
import jehc.djshi.iot.common.fins.core.FinsEndCode;

public abstract class SimpleFinsResponse implements FinsResponse {

	private final FinsCommandCode commandCode;
	private final FinsEndCode endCode;

	protected SimpleFinsResponse(final FinsCommandCode commandCode, final FinsEndCode endCode) {
		this.commandCode = commandCode;
		this.endCode = endCode;
	}

	@Override
	public FinsCommandCode getCommandCode() {
		return this.commandCode;
	}

	@Override
	public FinsEndCode getEndCode() {
		return this.endCode;
	}

}
