package jehc.djshi.iot.common.fins.test;

import jehc.djshi.iot.common.fins.core.FinsIoAddress;
import jehc.djshi.iot.common.fins.core.FinsMaster;
import jehc.djshi.iot.common.fins.core.FinsMasterException;
import jehc.djshi.iot.common.fins.core.FinsNodeAddress;
import jehc.djshi.iot.common.fins.tcp.master.FinsNettyTcpMaster;
import jehc.djshi.iot.common.fins.core.FinsIoAddress;
import jehc.djshi.iot.common.fins.core.FinsMaster;
import jehc.djshi.iot.common.fins.core.FinsMasterException;
import jehc.djshi.iot.common.fins.core.FinsNodeAddress;
import jehc.djshi.iot.common.fins.tcp.master.FinsNettyTcpMaster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
/**
 * TCP 测试
 */
public class TcpMasterTest {
    final static Logger logger = LoggerFactory.getLogger(TcpMasterTest.class);

    public static void main(String... args) throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
        FinsMaster client = new FinsNettyTcpMaster("192.168.89.251", 9600, new FinsNodeAddress(0, 0, 0));

        try {
            client.connect();
            logger.info("Connected");
            FinsNodeAddress destination = new FinsNodeAddress(0, 0, 0);
            FinsIoAddress address = FinsIoAddress.parseFrom(0x82271000);

            /**
             * 定时写数据
             */
            executor.scheduleAtFixedRate(() -> {
                try {logger.info("Going to start writing");
                    Short item = (short) System.currentTimeMillis();
                    client.writeWord(destination, address, item);
                    logger.info(String.format("Thread 1 writing data: (0x%04x)", item));
                } catch (Exception e) {
                    logger.error(e.getLocalizedMessage(), e);
                }
            } , 1, 1, TimeUnit.SECONDS);

            /**
             * 定时读数据
             */
            executor.scheduleAtFixedRate(() -> {
                Short item;
                try {
                    item = client.readWord(destination, address);
                    logger.info(String.format("Thread 2 reading data: (0x%04x)", item));
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } , 100, 500, TimeUnit.MILLISECONDS);

            TimeUnit.SECONDS.sleep(60);

        } catch (FinsMasterException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } finally {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.MINUTES);
            //client.disconnect();
        }
    }
}
