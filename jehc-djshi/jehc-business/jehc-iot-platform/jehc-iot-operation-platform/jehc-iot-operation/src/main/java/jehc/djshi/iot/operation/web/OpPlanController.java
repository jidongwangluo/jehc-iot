package jehc.djshi.iot.operation.web;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.Api;
import jehc.djshi.common.base.BasePage;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.common.entity.OauthAccountEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.util.StringUtil;
import io.swagger.annotations.ApiOperation;
import com.github.pagehelper.PageInfo;
import jehc.djshi.common.base.BaseAction;
import jehc.djshi.common.base.BaseSearch;
import jehc.djshi.iot.operation.model.OpPlan;
import jehc.djshi.iot.operation.service.OpPlanService;
/**
 * @Desc 运维计划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/opPlan")
@Api(value = "运维计划",tags = "运维计划", description = "运维计划")
public class OpPlanController extends BaseAction{
	@Autowired
	private OpPlanService opPlanService;
	/**
	* 查询并分页
	* @param baseSearch 
	*/
	@ApiOperation(value="查询并分页", notes="查询并分页")
	@NeedLoginUnAuth
	@PostMapping(value="/list")
	public BasePage getOpPlanListByCondition(@RequestBody(required=true)BaseSearch baseSearch){
		Map<String, Object> condition = baseSearch.convert();
		commonHPager(baseSearch);
		List<OpPlan> opPlanList = opPlanService.getOpPlanListByCondition(condition);
		for(OpPlan opPlan:opPlanList){
			if(!StringUtil.isEmpty(opPlan.getCreate_id())){
				OauthAccountEntity createBy = getAccount(opPlan.getCreate_id());
				if(null != createBy){
					opPlan.setCreateBy(createBy.getName());
				}
			}
			if(!StringUtil.isEmpty(opPlan.getUpdate_id())){
				OauthAccountEntity modifiedBy = getAccount(opPlan.getUpdate_id());
				if(null != modifiedBy){
					opPlan.setModifiedBy(modifiedBy.getName());
				}
			}
		}
		PageInfo<OpPlan> page = new PageInfo<OpPlan>(opPlanList);
		return outPageBootStr(page,baseSearch);
	}
	/**
	* 查询单条记录
	* @param id 
	*/
	@ApiOperation(value="查询单条记录", notes="查询单条记录")
	@NeedLoginUnAuth
	@GetMapping(value="/get/{id}")
	public BaseResult getOpPlanById(@PathVariable("id")String id){
		OpPlan opPlan = opPlanService.getOpPlanById(id);
		if(!StringUtil.isEmpty(opPlan.getCreate_id())){
			OauthAccountEntity createBy = getAccount(opPlan.getCreate_id());
			if(null != createBy){
				opPlan.setCreateBy(createBy.getName());
			}
		}
		if(!StringUtil.isEmpty(opPlan.getUpdate_id())){
			OauthAccountEntity modifiedBy = getAccount(opPlan.getUpdate_id());
			if(null != modifiedBy){
				opPlan.setModifiedBy(modifiedBy.getName());
			}
		}
		return outDataStr(opPlan);
	}
	/**
	* 添加
	* @param opPlan 
	*/
	@ApiOperation(value="添加", notes="添加")
	@PostMapping(value="/add")
	public BaseResult addOpPlan(@RequestBody OpPlan opPlan){
		int i = 0;
		if(null != opPlan){
			opPlan.setId(toUUID());
			opPlan.setCreate_id(getXtUid());
			opPlan.setCreate_time(getDate());
			i=opPlanService.addOpPlan(opPlan);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 修改
	* @param opPlan 
	*/
	@ApiOperation(value="修改", notes="修改")
	@PutMapping(value="/update")
	public BaseResult updateOpPlan(@RequestBody OpPlan opPlan){
		int i = 0;
		if(null != opPlan){
			opPlan.setUpdate_id(getXtUid());
			opPlan.setUpdate_time(getDate());
			i=opPlanService.updateOpPlan(opPlan);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
	/**
	* 删除
	* @param id 
	*/
	@ApiOperation(value="删除", notes="删除")
	@DeleteMapping(value="/delete")
	public BaseResult delOpPlan(String id){
		int i = 0;
		if(!StringUtil.isEmpty(id)){
			Map<String, Object> condition = new HashMap<String, Object>();
			condition.put("id",id.split(","));
			i=opPlanService.delOpPlan(condition);
		}
		if(i>0){
			return outAudStr(true);
		}else{
			return outAudStr(false);
		}
	}
}
