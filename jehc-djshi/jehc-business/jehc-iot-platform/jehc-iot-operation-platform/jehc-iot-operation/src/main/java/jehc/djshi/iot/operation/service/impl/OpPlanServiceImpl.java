package jehc.djshi.iot.operation.service.impl;
import java.util.List;
import java.util.Map;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.iot.operation.service.OpPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.iot.operation.dao.OpPlanDao;
import jehc.djshi.iot.operation.model.OpPlan;
/**
 * @Desc 运维计划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("opPlanService")
public class OpPlanServiceImpl extends BaseService implements OpPlanService {
	@Autowired
	private OpPlanDao opPlanDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<OpPlan> getOpPlanListByCondition(Map<String,Object> condition){
		try{
			return opPlanDao.getOpPlanListByCondition(condition);
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	public OpPlan getOpPlanById(String id){
		try{
			OpPlan opPlan = opPlanDao.getOpPlanById(id);
			return opPlan;
		} catch (Exception e) {
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param opPlan 
	* @return
	*/
	public int addOpPlan(OpPlan opPlan){
		int i = 0;
		try {
			i = opPlanDao.addOpPlan(opPlan);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param opPlan 
	* @return
	*/
	public int updateOpPlan(OpPlan opPlan){
		int i = 0;
		try {
			i = opPlanDao.updateOpPlan(opPlan);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param opPlan 
	* @return
	*/
	public int updateOpPlanBySelective(OpPlan opPlan){
		int i = 0;
		try {
			i = opPlanDao.updateOpPlanBySelective(opPlan);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delOpPlan(Map<String,Object> condition){
		int i = 0;
		try {
			i = opPlanDao.delOpPlan(condition);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改
	* @param opPlanList 
	* @return
	*/
	public int updateBatchOpPlan(List<OpPlan> opPlanList){
		int i = 0;
		try {
			i = opPlanDao.updateBatchOpPlan(opPlanList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 批量修改（根据动态条件）
	* @param opPlanList 
	* @return
	*/
	public int updateBatchOpPlanBySelective(List<OpPlan> opPlanList){
		int i = 0;
		try {
			i = opPlanDao.updateBatchOpPlanBySelective(opPlanList);
		} catch (Exception e) {
			i = 0;
			/**捕捉异常并回滚**/
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
