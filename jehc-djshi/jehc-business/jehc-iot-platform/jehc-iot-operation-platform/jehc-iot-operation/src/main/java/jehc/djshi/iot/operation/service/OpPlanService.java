package jehc.djshi.iot.operation.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.iot.operation.model.OpPlan;
/**
 * @Desc 运维计划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OpPlanService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OpPlan> getOpPlanListByCondition(Map<String, Object> condition);
	/**
	* 查询对象
	* @param id 
	* @return
	*/
	OpPlan getOpPlanById(String id);
	/**
	* 添加
	* @param opPlan 
	* @return
	*/
	int addOpPlan(OpPlan opPlan);
	/**
	* 修改
	* @param opPlan 
	* @return
	*/
	int updateOpPlan(OpPlan opPlan);
	/**
	* 修改（根据动态条件）
	* @param opPlan 
	* @return
	*/
	int updateOpPlanBySelective(OpPlan opPlan);
	/**
	* 删除
	* @param condition 
	* @return
	*/
	int delOpPlan(Map<String, Object> condition);
	/**
	* 批量修改
	* @param opPlanList 
	* @return
	*/
	int updateBatchOpPlan(List<OpPlan> opPlanList);
	/**
	* 批量修改（根据动态条件）
	* @param opPlanList 
	* @return
	*/
	int updateBatchOpPlanBySelective(List<OpPlan> opPlanList);
}
