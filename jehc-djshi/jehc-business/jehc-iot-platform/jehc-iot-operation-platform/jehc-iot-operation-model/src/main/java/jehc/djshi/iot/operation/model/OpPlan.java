package jehc.djshi.iot.operation.model;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * @Desc 运维计划
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class OpPlan extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**主键id**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**更新时间**/
	private String create_id;/**创建者id**/
	private String update_id;/**更新者id**/
	private Integer del_flag;/**删除标志**/
	private String plan_name;/**计划名称**/
	private String plan_type;/**计划类型：1巡检；2维护**/
	private Integer status;/**计划状态（1：生效；2：暂停）**/
	private Integer plan_cycle;/**计划周期（1：日；2：月；3：年）**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date start_time;/**开始时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date end_time;/**结束时间**/
	private String remarks;/**备注**/
}
