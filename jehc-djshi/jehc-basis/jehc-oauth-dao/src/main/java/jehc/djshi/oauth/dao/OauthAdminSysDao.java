package jehc.djshi.oauth.dao;

import jehc.djshi.oauth.model.OauthAdminSys;

import java.util.List;
import java.util.Map;

/**
 * oauth_admin_sys 管理员拥有系统范围
 * 2019-06-20 14:51:55  邓纯杰
 */
public interface OauthAdminSysDao{

    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdminSys> getOauthAdminSysList(Map<String,Object> condition);

    /**
     *
     * @param oauthAdminSys
     * @return
     */
    int addOauthAdminSys(OauthAdminSys oauthAdminSys);


    /**
     * 唯一对象
     * @param condition
     * @return
     */
    OauthAdminSys getSingleOauthAdminSys(Map<String,Object> condition);

    /**
     * 删除
     * @param id
     * @return
     */
    int delOauthAdminSys(String id);

    /**
     *
     * @param condition
     * @return
     */
    List<OauthAdminSys> getOauthAdminSysListByCondition(Map<String,Object> condition);
}
