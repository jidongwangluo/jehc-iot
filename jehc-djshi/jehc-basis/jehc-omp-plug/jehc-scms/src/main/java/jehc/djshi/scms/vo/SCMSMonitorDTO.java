package jehc.djshi.scms.vo;

import lombok.Data;

/**
 * @Desc 监控信息
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class SCMSMonitorDTO {
    private String key;
    private String value;

    public SCMSMonitorDTO(){

    }

    public SCMSMonitorDTO(String key, String value){
        this.key = key;
        this.value = value;
    }
}
