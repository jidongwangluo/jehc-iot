package jehc.djshi.scms.collect;

import jehc.djshi.scms.model.SCMSMonitor;

public interface SCMSMonitorService {
    /**
     * 保存上报信息
     * @param scmsMonitor
     * @return
     */
    int saveSCMSMonitor(SCMSMonitor scmsMonitor);
}
