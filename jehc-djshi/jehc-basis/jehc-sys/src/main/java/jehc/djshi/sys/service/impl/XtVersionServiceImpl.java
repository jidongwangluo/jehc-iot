package jehc.djshi.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.sys.dao.XtVersionDao;
import jehc.djshi.sys.model.XtVersion;
import jehc.djshi.sys.service.XtVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;

/**
 * @Desc 平台版本
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtVersionService")
public class XtVersionServiceImpl extends BaseService implements XtVersionService {
	@Autowired
	private XtVersionDao xtVersionDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtVersion> getXtVersionListByCondition(Map<String,Object> condition){
		try{
			return xtVersionDao.getXtVersionListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_version_id 
	* @return
	*/
	public XtVersion getXtVersionById(String xt_version_id){
		try{
			XtVersion xt_Version = xtVersionDao.getXtVersionById(xt_version_id);
			return xt_Version;
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtVersion
	* @return
	*/
	public int addXtVersion(XtVersion xtVersion){
		int i = 0;
		try {
			xtVersion.setCreate_id(getXtUid());
			xtVersion.setCreate_time(getDate());
			i = xtVersionDao.addXtVersion(xtVersion);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtVersion
	* @return
	*/
	public int updateXtVersion(XtVersion xtVersion){
		int i = 0;
		try {
			xtVersion.setUpdate_id(getXtUid());
			xtVersion.setUpdate_time(getDate());
			i = xtVersionDao.updateXtVersion(xtVersion);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改（根据动态条件）
	* @param xtVersion
	* @return
	*/
	public int updateXtVersionBySelective(XtVersion xtVersion){
		int i = 0;
		try {
			xtVersion.setUpdate_id(getXtUid());
			xtVersion.setUpdate_time(getDate());
			i = xtVersionDao.updateXtVersionBySelective(xtVersion);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtVersion(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtVersionDao.delXtVersion(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
