package jehc.djshi.sys.service.impl;
import java.util.List;
import java.util.Map;

import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.util.ExceptionUtil;
import jehc.djshi.sys.dao.XtConcordatDao;
import jehc.djshi.sys.model.XtConcordat;
import jehc.djshi.sys.service.XtConcordatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Desc 合同管理
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtConcordatService")
public class XtConcordatServiceImpl extends BaseService implements XtConcordatService {
	@Autowired
	private XtConcordatDao xtConcordatDao;
	/**
	* 分页
	* @param condition 
	* @return
	*/
	public List<XtConcordat> getXtConcordatListByCondition(Map<String,Object> condition){
		try {
			return xtConcordatDao.getXtConcordatListByCondition(condition);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 查询对象
	* @param xt_concordat_id 
	* @return
	*/
	public XtConcordat getXtConcordatById(String xt_concordat_id){
		try {
			return xtConcordatDao.getXtConcordatById(xt_concordat_id);
		} catch (Exception e) {
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
	}
	/**
	* 添加
	* @param xtConcordat
	* @return
	*/
	public int addXtConcordat(XtConcordat xtConcordat){
		int i = 0;
		try {
			i = xtConcordatDao.addXtConcordat(xtConcordat);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 修改
	* @param xtConcordat
	* @return
	*/
	public int updateXtConcordat(XtConcordat xtConcordat){
		int i = 0;
		try {
			i = xtConcordatDao.updateXtConcordat(xtConcordat);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
	/**
	* 删除
	* @param condition 
	* @return
	*/
	public int delXtConcordat(Map<String,Object> condition){
		int i = 0;
		try {
			i = xtConcordatDao.delXtConcordat(condition);
		} catch (Exception e) {
			i = 0;
			throw new ExceptionUtil(e.getMessage(),e.getCause());
		}
		return i;
	}
}
