package jehc.djshi.sys.service.impl;

import java.util.List;
import java.util.Map;

import jehc.djshi.sys.dao.XtNumberDao;
import jehc.djshi.sys.model.XtNumber;
import jehc.djshi.sys.service.XtNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jehc.djshi.common.base.BaseService;

/**
 * @Desc 单号生成
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Service("xtNumberService")
public class XtNumberServiceImpl extends BaseService implements XtNumberService {
	@Autowired
	private XtNumberDao xtNumberDao;
	/**
	 * 分页查询
	 * @param condition
	 * @return
	 */
	public List<XtNumber> getXtNumberListByCondition(Map<String, Object> condition){
		return xtNumberDao.getXtNumberListByCondition(condition);
	}
}
