package jehc.djshi.log.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
* @Desc 异常日志表 
* @Author 邓纯杰
* @CreateTime 2022-08-24 11:24:05
*/
@Data
public class LogError extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**异常ID**/
	private String content;/**异常日志内容**/
	private Integer type;/**异常日志级别**/
}
