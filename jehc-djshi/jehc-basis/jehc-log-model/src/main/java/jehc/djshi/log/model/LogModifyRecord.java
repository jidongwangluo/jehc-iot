package jehc.djshi.log.model;

import jehc.djshi.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
* @Desc 修改记录日志 
* @Author 邓纯杰
* @CreateTime 2022-08-24 12:24:23
*/
@Data
public class LogModifyRecord extends BaseEntity implements Serializable{
	private static final long serialVersionUID = 1L;
	private String id;/**id**/
	private String before_value;/**更变前值**/
	private String after_value;/**变更后值**/
	private String modules;/**模块**/
	private String field;/**字段**/
	private String business_id;/**业务编号**/
	private String batch;/**批次**/
}
