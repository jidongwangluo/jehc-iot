package jehc.djshi.oauth.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.BaseResult;
import jehc.djshi.oauth.service.OauthUninstallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Desc 卸载模块
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("")
@Api(value = "卸载模块",tags = "卸载模块",description = "卸载模块")
public class OauthUninstallController {

    @Autowired
    OauthUninstallService oauthUninstallService;

    /**
     * 卸载模块数据
     * @param id
     */
    @NeedLoginUnAuth
    @DeleteMapping(value="/oauth/uninstall")
    @ApiOperation(value="卸载模块数据", notes="卸载模块数据")
    public BaseResult uninstall(String id){
        return oauthUninstallService.delForUninstall(id);
    }
}
