package jehc.djshi.oauth.web;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jehc.djshi.common.annotation.Auth;
import jehc.djshi.common.annotation.NeedLoginUnAuth;
import jehc.djshi.common.base.*;
import jehc.djshi.common.session.HttpSessionUtils;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.oauth.constant.Constant;
import jehc.djshi.oauth.util.OauthUtil;
import jehc.djshi.oauth.vo.ChannelEntity;
import jehc.djshi.oauth.vo.Transfer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
/**
 * @Desc 授权中心在线用户API
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@RestController
@RequestMapping("/oauthOnline")
@Api(value = "授权中心在线用户API",tags = "授权中心在线用户API",description = "授权中心在线用户API")
@Slf4j
public class OauthOnlineController extends BaseAction {
    @Autowired
    private HttpSessionUtils httpSessionUtils;
    @Autowired
    OauthUtil oauthUtil;

    /**
     * 查询在线账户列表
     */
    @NeedLoginUnAuth
    @PostMapping(value="/list")
    @ApiOperation(value="查询在线账户列表", notes="查询在线账户列表")
    public BasePage getOauthKeyInfoListByCondition(@RequestBody BaseSearch baseSearch){
        List<String> list  = httpSessionUtils.getAllTokenKey();
        List<BaseHttpSessionEntity> entityList = new ArrayList();
        String oauthAccountEntity ="";
        BaseHttpSessionEntity baseHttpSessionEntity ;
        for (String token : list ){
              oauthAccountEntity = httpSessionUtils.getAttribute(token);
              baseHttpSessionEntity  = JsonUtil.fromAliFastJson(oauthAccountEntity, BaseHttpSessionEntity.class);
              entityList.add(baseHttpSessionEntity);
        }
        commonHPager(baseSearch);
        PageInfo<BaseHttpSessionEntity> page = new PageInfo<BaseHttpSessionEntity>(entityList);
        return outPageBootStr(page,baseSearch);
    }


    /**
     * 剔除
     * @param account_id
     */
    @ApiOperation(value="剔除", notes="剔除")
    @DeleteMapping(value="/delete/{account_id}")
    @Auth("/oauthAccount/delete")
    public BaseResult delete(@PathVariable("account_id")String account_id){
        int i = 0;
        if(!StringUtil.isEmpty(account_id)){
            String token = oauthUtil.getTokenByAccountId(account_id);
            if(!StringUtil.isEmpty(token)){
                boolean res = oauthUtil.deleteToken(token);
                if(res){
                    i = 1;
                }
            }
        }
        if(i>0){
            return outAudStr(true);
        }else{
            return outAudStr(false);
        }
    }
}
