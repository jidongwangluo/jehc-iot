package jehc.djshi.oauth.command;

import jehc.djshi.common.cache.redis.RedisUtil;
import jehc.djshi.common.constant.CacheConstant;
import jehc.djshi.common.util.JsonUtil;
import jehc.djshi.oauth.model.OauthKeyInfo;
import jehc.djshi.oauth.model.OauthSysMode;
import jehc.djshi.oauth.service.OauthKeyInfoService;
import jehc.djshi.oauth.service.OauthSysModeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * @Desc 初始化数据
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
@Order(0)
public class InitOauthData implements CommandLineRunner {
    @Autowired
    OauthKeyInfoService oauthKeyInfoService;

    @Autowired
    OauthSysModeService oauthSysModeService;

    @Autowired
    private RedisUtil redisUtil;

    /**
     *
     */
    private void init(){
        List<OauthKeyInfo> list = oauthKeyInfoService.getOauthKeyInfoListByCondition(new HashMap<>());
        for(OauthKeyInfo oauthKeyInfo:list){
            redisUtil.hset(CacheConstant.OAUTH_KEY,oauthKeyInfo.getKey_name(), JsonUtil.toFastJson(oauthKeyInfo));
        }

        List<OauthSysMode> oauthSysModeList = oauthSysModeService.getOauthSysModeListByCondition(new HashMap<>());
        redisUtil.set(CacheConstant.OAUTH_SYS_MODE,JsonUtil.toFastJson(oauthSysModeList));
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            init();
        } catch (Exception e) {

        }
    }
}
