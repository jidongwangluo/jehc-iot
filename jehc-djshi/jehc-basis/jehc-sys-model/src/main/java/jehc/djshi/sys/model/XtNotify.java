package jehc.djshi.sys.model;
import java.util.List;
import jehc.djshi.common.base.BaseEntity;
import lombok.Data;
/**
 * @Desc 通知
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class XtNotify  extends BaseEntity{
	private String notify_id;/**主键**/
	private String title;/**标题**/
	private String content;/**内容**/
	private int type;/**通知类型0默认1平台通知(系统自动通知）**/
	private List<XtNotifyReceiver> notifyReceivers;
}
