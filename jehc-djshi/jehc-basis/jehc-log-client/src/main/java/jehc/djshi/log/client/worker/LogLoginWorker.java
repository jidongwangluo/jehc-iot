package jehc.djshi.log.client.worker;

import jehc.djshi.common.util.SpringUtils;
import jehc.djshi.log.dao.LogLoginDao;
import jehc.djshi.log.model.LogLogin;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 登录日志工作线程
 * </p>
 *
 * @author dengcj
 * @since 2022-08-18
 */
@Slf4j
public class LogLoginWorker implements Runnable{

    LogLogin logLogin;//登录日志信息

    HttpServletRequest request;//请求

    public LogLoginWorker(){

    }

    /**
     *
     * @param logLogin
     * @param request
     * @param restTemplateUtil
     */
    public LogLoginWorker(LogLogin logLogin, HttpServletRequest request){
        this.logLogin = logLogin;
        this.request = request;
    }

    /**
     *
     */
    public void run(){
        try {
            send(request);
        } catch (Exception e) {
            log.info("记录登录日志失败:"+logLogin+"，异常信息：{}",e);
        }
    }

    /**
     *
     * @param request
     */
    public void send(HttpServletRequest request){
        try {
            log.info("记录登录日志失败开始");
            LogLoginDao logLoginDao =  SpringUtils.getBean(LogLoginDao.class);
            logLoginDao.addLogLogin(logLogin);
            log.info("记录登录日志失败结束");
        } catch (Exception e) {
            log.info("记录登录日志失败失败:"+logLogin+"，异常信息：{}",e);
        }
    }
}
