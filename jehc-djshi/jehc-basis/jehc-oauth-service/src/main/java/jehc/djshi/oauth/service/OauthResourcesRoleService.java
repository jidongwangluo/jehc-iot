package jehc.djshi.oauth.service;
import java.util.List;
import java.util.Map;

import jehc.djshi.oauth.model.OauthFunctionRole;
import jehc.djshi.oauth.model.OauthResourcesRole;
import jehc.djshi.oauth.model.OauthFunctionRole;
import jehc.djshi.oauth.model.OauthResourcesRole;

/**
 * @Desc 授权中心资源对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthResourcesRoleService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthResourcesRole> getOauthResourcesRoleListByCondition(Map<String, Object> condition);

	/**
	 * 分配资源
	 * @param oauthResourcesRoleList
	 * @param oauthFunctionRoleList
	 * @param role_id
	 * @return
	 */
	int addOauthResourcesRole(List<OauthResourcesRole> oauthResourcesRoleList, List<OauthFunctionRole> oauthFunctionRoleList, String role_id);

}
