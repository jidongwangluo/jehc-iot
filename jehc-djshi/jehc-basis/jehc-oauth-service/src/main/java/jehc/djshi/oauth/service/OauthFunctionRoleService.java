package jehc.djshi.oauth.service;
import java.util.List;
import java.util.Map;
import jehc.djshi.oauth.model.OauthFunctionRole;
import jehc.djshi.oauth.model.OauthFunctionRole;

/**
 * @Desc 授权中心功能对角色
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public interface OauthFunctionRoleService{
	/**
	* 分页
	* @param condition 
	* @return
	*/
	List<OauthFunctionRole> getOauthFunctionRoleListByCondition(Map<String, Object> condition);

	/**
	* 添加
	* @param oauthFunctionRole 
	* @return
	*/
	int addOauthFunctionRole(OauthFunctionRole oauthFunctionRole);
}
