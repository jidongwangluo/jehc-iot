package jehc.djshi.job.task;

import jehc.djshi.common.base.BaseService;
import lombok.extern.slf4j.Slf4j;

/**
 * @Desc 数据权限Job
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class XtDataAuthorityTask extends Thread{
	/**
	 * 业务逻辑处理
	 */
	public void service() {
		new XtDataAuthorityTask().start();
	}
	public void run(){
		try {
			excute();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	public void excute(){
		log.info("----------开始推送数据权限--------------");
		BaseService baseService = new BaseService();
		baseService.addPushDataAuthority();
		log.info("----------结束推送数据权限--------------");
	}
}
