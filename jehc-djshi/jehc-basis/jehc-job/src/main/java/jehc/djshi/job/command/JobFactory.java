package jehc.djshi.job.command;

import cn.hutool.core.collection.CollectionUtil;
import jehc.djshi.common.base.BaseService;
import jehc.djshi.common.entity.ScheduleJob;
import jehc.djshi.common.util.StringUtil;
import jehc.djshi.job.publisher.ChannelUtil;
import jehc.djshi.job.publisher.NettyUtil;
import jehc.djshi.job.vo.ChannelEntity;
import jehc.djshi.job.vo.JobHandlerEntity;
import jehc.djshi.job.vo.RequestInfo;
import jehc.djshi.job.publisher.ChannelUtil;
import jehc.djshi.job.publisher.NettyUtil;
import jehc.djshi.job.vo.ChannelEntity;
import jehc.djshi.job.vo.JobHandlerEntity;
import jehc.djshi.job.vo.RequestInfo;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Random;

/**
 * 任务集中处理工厂
 */
@Slf4j
public class JobFactory extends BaseService implements Job {

	@Autowired
    ChannelUtil channelUtil;
	@Autowired
    NettyUtil nettyUtil;
	/**
	 * 执行操作
	 * @param context
	 * @throws JobExecutionException
	 */
	public void execute(JobExecutionContext context) throws JobExecutionException {
		ScheduleJob scheduleJob = (ScheduleJob) context.getMergedJobDataMap().get("scheduleJob");
		String clientGroupId = scheduleJob.getClientGroupId();
		String jobHandler = scheduleJob.getJobHandler();
		if(!StringUtil.isEmpty(clientGroupId) && !StringUtil.isEmpty(jobHandler)){
			List<ChannelEntity> channelEntities = channelUtil.get(clientGroupId);
			if(CollectionUtil.isNotEmpty(channelEntities)){
				send(channelEntities,scheduleJob);
			}
		}
		log.info("scheduleJob，{}",scheduleJob);
	}

	public void send(List<ChannelEntity> channelEntities,ScheduleJob scheduleJob){
		try {
			/*
			String name = scheduleJob.getJobName();
			String id = scheduleJob.getJobId();
			String des = scheduleJob.getDesc();
			String clientId = scheduleJob.getClientId();
			String jobHandler = scheduleJob.getJobHandler();
			String jobPara = scheduleJob.getJobPara();
			String jobTitle = scheduleJob.getJobTitle();
			*/
			Random random = new Random();
			int size = random.nextInt(channelEntities.size());
			ChannelEntity channelEntity = channelEntities.get(size);
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setJobHandlerEntity(new JobHandlerEntity(scheduleJob.getJobHandler(),scheduleJob.getJobPara()));
			requestInfo.setObj(scheduleJob.getJobPara());
			channelEntity.setRequestInfo(requestInfo);
			nettyUtil.sendMessage(channelEntity);
		}catch (Exception e){
			e.printStackTrace();
			log.info("发送任务异常，{}",e);
		}
	}
}