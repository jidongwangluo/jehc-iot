package jehc.djshi.common.config;

import jehc.djshi.common.handler.SignHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Desc 验签拦截器
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Configuration
public class SignConfig implements WebMvcConfigurer {

    /**
     * 由于在拦截器中注解无效，需要提前注入bean
     * @return
     */
    @Bean
    public SignHandler httpUnRequestBodyHandler(){
        return new SignHandler();
    }

    /**
     * 放开资源
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(httpUnRequestBodyHandler());
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/favicon.ico",
                "/js/**",
                "/html/**",
                "/resources/*",
                "/webjars/**",
                "/v2/api-docs/**",
                "/swagger-ui.html/**",
                "/swagger-resources/**",
                "/oauth",
                "/druid/**",
                "/druid",
                "/monitor/druid",
                "/isAdmin",
                "/accountInfo",
                "/httpSessionEntity"
        );
    }
}
