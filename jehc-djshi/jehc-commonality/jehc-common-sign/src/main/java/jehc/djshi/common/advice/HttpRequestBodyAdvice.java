//package jehc.djshi.common.handler;
//
//import jehc.djshi.common.annotation.NeedHttpBodyDecrypt;
//import jehc.djshi.common.util.DecryptUtils;
//import jehc.djshi.common.util.RestTemplateUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.core.MethodParameter;
//import org.springframework.core.annotation.Order;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpInputMessage;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
//
//import javax.annotation.Resource;
//import java.io.ByteArrayInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.lang.reflect.Type;
//
///**
// * @Desc 全局参数加密解密
// * @Author 邓纯杰
// * @CreateTime 2012-12-12 12:12:12
// */
//@ControllerAdvice
//@Slf4j
//@Order(-1)
//public class HttpRequestBodyAdviceHandler implements RequestBodyAdvice {
//
//    @Resource
//    RestTemplateUtil restTemplateUtil;
//
//    /**
//     *
//     * @param args
//     * @param targetType
//     * @param converterType
//     * @return
//     */
//    @Override
//    public boolean supports(MethodParameter args, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
//        boolean methodNeedHttpBodyDecrypt = args.hasMethodAnnotation(NeedHttpBodyDecrypt.class);
//        if (methodNeedHttpBodyDecrypt) {
//            log.info("supports");
//            return true;
//        }
//        boolean classNeedHttpBodyDecrypt = args.getDeclaringClass().getAnnotation(NeedHttpBodyDecrypt.class) != null;
//        if (classNeedHttpBodyDecrypt) {
//            log.info("supports");
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * 参数解密 服务端解密 用于验签
//     * @param inputMessage
//     * @param parameter
//     * @param targetType
//     * @param converterType
//     * @return
//     * @throws IOException
//     */
//    @Override
//    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
//        log.info("beforeBodyRead");
//        if (inputMessage.getBody().available() <= 0) {
//            return inputMessage;
//        }
//        byte[] requestDataByte = new byte[inputMessage.getBody().available()];
//        inputMessage.getBody().read(requestDataByte);
//        requestDataByte  = DecryptUtils.decrypt(requestDataByte);//对参数进行解密（与前端约定解密规则）
//        InputStream rawInputStream = new ByteArrayInputStream(requestDataByte);// 对解密后数据，构造新读取流
//        return new HttpInputMessage() {
//            @Override
//            public HttpHeaders getHeaders() {
//                return inputMessage.getHeaders();
//            }
//
//            @Override
//            public InputStream getBody() throws IOException {
//                return rawInputStream;
//            }
//        };
//    }
//
//    /**
//     *
//     * @param body
//     * @param inputMessage
//     * @param parameter
//     * @param targetType
//     * @param converterType
//     * @return
//     */
//    @Override
//    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
//        log.info("afterBodyRead,{}",body);
//        return body;
//    }
//
//    /**
//     *
//     * @param body
//     * @param inputMessage
//     * @param parameter
//     * @param targetType
//     * @param converterType
//     * @return
//     */
//    @Override
//    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
//        log.info("handleEmptyBody,{}",body);
//        return null;
//    }
//}
