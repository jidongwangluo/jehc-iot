package jehc.djshi.common.base;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import jehc.djshi.common.util.date.DateUtil;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
/**
 * @Desc Entity支持类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class BaseEntity extends DateUtil{
	private Object item;
	private Integer version;/**乐观锁使用**/
	private String xt_userinfo_realName;/**操作人名称**/
	private String xt_userinfo_email;/**邮件**/
	private String jehcimg_base_url;/**图片工程资源统一URL**/
	private String jehcimg_base_path_url;/**图片路径统一URL+路径（完整路径）**/
	private String jehcsources_base_url;/**平台默认资源统一URL**/
	private String jehcsources_base_path_url;/**平台默认资源路径统一URL+路径（完整路径）**/
	private String xt_attachmentPath;/**文件相对路径**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date create_time;/**创建时间**/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;/**修改时间**/
	private String createBy;/**创建人名称**/
	private String modifiedBy;/**修改者名称**/
	private String create_id;/**创建人id**/
	private String update_id;/**修改人id**/
	@Deprecated
	private Integer delFlag;/**删除标记：0正常1已删除（废弃）**/
	private Integer del_flag;/**删除标记：0正常1已删除**/
	private String sessionId;
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
