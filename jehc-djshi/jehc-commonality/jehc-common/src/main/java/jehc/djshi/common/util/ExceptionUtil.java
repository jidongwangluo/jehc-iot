package jehc.djshi.common.util;
/**
 * @Desc 自定义异常类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class ExceptionUtil extends RuntimeException {
    public ExceptionUtil(String message, Throwable cause,boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    public ExceptionUtil(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionUtil(String message) {
        super(message);
    }

    public ExceptionUtil(Throwable cause) {
        super(cause);
    }
}
