package jehc.djshi.common.cache.redis;
import jehc.djshi.common.constant.CacheConstant;
import jehc.djshi.common.util.StringUtil;

import java.util.UUID;

/**
 * @Desc LockEntity
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class LockEntity {
    String lockKey;//锁的key键
    String lockValue;//自定义值
    Integer expireLockTimeOut;//锁的自动释放时间(秒)
    Integer timeoutSecond;//未获取到锁的请求，尝试重试的最久等待时间(秒)，比如取锁 不能超过200秒 如果超过200秒 则自动加锁失败
    Integer retryTimes;//重试机制次数

    public LockEntity(){

    }

    public LockEntity(String lockKey){
        this.lockKey = lockKey;
    }

    public String getLockKey() {
        return lockKey;
    }

    public void setLockKey(String lockKey) {
        this.lockKey = lockKey;
    }

    public String getLockValue() {
        if(StringUtil.isEmpty(lockValue)){
            lockValue = UUID.randomUUID().toString();
        }
        return lockValue;
    }

    public void setLockValue(String lockValue) {
        this.lockValue = lockValue;
    }

    public Integer getExpireLockTimeOut() {
        if (null == expireLockTimeOut || expireLockTimeOut <= 0 ) {
            expireLockTimeOut = CacheConstant.LOCKE_EXPIRE_LOCK_TIME_OUT;
        }
        return expireLockTimeOut;
    }

    public void setExpireLockTimeOut(Integer expireLockTimeOut) {
        this.expireLockTimeOut = expireLockTimeOut;
    }

    public Integer getTimeoutSecond() {
        if (timeoutSecond <= 0) {
            timeoutSecond = CacheConstant.LOCK_TIME_OUT;
            return timeoutSecond;
        }
        return timeoutSecond;
    }

    public void setTimeoutSecond(Integer timeoutSecond) {
        this.timeoutSecond = timeoutSecond;
    }

    public Integer getRetryTimes() {
        if (null == retryTimes || retryTimes <= 0 ) {
            retryTimes = CacheConstant.LOCK_RETRY_TIMES;
        }
        return retryTimes;
    }

    public void setRetryTimes(Integer retryTimes) {
        this.retryTimes = retryTimes;
    }
}
