package jehc.djshi.common.base.binder;
import org.springframework.beans.propertyeditors.PropertiesEditor;

/**
 * @Desc DoubleEditor
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class DoubleEditor extends PropertiesEditor{
	@Override  
    public void setAsText(String text) throws IllegalArgumentException {  
        if (text == null || text.equals("")) {  
            text = "0";  
        }  
        setValue(Double.parseDouble(text));  
    }  
  
    @Override  
    public String getAsText() {  
        return getValue().toString();  
    }  
}
