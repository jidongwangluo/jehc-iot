package jehc.djshi.common.constant;

/**
 * @Desc SESSION常量
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class SessionConstant {
	public static final String SESSIONID = "JEHC-SESSION-ID";
	public static final String TOKEN="Token";


	public static final String TOKEN_STORE_PATH=BaseConstant.ROOT+"Token"+":";//token存储路径

	public static final String VERIFY_STORE_PATH = BaseConstant.ROOT+"Verify"+":";//Verify存储路径

	public static final String ACCOUNT_STORE_PATH = BaseConstant.ROOT+"Account"+":";//用户存储路径 存放Key:account-id，Value:token值
}

