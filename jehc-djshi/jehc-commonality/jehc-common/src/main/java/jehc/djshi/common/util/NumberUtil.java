package jehc.djshi.common.util;

import jehc.djshi.common.constant.CacheConstant;
import jehc.djshi.common.constant.CacheConstant;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
/**
 * @Desc NumberUtil
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Component
public class NumberUtil {

    @Autowired
    private RedisTemplate redisTemplate;

    private static DecimalFormat df = new DecimalFormat("#0.00");

    public static double getValue(double value) {
        return Math.round(value * 10000.0) / 10000.0;
    }

    public static double get2DecimalValue(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    public static double getZeroIfNull(Double value) {
        return value == null ? 0.0 : value;
    }

    public static String get2DecimalString(double value) {
        return df.format(value);
    }

    public static long getZeroIfNull(Long value) {
        return value == null ? 0 : value;
    }

    /** round n down to nearest multiple of m */
    public static long roundDown(long n, long m) {
        return n >= 0 ? (n / m) * m : ((n - m + 1) / m) * m;
    }

    public static int getValue(Integer number) {
        return ObjectUtils.defaultIfNull(number, 0);
    }

    /**
     * @param v1
     *           double
     * @return String
     * */
    public static String getAdditionResultString(double...v1) {
        BigDecimal total = new BigDecimal(0);
        BigDecimal arg;
        for (int i = 0; i < v1.length; i++) {
            arg = new BigDecimal(Double.toString(v1[i]));
            total = total.add(arg);
        }
        return get2DecimalString(total.doubleValue());
    }

    /**
     * @param v1
     *           double
     * @return String
     * */
    public static double getAdditionResultValue(double...v1) {
        BigDecimal total = new BigDecimal(0);
        BigDecimal arg;
        for (int i = 0; i < v1.length; i++) {
            arg = new BigDecimal(Double.toString(v1[i]));
            total = total.add(arg);
        }
        return get2DecimalValue(total.doubleValue());
    }

    private static DecimalFormat moneyFormat = new DecimalFormat("#,##0.00;-#,##0.00");

    public static String formattedString(String amount) {
        return "$" + amount;
    }

    public static String formattedString(double amount) {
        return "$" + getValue(amount);
    }

    public static String getFormattedAmount(double amount) {
        return moneyFormat.format(amount);
    }

    public static double getRoundingDownAmount(double amount, int cents) {
        long amountInCents = Math.round(amount * 100);
        return NumberUtil.roundDown(amountInCents, (long)cents) / 100.0;
    }

    /**
     * 将数据保留两位小数
     */
    public static String getFormatted(double num) {
        DecimalFormat dFormat=new DecimalFormat("#0.00");
        return dFormat.format(num);
    }





    /**
     * 通过key获取自增
     * @param key
     * @return
     */
    public Long generateId(String key) {
        RedisAtomicLong counter = new RedisAtomicLong(key, redisTemplate.getConnectionFactory());
        return counter.incrementAndGet();
    }

    /**
     * 通过key获取自增并设定过期时间
     * @param key
     * @param date 过期时间
     * @return
     */
    public Long generateId(String key,Date date) {
        RedisAtomicLong counter = new RedisAtomicLong(key, redisTemplate.getConnectionFactory());
        counter.expireAt(date);
        return counter.incrementAndGet();
    }

    /**
     * 获取唯一ID
     * @return
     */
    public Long getUniqueNumber(){
        StringBuffer sb = new StringBuffer();
        int maxLen = 16;//生成16位ID
        Long generateId = generateId(CacheConstant.JEHC_AUTO_KEY);
        int len = String .valueOf(generateId).length();
        if(len < maxLen){//长度小于16进行补0
            sb.append("1");
            for (int i = 0; i < maxLen - (len+1); i++) {
                sb.append("0");
            }
        }
        sb.append(generateId);
        return Long.valueOf(sb.toString());
    }

    /**
     * 获取唯一订单号
     * @return
     */
    public String genOrderNubmer(){
        Long id = generateId(CacheConstant.JEHC_ORDER_KEY,getExpireAtData());
        String orderId = format(id, CacheConstant.JEHC_ORDER_NUMBER_PREFIX);
        return orderId;
    }

    /**
     * 过期时间
     * @return
     */
    public static Date getExpireAtData(){
        Date date = null;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        date = calendar.getTime();
        return date;
    }

    /**
     * 格式化时间
     * @param format 时间格式
     * @return
     */
    public static String getDateFormat(String format){
        DateFormat df = new SimpleDateFormat(null == format || "".equals(format) ? CacheConstant.JEHC_DATE_FORMAT : format);
        return df.format(new Date());
    }


    /**
     * 自定义格式
     * @param id
     * @param prefix 前缀
     * @return
     */
    private String format(Long id,String prefix){
        int minLength = 20;
        StringBuffer sb = new StringBuffer();
        if(null != prefix && !"".equals(prefix)){
            sb.append(prefix);
        }
        sb.append(getDateFormat(null));
        String strId = sb.toString()+""+id;
        int length = strId.length();
        if(length < minLength){
            for(int i = 0;i < minLength - length; i++){
                sb.append("0");
            }
        }
        sb.append(id);
        return sb.toString();
    }

}
