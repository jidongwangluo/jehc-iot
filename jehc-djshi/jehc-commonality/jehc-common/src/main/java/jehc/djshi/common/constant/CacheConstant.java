package jehc.djshi.common.constant;
/**
 * @Desc 缓存变量存放位置
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
public class CacheConstant {
	public static final String OAUTH_KEY = BaseConstant.ROOT+"JEHC-OAUTH-KEY-INFO";
	public static final String OAUTH_SYS_MODE = BaseConstant.ROOT+"JEHC-OAUTH-SYS-MODE";
	public static final String SYSHASH = BaseConstant.ROOT+"JEHC-SYS-DATA";
	public static final String XTDATADICTIONARYCACHE=BaseConstant.ROOT+"XtDataDictionaryCache";//数据字典
	public static final String XTPATHCACHE=BaseConstant.ROOT+"XtPathCache";//平台路径
	public static final String XTIPFROZENCACHE=BaseConstant.ROOT+"XtIpFrozenCache";//IP黑户
	public static final String XTCONSTANTCACHE= BaseConstant.ROOT+"XtConstantCache";//平台常量
	public static final String XTFUNCTIONINFOCOMMONCACHE=BaseConstant.ROOT+"XtFunctioninfoCommonCache";//公共功能
	public static final String SOLRCORECACHE=BaseConstant.ROOT+"SolrCoreCache";//SOLR实例缓存
	public static final String XTAREAREGIONCACHE = BaseConstant.ROOT+"XtAreaRegionCache";//行政区域实例缓存
	
	public static final String ONLINEUSERINFO=BaseConstant.ROOT+"OnLineuserinfo";//在线用户监控1表示一个账号不能同时登陆多台设备或多浏览器 其他表示无限制
	
	public static final String XTPATHCACHE_XTDbSTRUCTURE_FILE_PATH = BaseConstant.ROOT+"XTPATHCACHE_XTDbSTRUCTURE_FILE_PATH";//数据库表结构word导出路径
	
	public static final String XTDBBACKPATH= BaseConstant.ROOT+"XTDBBACKPATH";//数据库备份路径

	public static final String JEHC_CLOUD_KEY ="jehcCloudKey";
	public static final String JEHC_CLOUD_SECURITY ="jehcCloudSecurity";

	public static final String JEHC_CLOUD_USER_DEFAULT_PWD="123456";

	public static final String JEHC_CLOUD_LOCK = BaseConstant.ROOT+"JEHC_CLOUD_LOCK";

	//////////分布式锁相关开始////////////
    public static final Integer LOCKE_EXPIRE_LOCK_TIME_OUT = 30;//加锁默认失效时间（分布式环境）
    public static final Integer LOCK_RETRY_TIMES= 5;//加锁重试次数（分布式环境）
    public static final Long LOCK_RETRY_INTERVAL = 50L;//加锁重试时间间隔（分布式环境）
    public static final Integer LOCK_TIME_OUT = 2;//未获取到锁的请求，尝试重试的最久等待时间(秒)，比如取锁 不能超过200秒 如果超过200秒 则自动加锁失败（分布式环境）
	//////////分布式锁相关结束////////////

	////////////订单号相关开始
	public static final String JEHC_AUTO_KEY = BaseConstant.ROOT+"JEHC_AUTO_KEY";// 增长id 缓存key
	public static final String JEHC_ORDER_KEY = BaseConstant.ROOT+"JEHC_ORDER_KEY";// 订单缓存key
	public static final String JEHC_ORDER_NUMBER_PREFIX = "Z";//订单前缀
	public static final String JEHC_DATE_FORMAT = "yyyyMMddHHmmss";// 时间格式化
}
