package jehc.djshi.common.util.logger;
import lombok.extern.slf4j.Slf4j;
/**
 * @Desc logback工具类
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Slf4j
public class Logback4jUtil{
	public Logback4jUtil() {
	}

	public void debug(String className, String msg) {
		log.debug(className + " - " + msg);
	}

	public void info(String className, String msg) {
		log.info(className + " - " + msg);
	}

	public void warn(String className, String msg) {
		log.warn(className + " - " + msg);
	}

	public void error(String className, String msg) {
		log.error(className + " - " + msg);
	}
}
