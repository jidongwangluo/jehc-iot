package jehc.djshi.common.entity;

import lombok.Data;

/**
 * @Desc 角色实体
 * @Author 邓纯杰
 * @CreateTime 2012-12-12 12:12:12
 */
@Data
public class RoleinfoEntity {
    private String account_id;
    private String role_id;
    private String sources_id;
}
